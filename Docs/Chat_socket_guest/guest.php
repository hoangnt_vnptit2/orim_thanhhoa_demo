<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\RoomChatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Room Chats';
$this->params['breadcrumbs'][] = $this->title;

$baseUrl = Yii::$app->request->baseUrl;
$this->registerJsFile("@web/js/socket-connect-guest.js",
    ['depends' => [dmstr\web\AdminLteAsset::className(), \frontend\assets\AppAsset::className()]]);
$this->registerJs(<<<JS
    
    var roomDetail = new Vue({
        el: '#roomDetailBox',
        data: {
            criminalObject: {
                avatar_path: '/img/avatar-default.png' 
            },
            roomList: [],
            roomMessageList: [],
            roomSelected: {},
            isLoading: false,
            limit: 8,
            start: 0,
            lastMessage: false
        },
        methods: {
            init: function(){
                var me = this;
                me.getRoomList(function(){
                    me.getRoomMessageList();
                });
            },
            getRoomList: function(callback){
                var me = this;
                me.isLoading = true;
                $.ajax({
                    url: "$baseUrl/room-chat/get-room-list",
                    dataType: "json",
                    type: "post",
                    success: function(res){
                        me.roomList = res;
                        me.isLoading = false;
                        me.roomSelected = res[0];
                        if(callback) {
                            callback();
                        }
                    },
                    error: function(){
                    }
                });
            },
            getRoomMessageList: function(){
                var me = this;
                me.isLoading = true;
                $.ajax({
                    url: "$baseUrl/room-chat/load-message",
                    dataType: "json",
                    type: "post",
                    data: {limit:me.limit, start:me.start, room_id: me.roomSelected.room_id},
                    success: function(res){
                        if (!res.length) {
                            me.lastMessage = true;
                        }
                        me.roomMessageList = me.roomMessageList.concat(res);
                        me.isLoading = false;
                    }
                });
            },
            changeRoomSelected: function(room_id) {
              me = this;
              me.roomSelected = me.roomList[room_id];
              me.roomMessageList = [];
              console.log(me.roomSelected);
              me.getRoomMessageList();
            }
        }
    });
    roomDetail.init();
    window.onload = function () { $(".box-body").scrollTop($(".box-body")[0].scrollHeight); }
    
    $('.box-body').scroll(function(){
        if (($('.box-body').scrollTop() == 0) && !roomDetail.lastMessage){
            
             roomDetail.isLoading = true;
             roomDetail.start = roomDetail.start + roomDetail.limit;
             roomDetail.getRoomMessageList();
             // $('.box-body').scrollTop(30);
             // $('.inner').prepend(data);

        }
    });
JS
);


?>
<div id="roomDetailBox">
    <div class="col-md-9" style="padding-right: 0px;">
        <div class="box no-border direct-chat direct-chat-warning" style="margin-top: 15px">
            <div class="box-header with-border">
                <h3 class="box-title">Direct Chat</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="max-height: calc(100vh - 213px);min-height: calc(100vh - 213px);">
                <!-- Conversations are loaded here -->
                <div class="direct-chat-messages" style="height: auto;">
                    <!-- Message. Default to the left -->
                    <div v-for="(message,index) in roomMessageList" v-if="message.user_id" class="direct-chat-msg"
                         style="max-width: 80%; float:left; min-width: 60%; clear: both;">
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-left">{{roomSelected.full_name}}</span>
                            <span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                        </div>
                        <!-- /.direct-chat-info -->
                        <img class="direct-chat-img" src="/img/avatar-default.png" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            {{message.message}}
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>

                    <!-- Message. Default to the left -->
                    <div v-else="" class="direct-chat-msg right"
                         style="max-width: 80%; float: right; min-width: 60%; clear: both;">
                        <div class="direct-chat-info clearfix">
                            <span class="direct-chat-name pull-right">{{roomSelected.full_name}}</span>
                            <span class="direct-chat-timestamp pull-left">23 Jan 2:00 pm</span>
                        </div>
                        <!-- /.direct-chat-info -->
                        <img class="direct-chat-img" src="/img/avatar-default.png" alt="message user image">
                        <!-- /.direct-chat-img -->
                        <div class="direct-chat-text">
                            {{message.message}}
                        </div>
                        <!-- /.direct-chat-text -->
                    </div>

                </div>

                <!-- /.direct-chat-pane -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer" style="margin-bottom: 15px;">
                <div id="feedback"></div>
<!--                <form action="#" method="post">-->
                    <div class="input-group">
                        <input id="message" type="text" name="message" placeholder="Type Message ..." class="form-control">
                        <span class="input-group-btn">
                            <button id="send" type="button" class="btn btn-warning btn-flat">Send</button>
                          </span>
                    </div>
<!--                </form>-->
            </div>
            <!-- /.box-footer-->
        </div>
    </div>

    <div class="col-md-3" style="margin-top: 15px;">
        <div class="col-md-10 col-md-offset-1">
            <br>
            <br>
            <button id="btn113" type="button" class="btn btn-block btn-primary">113</button>
            <br>
            <br>
            <button id="btn114" type="button" class="btn btn-block btn-primary">114</button>
            <br>
            <br>
            <button id="btn115" type="button" class="btn btn-block btn-primary">115</button>
        </div>



    </div>


</div>
