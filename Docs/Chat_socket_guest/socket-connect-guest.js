
socket.on('connect', function(){

    setTimeout(function () {
        console.log(APP_CFG.guest_phone);


        var message = document.getElementById('message'),
            btnSend = document.getElementById('send'),
            btn113 = document.getElementById('btn113'),
            btn114 = document.getElementById('btn114'),
            feedback = document.getElementById('feedback'),
            btn115 = document.getElementById('btn115');
        var roomSelecting ='';
        var callCenter = '';
        btn113.addEventListener('click', function(){
            console.log('Create room 113-'+ APP_CFG.guest_phone);
            socket.emit('guestJoinRoom', {room_id: '113-'+APP_CFG.guest_phone, call_center:'113', phone_number:APP_CFG.guest_phone});
            roomSelecting = '113-'+APP_CFG.guest_phone;
            callCenter = '113';
        });

        btn114.addEventListener('click', function(){
            console.log('Create room 114-'+ APP_CFG.guest_phone);
            socket.emit('guestJoinRoom', {room_id: '114-'+APP_CFG.guest_phone, call_center:'114', phone_number:APP_CFG.guest_phone });
            roomSelecting = '114-'+APP_CFG.guest_phone;
            callCenter = '114';
        });

        btn115.addEventListener('click', function(){
            console.log('Create room 115-'+ APP_CFG.guest_phone);
            socket.emit('guestJoinRoom', {room_id: '115-'+APP_CFG.guest_phone, call_center:'115', phone_number:APP_CFG.guest_phone});
            roomSelecting = '115-'+APP_CFG.guest_phone;
            callCenter = '115';
        });

        $(window).on('keydown', function(e) {
            if (e.which == 13) {
                sendMessage(message, roomSelecting, callCenter);
                return false;
            }
        });

        btnSend.addEventListener('click', function(){
            sendMessage(message, roomSelecting, callCenter);
        });

        socket.on('newMessage', function(data){
            feedback.innerHTML = '';
            appendMessageLeft(data);
        });

        message.addEventListener('keypress', function(){
            socket.emit('typing', {room_id: roomSelecting, phone_number: APP_CFG.guest_phone});
        })

        socket.on('typing', function(data){
            feedback.innerHTML = '<p><em>' + data + ' is typing a message...</em></p>';
        });
    })
});

socket.on("unauthorized", function(error, callback) {
    if (error.data.type == "UnauthorizedError" || error.data.code == "invalid_token") {
      // redirect user to login page perhaps or execute callback:
      console.log("User's token has expired");
    }
});

function sendMessage(message, roomSelecting, callCenter) {

    console.log(message.value);
    if($.trim(message) == '') {
        return false;
    }

    socket.emit('sendMessage', {
        message: message.value,
        room_id: roomSelecting,
        phone_number: APP_CFG.guest_phone,
        call_center: callCenter
    });

    appendMessageRight({message: message.value});
    message.value = "";
}

function appendMessageLeft(data) {
    $('<div class="direct-chat-msg" style="max-width: 80%; float: left; min-width: 60%; clear: both;">'
        + '<div class="direct-chat-info clearfix">' +
        '<span class="direct-chat-name pull-left">' + data.guestPhone + '</span>' +
        '<span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>\n' +
        '</div>'
        + '<img class="direct-chat-img" src="/img/avatar-default.png" alt="message user image">'
        + '<div class="direct-chat-text">' + data.message + '</div>'
        + '</div>').appendTo($('.direct-chat-messages'));
    // $('.message-input input').val(null);
    // $('.contact.active .preview').html('<span>You: </span>' + message);
    $('html, .box-body').animate({scrollTop:$(".direct-chat-messages").height()}, 'fast');
}

function appendMessageRight(data) {
    $('<div class="direct-chat-msg right" style="max-width: 80%; float: right; min-width: 60%; clear: both;">'
        + '<div class="direct-chat-info clearfix">' +
        '<span class="direct-chat-name pull-right">' + data.guestPhone + '</span>' +
        '<span class="direct-chat-timestamp pull-left">23 Jan 2:00 pm</span>\n' +
        '</div>'
        + '<img class="direct-chat-img" src="/img/avatar-default.png" alt="message user image">'
        + '<div class="direct-chat-text">' + data.message + '</div>'
        + '</div>').appendTo($('.direct-chat-messages'));
    // $('.message-input input').val(null);
    // $('.contact.active .preview').html('<span>You: </span>' + message);
        $('html, .box-body').animate({scrollTop:$(".direct-chat-messages").height()}, 'fast');
}




