import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { Md5 } from 'ts-md5/dist/md5';
import moment from 'moment';
import { LoadingController } from "ionic-angular";
import { Http, Headers, RequestOptions } from "@angular/http";
import { Geolocation } from "@ionic-native/geolocation";
import { FilePath } from '@ionic-native/file-path';

/*
  Generated class for the GlobalHeroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

@Injectable()
export class GlobalHeroProvider {
  parentServiceCatergories=[]
  subServicesOfParentService=[]
  isThisAppHasAdminRole=false;
  removeAreaCheckWhenAssinging=true;
  appInfo = {
    bundleID: '',
    localVersion: '',
    newestVersion: ''
  }
  prodOrDemo = false
  currentDashboardParams = {
    id: '',
    time: '',
    currentPickName: ''
  }
  pieChart1Data = [];
  provinceChartData = []
  currentTabInStatistic = ''

  baseURL = "https://orimx.vnptit.vn"
  baseURLDemo = "https://orimx.vnptit.vn"
  allCatergories = []
  tratTuList = []
  hatangList = []
  quyhoachList = []
  moitruongList = []
  imageURL = []
  picsWithLocation = []
  externalLinks = { "Display": false, "Links": [{ "url": "https://danhgiahailong.hochiminhcity.gov.vn/#/", "title": "Đánh giá hài lòng" }, { "url": "https://dichvucong.hochiminhcity.gov.vn/icloudgate/version4/ps/page/bs/home.cpx", "title": "Nộp và tra cứu hồ sơ trực tuyến" }] }
  baseURLForIcon = 'https://orimx.vnptit.vn/storage/'
  device_type = 'a'
  right_code = ""
  cityList = null
  wardList = null
  districtList = null
  caseDegreeList = null
  public firstTimeRunning = null;
  public number;
  islogin = false;
  currentCaseAddress = ""
  currentLat = null;
  currentLng = null;
  // testMode=1;
  loading;
  deviceID = ""
  currentUserPermission = null
  currentUserInfo = {
    accountID: 0,
    nguoidung_id: 0,
    donvi_id: 0,
    ten_donvi: '',
    username: "",
    ho_ten: "",
    token: "",
    email: "",
    didong: "",
    area: null
  }

  permissionCode = {
    NewIssue: 10,// tin báo mới					
    HandlingStart: 11, //Điều phối đến đơn vị tiếp nhận					
    ExecuteAssign: 12, //Phân công xử lý					
    SupportRequirements: 20,// Chuyên viên yêu cầu hỗ trợ					
    SupportDeptAssign: 21,//Phân công đơn vị hỗ trợ					
    SupportExecuteAssign: 22,//Đơn vị hỗ trợ phân công xử lý					
    ExecuteCompleted: 13,//Đã xử lý hoàn tất- cấp dưới báo					
    ApprovedComplete: 14,// Kết thúc luồng xử lý. Lãnh đạo phê duyệt cấp cuối cùng					
    RecycleIssueNotify: 33,// Báo tin rác- cấp dưới báo					
    RecycleIssue: 34,// Báo tin rác- phê duyệt cấp cuối cùng					
    OutOfBoundNotify: 43,// Ngoài khu vực quản lý- cấp dưới báo					
    OutOfBound: 44,// Ngoài khu vực quản lý - phê duyệt cấp cuối cùng					
    ExecuteCompletedSuport: 23//Đã hỗ trợ hoàn tất- cấp dưới báo					
  }
  imgBase = ""
  testMode = 0;
  //new endpoint:  http://221.132.39.229:8093/
  //old endpoint:  http://greencity.center2.vnptit.vn:8098/
  apiURL = {
    getListOfDashboardByCaseStatus:'https://orimx.vnptit.vn/kong/api/core/v1/crm/issue/get-by-condition',
    getCatergoriesBasedOnArea: 'https://orimx.vnptit.vn/kong/api/masterdata/v1/service/ListAllServices?areaCode=',
    support_confirm: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update-support',
    get_newsInfo_byRegion: 'https://orimx.vnptit.vn/kong/api/App/get_newsInfo_byRegion',
    Thong_Ke_Mobile: 'https://orimx.vnptit.vn/kong/api/core/v1/dashboard/all',
    func_View_HistoryNews: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/get-by-assignee-condition',
    func_View_HistoryNews__admin: 'https://orimx.vnptit.vn/kong/api/core/v1/crm/issue/get-by-condition',
    update_resolve_attachement: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update/resolveattachment',
    get_catergories_all: 'https://orimx.vnptit.vn/kong/api/core/v1/category/all',
    update_attachment: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update/attachment',
    img_to_db: 'https://orimx.vnptit.vn/kong/api/download/',
    function_gui_anh_xu_ly: 'https://orimx.vnptit.vn/kong/api/App/func_UpLoadImages_XuLy/?id=',
    func_Gui_DanhGia_Event: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update/rating',
    func_layQuaTrinhXuLy_theoSuCo: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/get-issue-processing',
    informCaseProcessingStartTime: 'https://orimx.vnptit.vn/kong/api/App/func_ClickView_Event',
    gui_video: 'https://orimx.vnptit.vn/kong/api/App/func_UpLoadVideo/?id=',
    gui_imgs_new: 'https://orimx.vnptit.vn/kong/api/upload/',
    func_XuLy_SuCo_HoanTat: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update-executed',
    func_LogOut: ' https://orimx.vnptit.vn/kong/api/permission/v1/account/logout',
    func_XuLy_SuCo_PheDuyetHoanThanh: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update-approved',
    func_PhanCong: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/assign-execute',
    func_XuLy_SuCo_BaoRac: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/update-recycleByLeader',
    fucn_laydsDonViPhancong: 'https://orimx.vnptit.vn/kong/api/permission/v1/department/get-sub-department?departmentId=',
    func_DsDonVi_CoTheHoTro: 'https://orimx.vnptit.vn/kong/api/permission/v1/department/get-department-support?departmentId=',
    func_DsNhanVien: 'https://orimx.vnptit.vn/kong/api/permission/v1/account/getuserbydepartmentid?departmentId=',
    func_laydsDM_MucDo: ' https://orimx.vnptit.vn/kong/api/App/func_laydsDM_MucDo',
    func_laydsPhuong_Quan: ' https://orimx.vnptit.vn/kong/api/App/func_laydsPhuong_Quan',
    func_laydsTinh: ' https://orimx.vnptit.vn/kong/api/App/func_laydsTinh',
    func_laydsQuan_All: 'https://orimx.vnptit.vn/kong/api/App/func_laydsQuan_All',
    func_laydsPhuong_All: 'https://orimx.vnptit.vn/kong/api/App/func_laydsPhuong_All',
    func_DsSuCo_ChuaXuLy: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/get-by-assignee-condition',
    func_ChangPass: 'https://orimx.vnptit.vn/kong/api/permission/v1/user/updatepassword',
    func_DsSuCo_DaGui: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/get-by-listid',
    func_GuiSuCo_New: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/create',
    func_DangKiThongTin_Vip: ' https://orimx.vnptit.vn/kong/api/App/func_DangKiThongTin_Vip',
    func_KiemTraPassword_BaBuoc: ' https://orimx.vnptit.vn/kong/api/App/func_KiemTraPassword_BaBuoc',
    func_KiemTraMaNguoiDung_BaBuoc: ' https://orimx.vnptit.vn/kong/api/App/func_KiemTraMaNguoiDung_BaBuoc',
    newLogin: 'https://orimx.vnptit.vn/kong/api/permission/v1/account/login',
    getAllCatergories: 'https://orimx.vnptit.vn/kong/api/masterdata/v1/service/ListAllServices',
    assignSupport: 'https://orimx.vnptit.vn/kong/api/core/v1/issue/assign-support',
    getPermissions: 'https://orimx.vnptit.vn/kong/api/permission/v1/permission/getlistmenubytoken?source=menu_app&token=',
    getRealTimeMarkers: 'https://orimx.vnptit.vn/kong/api/report_detail/v1/getIssueByAreacodeUnfinish',
    getTongIssue: 'https://orimx.vnptit.vn/kong/api/report/v1/date/area/total-issue',
    getDaXuLyIssue: 'https://orimx.vnptit.vn/kong/api/report/v1/date/area/issue/get-by-status',
    getChuaXuLyIssue: 'https://orimx.vnptit.vn/kong/api/report/v1/date/area/issue/get-by-status',
    getChuyenIssue: 'https://orimx.vnptit.vn/kong/api/report/v1/date/area/issue/forwarded',
    getSatisfactionRate: 'https://orimx.vnptit.vn/kong/api/report/v1/date/area/sastify',
    pushNotiFromServer: 'https://orimx.vnptit.vn/kong/api/core/hub/firebase',
    getAllSubAreas: 'https://orimx.vnptit.vn/kong/api/masterdata/v1/area/getListAreaByTypeAndParentCode',
    getAllMangersOfADepartment: 'https://orimx.vnptit.vn/kong/api/permission/v1/account/get-user-manage-by-departmentid?departmentId='
  };

  listOfPreferences = {
    DmPhanloaiXuly: [
      { id: 13, code: 'CX', name: 'Xử lý' },
      { id: 33, code: 'RAC', name: 'Tin rác' },
      { id: 20, code: 'HT', name: 'Cần hỗ trợ' },
      { id: 43, code: 'NKV', name: 'Ngoài khu vực xử lý' }
    ],
    DmThanhPho: [
      { id: 0, code: "HMC", name: "Hồ Chí Minh" },
      { id: 1, code: "HNI", name: "Hà Nội" },
      { id: 2, code: "DNG", name: "Đà Nẵng" }
    ],
    DmQuan: [
      { id: 0, city: "HMC", code: "Q1", name: "Quận 1" },
      { id: 1, city: "HMC", code: "Q2", name: "Quận 2" },
      { id: 2, city: "HMC", code: "Q3", name: "Quận 3" },
      { id: 3, city: "HMC", code: "Q4", name: "Quận 4" },
      { id: 4, city: "HMC", code: "Q5", name: "Quận 5" },
      { id: 5, city: "HMC", code: "Q6", name: "Quận 6" },
      { id: 6, city: "HMC", code: "Q7", name: "Quận 7" },
      { id: 7, city: "HMC", code: "Q8", name: "Quận 8" },
      { id: 8, city: "HMC", code: "Q9", name: "Quận 9" },
      { id: 9, city: "HMC", code: "Q10", name: "Quận 10" },
      { id: 10, city: "HMC", code: "Q11", name: "Quận 11" },
      { id: 11, city: "HMC", code: "Q12", name: "Quận 12" },
      { id: 12, city: "HMC", code: "QGV", name: "Quận Gò Vấp" },
      { id: 13, city: "HMC", code: "QCC", name: "Huyện Củ Chi" },
      { id: 14, city: "HMC", code: "QHM", name: "Huyện Hóc Môn" },
      { id: 15, city: "HMC", code: "QNB", name: "Quận Nhà Bè" },
      { id: 16, city: "HMC", code: "QBC", name: "Quận Bình Chánh" },
      { id: 17, city: "HMC", code: "QBT", name: "Quận Bình Tân" },
      { id: 18, city: "HMC", code: "QTB", name: "Quận Tân Bình" },
      { id: 19, city: "HMC", code: "QTD", name: "Quận Thủ Đức" },
      { id: 20, city: "HMC", code: "QBTH", name: "Quận Bình Thạnh" },
      { id: 21, city: "HMC", code: "QNB", name: "Huyện Nhà Bè" },
      { id: 22, city: "HMC", code: "QCG", name: "Huyện Orim-X Light" },
      { id: 23, city: "HMC", code: "QPN", name: "Quận Phú Nhuận" }
    ],

    DmLoaiViTri: [
      { id: 1, code: "self", name: "Vị trí hiện tại" },
      { id: 2, code: "orther", name: "Chọn trên bản đồ" }
    ],

    DmLoaiSuCo: [
      {
        id: 0,
        code: "TTBBTP",
        phanloai: "RKC",
        name: "Tụ tập buôn bán trái phép",
        noidung: "Tụ tập buôn bán trái phép"
      },
      {
        id: 1,
        code: "QCSQD",
        phanloai: "RKC",
        name: "Quảng cáo sai quy định",
        noidung: "Quảng cáo sai quy định"
      },
      {
        id: 2,
        code: "DXLCLLD",
        phanloai: "RKC",
        name: "Đậu xe lấn chiếm lòng lề đường",
        noidung: "Đậu xe lấn chiếm lòng lề đường"
      },
      {
        id: 3,
        code: "XDKP",
        phanloai: "KC",
        name: "Xây dựng không phép",
        noidung: "Xây dựng không phép"
      },
      {
        id: 4,
        code: "DRTSQD",
        phanloai: "KC",
        name: "Đổ rác thải sai quy định",
        noidung: "Đổ rác thải sai quy định"
      },
      {
        id: 5,
        code: "KHAC",
        phanloai: "TB",
        name: "Vấn đề khác",
        noidung: "Vấn đề khác"
      }
    ],

    DmMucDo: [
      { id: 0, code: "KKC", name: "Không khẩn cấp" },
      { id: 1, code: "TB", name: "Trung bình" },
      { id: 2, code: "KC", name: "Khẩn cấp" },
      { id: 3, code: "RKC", name: "Rất khẩn cấp" }
    ],

    DmPhanLoaiTin: [
      { id: 32, code: "CX", name: "Xử lý" },
      { id: 0, code: "RAC", name: "Tin rác" },
      { id: 2, code: "HT", name: "Cần hỗ trợ" }
    ],

    DmCachNhapDiaChi: [
      { id: 1, code: "1", name: "GPS" },
      { id: 2, code: "2", name: "Nhập địa chỉ" }
    ]
  };

  constructor(private filePath: FilePath,
    public geolocation: Geolocation, public http: Http, public storage: Storage, public loadingCtrl: LoadingController) {
    console.log("Hello GlobalHeroProvider Provider");
  }
  validateSpecialCharacter(str, type) {

    let format
    if (type == "content") {
      format = /[!@#$%&*()_+\-=\[\]{}\\|<>\/?]/;
      if (format.test(str))
        return {
          res: false,
          msg: "Vui lòng không sử dụng ký tự đặc biệt trong nội dung gửi"
        };
      else
        return {
          res: true,
          msg: ""
        };
    }
    else if (type == "phone") {
      format = new RegExp("^[0-9]*[1-9][0-9]*$");
      if (format.test(str) && str.length >= 10 && str.length <= 11 && str.startsWith('0'))
        return {
          res: true,
          msg: ""
        };
      else
        return {
          res: false,
          msg: 'Vui lòng nhập đúng định dạng số điện thoại'
        };
    }
    else if (type == "name") {
      format = new RegExp("^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
        "ẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệếỉịọỏốồổỗộớờởỡợ" +
        "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$");
      if (!format.test(str))
        return {
          res: false,
          msg: "Vui lòng chỉ sử dụng chữ cái khi gửi họ tên"
        };
      else
        return {
          res: true,
          msg: ""
        };
    }
    else {
      format = /[!@#$%&*()_+\-=\[\]{}\\|<>\/?]/;
      if (format.test(str))
        return {
          res: false,
          msg: "Vui lòng không sử dụng ký tự đặc biệt trong " + type
        };
      else
        return {
          res: true,
          msg: ""
        };
    }

  }
  convertNativeURI(path) {
    return new Promise((resolve, reject) => {
      this.filePath.resolveNativePath(path)
        .then(filePath => {
          console.log(filePath)
          resolve(filePath)
        })
        .catch(err => reject(err));
    }
    );
  }
  checkExternalLinksToDisplay() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = 'http://222.255.102.166:8098/api/App/func_ShowLink';
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            if (res.json().code == 1) {
              this.externalLinks = res.json().data;
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }
  dateDisplayFix(date) {
    date = moment(date).format('DD/MM/YYYY HH:mm:ss');
    return date;
  }
  md5Harshing(string) {
    console.log(string)
    console.log(Md5.hashStr(string))
    // console.log(Md5.hashStr(string, true))
    // console.log(Md5.hashAsciiStr(string))
    // console.log(Md5.hashAsciiStr(string, true))
    return Md5.hashStr(string);
  }
  updateImgUrl(e) {
    console.log(e)
    e.srcElement.src = "imgs/city.png"
  }
  compare2Dates(input) {
    let strParts = input.split('/');
    input = strParts[0] + ' ' + strParts[1] + ' ' + strParts[2];
    var format = 'DD MM YYYY';
    var today = moment().startOf('day');
    var begin = moment(input, format);
    if (today.diff(begin) < 0) {
      console.log(today.format(format) + ' is  before ' + begin.format(format));
      return 0
    } else {
      console.log(today.format(format) + ' is  after ' + begin.format(format));
      return 1
    }
  }

  searchDate(start, data) {
    console.log(start)
    let end = moment();
    start = moment(start, 'DD MM YYYY');
    console.log(start)
    console.log(end)
    let result = []
    for (let a of data) {
      console.log(a.createdOn)
      let date = moment(a.createdOn)
      if (!(date.isBefore(start) || date.isAfter(end))) { console.log('ok'); result.push(a) }
      else
        console.log('not ok');
    }
    return result;
  }
  generateColorsOnAuto() {

    var hexValues = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e"];

    function populate(a) {
      for (var i = 0; i < 6; i++) {
        var x = Math.round(Math.random() * 14);
        var y = hexValues[x];
        a += y;
      }
      return a;
    }

    var newColor1 = populate('#');
    var newColor2 = populate('#');
    var angle = Math.round(Math.random() * 360);

    var gradient = "linear-gradient(" + angle + "deg, " + newColor1 + ", " + newColor2 + ")";
    console.log(gradient)
    return gradient;

  }

  presentLoadingCustom(message) {
    if (message != '') {
      let content = '<img class="loadingImg" style="vertical-align: text-bottom" src="imgs/customSpinner.gif" /><h5>' + message + '</5>'
      this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: content,
      });
    }
    else {
      this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `<img class="loadingImg" src="imgs/customSpinner.gif" />`,
      });
    }

    this.loading.present();
  }
  cancleCustomLoading() {
    this.loading.dismissAll();
  }

  closeLoadingCustom() {
    this.loading.dismiss();
  }

  checkImgSrc(src) {
    // if (src.startsWith('http')) {
    //   return src.split(',');
    // }
    // else
    //   return [];
    if (src.length > 0) {
      let result = []
      for (let pic of src) {
        result.push(pic.src)
      }
      return result
    }
    else
      return []
  }

  getRealTimeLocation() {
    this.geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      })
      .then(resp => {
        this.picsWithLocation.push({ lat: resp.coords.latitude, lng: resp.coords.longitude })
      })
      .catch(error => {
        console.log(error)
        this.picsWithLocation.push({ lat: 0, lng: 0 })
      });
  }

  checkPermission(code) {
    let res = false;
    for (let right of this.currentUserPermission) {
      if (code == right.code) {
        res = true; break;
      }
    }
    return res;
  }

  pushNotification(message, target, action) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("Authorization", "key=AAAA6UwxO4k:APA91bEfHx9IOSl8lrvZUaKQ746ImB1lPzxUTh_8CHv2d_4XRbWa4g-odPwI5vxYvATN6UETqOb43YbyupXP2k6HoZDQalDkcWIFqwqidyX03VoF9z56zVSwyx0tzmqkdGVW7UDFC-EK")
      let apiURL = 'https://fcm.googleapis.com/fcm/send';
      let bodyContent

      if (action == "")
        action = "new_case"
      if (this.device_type == 'a') {
        bodyContent =
          {
            "data": {
              "title": "Thông báo",
              "body": message,
              "notId": 1,
              "action": action
            },
            "to": target,
            "content-available": 1,
            "badge": 1
          }
      }
      else {
        bodyContent = {
          "notification": {
            "body": message,
            "title": "Thông báo",
            "click_action": "FCM_PLUGIN_ACTIVITY"
          },
          "data": {
            "action": action,
          },
          "to": target,
          "priority": "high",
          "content_available": true,
          "badge": 1
        }
      }
      this.http
        .post(
          apiURL, bodyContent,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  pushNotificationFromServer(message, target, action) {
    debugger
    // return new Promise((resolve, reject) => {
    //   let headers = new Headers();
    //   headers.append("Content-Type", "application/json");
    //   let apiURL = this.apiURL.pushNotiFromServer;
    //   let bodypush = {
    //     userDeviceIds: target,
    //     heading: 'Thông báo',
    //     content: message,
    //     act: action
    //   }
    //   console.log(bodypush)
    //   this.http
    //     .post(
    //       apiURL, bodypush,
    //       { headers: headers }
    //     )
    //     .subscribe(
    //       res => {
    //         console.log(res.json());
    //         console.log('đã gửi push')
    //         resolve(res.json());
    //       },
    //       err => {
    //         console.log('gửi push fail')
    //         console.log(err);
    //         reject(err);
    //       }
    //     );
    // });
  }

  languageContent = {
    "menu": {
      "quantri": "Quản trị",
      "baosukien": "Báo phản ánh",
      "thongtin": "Thông tin",
      "sukiendagui": "Các phản ánh đã gửi",
      "dangnhap": "Đăng nhập",
      "huongdan": "Hướng dẫn",
      "donviphattrien": "Đơn vị phát triển",
      "phancong": "Phân công",
      "xuly": "Xử lý",
      "pheduyet": "Phê duyệt",
      "dashboard": "Dashboard",
      "thongtinnhanvien": "Thông tin nhân viên",
      "doimatkhau": "Đổi mật khẩu",
      "dangxuat": "Đăng xuất",
      "version": "Phiên bản"
    },
    "approval": {
      "header_title": "Các phản ánh cần phê duyệt",
      "today": "Ngày hôm nay",
      "picked_date": "Ngày chọn",
      "body_title": "Phản ánh chờ phê duyệt",
      "date_action": "Chọn ngày gửi",
      "reportFrom": "Người báo"
    },
    "assign": {
      "header_title": "Các phản ánh tiếp nhận",
      "today": "Ngày hôm nay",
      "picked_date": "Ngày chọn",
      "body_title1": "Phản ánh cần xử lý chưa phân công",
      "date_action": "Chọn ngày gửi",
      "reportFrom": "Người báo",
      "no_message": "Hiện không có tin báo nào",
      "body_title2": "Phản ánh cần hỗ trợ chưa phân công"
    },
    "assign_confirm": {
      "title1": "Thông tin phản ánh",
      "note": "Ghi chú",
      "report_from": "Người báo",
      "img_title": "Hình ảnh phản ánh",
      "title2": "Phân công xử lý",
      "employee": "Chuyên viên",
      "department": "Phòng ban",
      "title3": "Chọn nhân viên",
      "employee_list": "Nhân viên",
      "title4": "Chọn đơn vị",
      "department_list": "Đơn vị",
      "title5": "Nội dung phân công",
      "title6": "Yêu cầu hỗ trợ",
      "title7": "Chọn đơn vị",
      "support_content": "Nội dung yêu cầu hỗ trợ",
      "support_list": "Đơn vị hỗ trợ được chọn"
    },
    "process_confirm": {
      "title1": "Thông tin phản ánh",
      "report_from": "Người báo",
      "status": "Ghi chú",
      "assigned_content": "Nội dung phân công",
      "img_title": "Hình ảnh phản ánh",
      "location": "Xem vị trí phản ánh",
      "timeline": "Xem quá trình xử lý",
      "title2": "Thông tin xử lý",
      "process_type": "Loại xử lý",
      "process_comment": "Nội dung xử lý"
    },
    "approve_confirm": {
      "title1": "Thông tin phản ánh",
      "report_from": "Người báo",
      "status": "Ghi chú",
      "assigned_content": "Nội dung phân công",
      "img_title": "Hình ảnh phản ánh",
      "process_img_title": "Hình ảnh xử lý",
      "location": "Xem vị trí phản ánh",
      "timeline": "Xem quá trình xử lý",
      "title2": "Phê duyệt xử lý",
      "approve_comment": "Nội dung duyệt"
    },
    "confirm_footer": {
      "anhxulyBtn": "Đính kèm ảnh xử lý",
      "pheduyetBtn": "Phê Duyệt",
      "sendBtn": "Gửi",
      "spamBtn": "Báo rác"
    },
    "case_detail": {
      "imgError": "Hình ảnh đang được cập nhật...",
      "videoError": "Video đang được cập nhật...",
      "title": "Thông tin phản ánh",
      "report_from": "Người báo",
      "img_title": "Hình ảnh phản ánh",
      "process_img_title": "Hình ảnh xử lý",
      "location": "Xem vị trí phản ánh",
      "video_title": "Video phản ánh",
      "rating_title": " Ðánh giá quá trình xử lý",
      "rating_btn": "Gưi dánh giá",
      "rating_content": "Nội dung"
    },
    "changepass": {
      "header_title": "Ðổi mật khẩu",
      "new_pass": "Mật khẩu mới",
      "re_new_pass": "Nhhập mật khẩu mới",
      "update_btn": "Cập nhật"
    },
    "dashboard": {
      "from": "Từ ngày",
      "to": "Ðến ngày",
      "area": "KHU VỰC",
      "unit": "Ðơn vị",
      "all": "Toàn bộ",
      "allCases": "Tổng tin báo",
      "Processed": "Ðã xử lý",
      "Processing": "Chua xử lý",
      "Transfered": "Ðã chuyển"
    },
    "imgpicker": {
      "title1": "Chọn ảnh xử lý dính kèm",
      "title2": "Xin vui lòng chọn ảnh và xác nhận!",
      "imgPicker_Btn": "Chọn ảnh"
    },
    "intro": {
      "txt1": "THÀNH PHỐ AN TOÀN",
      "txt2": "Ðơn vị phát triển",
      "txt3": "HỆ THỐNG TUONG TÁC",
      "txt4": "Người dân - Chính quyền về Trật tự đô thị?",
      "txt5": "Với mục tiêu xây dựng môi trường đô thị trên địa bàn ngày càng xanh, sạch, đẹp, an toàn và văn minh. Ứng dụng di động 'Orim-X Light Trực Tuyến' là phương tiện để người dân cung cấp các thông tin vi phạm về tụ tập buôn bán, quảng cáo, xây dựng, lấn chiếm, đổ rác, xã nước thải,... giúp chính quyền nhanh chóng tiếp cận và xử lý theo quy định.",
      "txt6": "Hướng dẫn sử dụng",
      "txt7": "Nhập thông tin muốn báo cáo",
      "txt8": "Cung cấp thêm các thông tin bổ trợ: vị trí, hình ảnh,....",
      "txt9": "Gửi thông tin về hệ thống..",
      "txt10": "Ðiều khoản sử dụng",
      "txt11": "Tôi cam đoan những thông tin tôi cung cấp cho ứng dụng là đúng sự thật và tôi hoàn toàn chịu trách nhiệm trước pháp luật về thông tin này.",
      "agree_txt": "Tôi đã đọc và đồng ý sử dụng.",
      "start_btn": "Bắt đầu"
    },
    "login": {
      "header_title": "ÐĂNG NHẬP HỆ THỐNG",
      "username_placeholder": "Nhập tên đăng nhập...",
      "password_placeholder": "Nhập mật khẩu...",
      "login_btn": "Ðăng nhập",
      "subtitle": "THÔNG TIN ÐĂNG NHẬP",
      "username": "Tên đăng nhập",
      "fullname": "Họ và tên",
      "phonenumber": "Di động",
      "mail": "Email",
      "user_role": "Vai trò",
      "department": "Ðơn vị",
      "logout_btn": "Ðăng xuất"
    },
    "process": {
      "header_title": "Các phản ánh chờ xử lý/ hỗ trợ",
      "today": "Ngày hôm nay",
      "picked_date": "Ngày chọn",
      "body_title1": "Các phản ánh chờ xử lý",
      "date_action": "Chọn ngày gửi",
      "reportFrom": "Người báo",
      "no_message": "Hiện không có tin báo nào",
      "body_title2": "Các phản ánh chờ hỗ trợ"
    },
    "sentcases": {
      "header_title": "phản ánh",
      "today": "Ngày hôm nay",
      "picked_date": "Ngày chọn",
      "date_action": "Chọn ngày gửi",
      "body_title1": "phản ánh",
      "reportFrom": "Người báo",
      "case_note": "Thông tin phản hồi"
    },
    "submitdata": {
      "map_placeholder": "Tìm kiếm vị trí phản ánh...",
      "modal3_text1": "Xin hãy sử dụng ngôn ngữ một cách rõ ràng và phát âm tốt nhất có thể",
      "modal3_text2": "Hãy ấn dừng lại để ngưng thu âm.",
      "modal3_text3": "Có lỗi xảy ra, xin vui lòng thử lại.",
      "modal3_text4": "Thử lại",
      "modal4_text1": "Xin hãy sử dụng ngôn ngữ một cách rõ ràng và phát âm tốt nhất có thể",
      "modal4_text2": "Hãy ấn dừng lại để ngưng thu âm.",
      "modal4_text3": "Có lỗi xảy ra, xin vui lòng thử lại.",
      "modal4_text4": "Thử lại",
      "step3_sub_title": "Thông tin vụ việc",
      "case_content": "Nội dung thông báo",
      "fullname": "Họ và tên",
      "phonenumber": "Số điện thoại",
      "send_btn": "Gửi Tin Báo",
      "step1_title": "Bước 1: chụp ảnh/ quay phim",
      "step2_title": "Bước 2: vị trí phản ánh",
      "step3_title": "Bước 3: thông tin phản ánh"
    },
    "home": {
      "tab1_title": "Trật Tự",
      "tab2_title": "Quy Hoạch",
      "tab3_title": "Môi Trường" 
    },
    "timeline": {
      "department": 'Đơn vị',
      "header_title": "Quá trình xử lý",
      "employee": "Nhân viên",
      "content": "Nội dung"
    },
    "userinfo": {
      "header_title": "Thông tin người sử dụng",
      "fullname": "Họ và tên",
      "ID": "CMND",
      "phonenumber": "Số điện thoại",
      "businessName": "Tên doanh nghiệp",
      "address": "Ðịa chỉ",
      "update_btn": "Cập nhật thông tin"
    }
  };
  chosenLanguage = "vi";
  languageList = {
    "vi": {
      "menu": {
        "quantri": "Quản trị",
        "baosukien": "Báo phản ánh",
        "thongtin": "Thông tin",
        "sukiendagui": "Các phản ánh đã gửi",
        "dangnhap": "Đăng nhập",
        "huongdan": "Hướng dẫn",
        "donviphattrien": "Đơn vị phát triển",
        "phancong": "Phân công",
        "xuly": "Xử lý",
        "pheduyet": "Phê duyệt",
        "dashboard": "Dashboard",
        "thongtinnhanvien": "Thông tin nhân viên",
        "doimatkhau": "Đổi mật khẩu",
        "dangxuat": "Đăng xuất",
        "version": "Phiên bản"
      },
      "approval": {
        "header_title": "Các phản ánh cần phê duyệt",
        "today": "Ngày hôm nay",
        "picked_date": "Ngày chọn",
        "body_title": "Phản ánh chờ phê duyệt",
        "date_action": "Chọn ngày gửi",
        "reportFrom": "Người báo"
      },
      "assign": {
        "header_title": "Các phản ánh tiếp nhận",
        "today": "Ngày hôm nay",
        "picked_date": "Ngày chọn",
        "body_title1": "Phản ánh cần xử lý chưa phân công",
        "date_action": "Chọn ngày gửi",
        "reportFrom": "Người báo",
        "no_message": "Hiện không có tin báo nào",
        "body_title2": "Phản ánh cần hỗ trợ chưa phân công"
      },
      "assign_confirm": {
        "title1": "Thông tin phản ánh",
        "note": "Ghi chú",
        "report_from": "Người báo",
        "img_title": "Hình ảnh phản ánh",
        "title2": "Phân công xử lý",
        "employee": "Chuyên viên",
        "department": "Phòng ban",
        "title3": "Chọn nhân viên",
        "employee_list": "Nhân viên",
        "title4": "Chọn đơn vị",
        "department_list": "Đơn vị",
        "title5": "Nội dung phân công",
        "title6": "Yêu cầu hỗ trợ",
        "title7": "Chọn đơn vị",
        "support_content": "Nội dung yêu cầu hỗ trợ",
        "support_list": "Đơn vị hỗ trợ được chọn"
      },
      "process_confirm": {
        "title1": "Thông tin phản ánh",
        "report_from": "Người báo",
        "status": "Ghi chú",
        "assigned_content": "Nội dung phân công",
        "img_title": "Hình ảnh phản ánh",
        "location": "Xem vị trí phản ánh",
        "timeline": "Xem quá trình xử lý",
        "title2": "Thông tin xử lý",
        "process_type": "Loại xử lý",
        "process_comment": "Nội dung xử lý"
      },
      "approve_confirm": {
        "title1": "Thông tin phản ánh",
        "report_from": "Người báo",
        "status": "Ghi chú",
        "assigned_content": "Nội dung phân công",
        "img_title": "Hình ảnh phản ánh",
        "process_img_title": "Hình ảnh xử lý",
        "location": "Xem vị trí phản ánh",
        "timeline": "Xem quá trình xử lý",
        "title2": "Phê duyệt xử lý",
        "approve_comment": "Nội dung duyệt"
      },
      "confirm_footer": {
        "anhxulyBtn": "Đính kèm ảnh xử lý",
        "pheduyetBtn": "Phê Duyệt",
        "sendBtn": "Gửi",
        "spamBtn": "Báo rác"
      },
      "case_detail": {
        "imgError": "Hình ảnh đang được cập nhật...",
        "videoError": "Video đang được cập nhật...",
        "title": "Thông tin phản ánh",
        "report_from": "Người báo",
        "img_title": "Hình ảnh phản ánh",
        "process_img_title": "Hình ảnh xử lý",
        "location": "Xem vị trí phản ánh",
        "video_title": "Video phản ánh",
        "rating_title": " Ðánh giá quá trình xử lý",
        "rating_btn": "Gưi dánh giá",
        "rating_content": "Nội dung"
      },
      "changepass": {
        "header_title": "Ðổi mật khẩu",
        "new_pass": "Mật khẩu mới",
        "re_new_pass": "Nhhập mật khẩu mới",
        "update_btn": "Cập nhật"
      },
      "dashboard": {
        "from": "Từ ngày",
        "to": "Ðến ngày",
        "area": "KHU VỰC",
        "unit": "Ðơn vị",
        "all": "Toàn bộ",
        "allCases": "Tổng tin báo",
        "Processed": "Ðã xử lý",
        "Processing": "Chua xử lý",
        "Transfered": "Ðã chuyển"
      },
      "imgpicker": {
        "title1": "Chọn ảnh xử lý dính kèm",
        "title2": "Xin vui lòng chọn ảnh và xác nhận!",
        "imgPicker_Btn": "Chọn ảnh"
      },
      "intro": {
        "txt1": "THÀNH PHỐ AN TOÀN",
        "txt2": "Ðơn vị phát triển",
        "txt3": "HỆ THỐNG TUONG TÁC",
        "txt4": "Người dân - Chính quyền về Trật tự đô thị?",
        "txt5": "Với mục tiêu xây dựng môi trường đô thị trên địa bàn ngày càng xanh, sạch, đẹp, an toàn và văn minh. Ứng dụng di động 'Orim-X Light Trực Tuyến' là phương tiện để người dân cung cấp các thông tin vi phạm về tụ tập buôn bán, quảng cáo, xây dựng, lấn chiếm, đổ rác, xã nước thải,... giúp chính quyền nhanh chóng tiếp cận và xử lý theo quy định.",
        "txt6": "Hướng dẫn sử dụng",
        "txt7": "Nhập thông tin muốn báo cáo",
        "txt8": "Cung cấp thêm các thông tin bổ trợ: vị trí, hình ảnh,....",
        "txt9": "Gửi thông tin về hệ thống..",
        "txt10": "Ðiều khoản sử dụng",
        "txt11": "Tôi cam đoan những thông tin tôi cung cấp cho ứng dụng là đúng sự thật và tôi hoàn toàn chịu trách nhiệm trước pháp luật về thông tin này.",
        "agree_txt": "Tôi đã đọc và đồng ý sử dụng.",
        "start_btn": "Bắt đầu"
      },
      "login": {
        "header_title": "ÐĂNG NHẬP HỆ THỐNG",
        "username_placeholder": "Nhập tên đăng nhập...",
        "password_placeholder": "Nhập mật khẩu...",
        "login_btn": "Ðăng nhập",
        "subtitle": "THÔNG TIN ÐĂNG NHẬP",
        "username": "Tên đăng nhập",
        "fullname": "Họ và tên",
        "phonenumber": "Di động",
        "mail": "Email",
        "user_role": "Vai trò",
        "department": "Ðơn vị",
        "logout_btn": "Ðăng xuất"
      },
      "process": {
        "header_title": "Các phản ánh chờ xử lý/ hỗ trợ",
        "today": "Ngày hôm nay",
        "picked_date": "Ngày chọn",
        "body_title1": "Các phản ánh chờ xử lý",
        "date_action": "Chọn ngày gửi",
        "reportFrom": "Người báo",
        "no_message": "Hiện không có tin báo nào",
        "body_title2": "Các phản ánh chờ hỗ trợ"
      },
      "sentcases": {
        "header_title": "phản ánh",
        "today": "Ngày hôm nay",
        "picked_date": "Ngày chọn",
        "date_action": "Chọn ngày gửi",
        "body_title1": "phản ánh",
        "reportFrom": "Người báo",
        "case_note": "Thông tin phản hồi"
      },
      "submitdata": {
        "map_placeholder": "Tìm kiếm vị trí phản ánh...",
        "modal3_text1": "Xin hãy sử dụng ngôn ngữ một cách rõ ràng và phát âm tốt nhất có thể",
        "modal3_text2": "Hãy ấn dừng lại để ngưng thu âm.",
        "modal3_text3": "Có lỗi xảy ra, xin vui lòng thử lại.",
        "modal3_text4": "Thử lại",
        "modal4_text1": "Xin hãy sử dụng ngôn ngữ một cách rõ ràng và phát âm tốt nhất có thể",
        "modal4_text2": "Hãy ấn dừng lại để ngưng thu âm.",
        "modal4_text3": "Có lỗi xảy ra, xin vui lòng thử lại.",
        "modal4_text4": "Thử lại",
        "step3_sub_title": "Thông tin vụ việc",
        "case_content": "Nội dung thông báo",
        "fullname": "Họ và tên",
        "phonenumber": "Số điện thoại",
        "send_btn": "Gửi Tin Báo",
        "step1_title": "Bước 1: chụp ảnh/ quay phim",
        "step2_title": "Bước 2: vị trí phản ánh",
        "step3_title": "Bước 3: thông tin phản ánh"
      },
      "home": {
        "tab1_title": "Trật Tự",
        "tab2_title": "Quy Hoạch",
        "tab3_title": "Môi Trường"
      },
      "timeline": {
        "department": 'Đơn vị',
        "header_title": "Quá trình xử lý",
        "employee": "Nhân viên",
        "content": "Nội dung"
      },
      "userinfo": {
        "header_title": "Thông tin người sử dụng",
        "fullname": "Họ và tên",
        "ID": "CMND",
        "phonenumber": "Số điện thoại",
        "businessName": "Tên doanh nghiệp",
        "address": "Ðịa chỉ",
        "update_btn": "Cập nhật thông tin"
      }
    },
    "en": {
      "menu": {
        "quantri": "Management",
        "baosukien": "Event report",
        "thongtin": "Information",
        "sukiendagui": "Events sent",
        "dangnhap": "Login",
        "huongdan": "Instructions",
        "donviphattrien": "Development unit",
        "phancong": "Assign",
        "xuly": "Handle",
        "pheduyet": "Approve",
        "dashboard": "Dashboard",
        "thongtinnhanvien": "Staff information",
        "doimatkhau": "Change password",
        "dangxuat": "Sign out",
        "version": "Version"
      },
      "approval": {
        "header_title": "Events to approve",
        "today": "Today",
        "picked_date": "Selected date",
        "body_title": "Approval pending",
        "date_action": "Select date to send",
        "reportFrom": "Reporter"
      },
      "assign": {
        "header_title": "Receiving events",
        "today": "Today",
        "picked_date": "Selected date",
        "body_title1": "The incident to be processed has not been assigned",
        "date_action": "Select date to send",
        "reportFrom": "Reporter",
        "no_message": "There are currently no reports",
        "body_title2": "The incident needs support has not been assigned"
      },
      "assign_confirm": {
        "title1": "Event information",
        "note": "Notes",
        "report_from": "Reporter",
        "img_title": "Image of the incident",
        "title2": "Assign processing",
        "employee": "Expert",
        "department": "Department",
        "title3": "Select staff",
        "employee_list": "Employee",
        "title4": "Select unit",
        "department_list": "Unit",
        "title5": "Assignment content",
        "title6": "Request support",
        "title7": "Select unit",
        "support_content": "Support request content",
        "support_list": "Support unit selected"
      },
      "process_confirm": {
        "title1": "Event information",
        "report_from": "Reporter",
        "status": "Notes",
        "assigned_content": "Assigned content",
        "img_title": "Image of the incident",
        "location": "See job location",
        "timeline": "See processing",
        "title2": "Processing information",
        "process_type": "Process Type",
        "process_comment": "Processing content"
      },
      "approve_confirm": {
        "title1": "Event information",
        "report_from": "Reporter",
        "status": "Notes",
        "assigned_content": "Assigned content",
        "img_title": "Image of the incident",
        "process_img_title": "Image processing",
        "location": "See job location",
        "timeline": "See processing",
        "title2": "Processing approval",
        "approve_comment": "Approved Content"
      },
      "confirm_footer": {
        "anhxulyBtn": "Attach processing image",
        "pheduyetBtn": "Approve",
        "sendBtn": "Send",
        "spamBtn": "Report spam"
      },
      "case_detail": {
        "imgError": "Pictures are in processing...",
        "videoError": "Video is in processing...",
        "title": "Event information",
        "report_from": "Reporter",
        "img_title": "Image of the incident",
        "process_img_title": "Image processing",
        "location": "See job location",
        "video_title": "Video incident",
        "rating_title": "Evaluating processing process",
        "rating_btn": "Rate",
        "rating_content": "Content"
      },
      "changepass": {
        "header_title": "Change password",
        "new_pass": "New password",
        "re_new_pass": "Enter new password",
        "update_btn": "Update"
      },
      "dashboard": {
        "from": "From",
        "to": "To",
        "area": "AREA",
        "unit": "Unit",
        "all": "All",
        "allCases": "Total news",
        "Processed": "Processed",
        "Processing": "Processing",
        "Transfered": "Moved"
      },
      "imgpicker": {
        "title1": "Choose attached processing photos",
        "title2": "Please select photos and confirm!",
        "imgPicker_Btn": "Choose photos"
      },
      "intro": {
        "txt1": "SAFE CITY",
        "txt2": "Development unit",
        "txt3": "WORKING SYSTEM",
        "txt4": "People - Government of Urban Order?",
        "txt5": "With the aim of building an urban environment in an increasingly green, clean, beautiful, safe and civilized area, the mobile application 'Orim-X Light Trực Tuyến' is a means for people to provide information. Violation news about gathering, trading, building, encroaching, garbage dumping, waste water communes, ... help the government quickly approach and handle according to regulations. ",
        "txt6": "User guide",
        "txt7": "Enter the information you want to report",
        "txt8": "Provide additional information: location, image, ....",
        "txt9": "Send information about the system ..",
        "txt10": "Terms of use",
        "txt11": "I certify that the information I provide to the application is true and I am fully responsible to the law for this information.",
        "agree_txt": "I have read and agree to use.",
        "start_btn": "Start"
      },
      "login": {
        "header_title": "LOG IN SYSTEM",
        "username_placeholder": "Enter your username ...",
        "password_placeholder": "Enter password ...",
        "login_btn": "Login",
        "subtitle": "USER INFORMATION",
        "username": "Login name",
        "fullname": "Full name",
        "phonenumber": "Mobile",
        "mail": "Email",
        "user_role": "Role",
        "department": "Positioning",
        "logout_btn": "Logout"
      },
      "process": {
        "header_title": "Events pending / support",
        "today": "Today",
        "picked_date": "Selected date",
        "body_title1": "Pending matters",
        "date_action": "Select date to send",
        "reportFrom": "Reporter",
        "no_message": "There are currently no reports",
        "body_title2": "Events waiting for support"
      },
      "sentcases": {
        "header_title": "Incident",
        "today": "Today",
        "picked_date": "Selected date",
        "date_action": "Select date to send",
        "body_title1": "Event",
        "reportFrom": "Reporter",
        "case_note": "Feedback"
      },
      "submitdata": {
        "map_placeholder": "Search for event location ...",
        "modal3_text1": "Please use the language clearly and pronounce it as best as possible",
        "modal3_text2": "Press stop to stop recording.",
        "modal3_text3": "An error has occurred, please try again.",
        "modal3_text4": "Retry",
        "modal4_text1": "Please use the language clearly and pronounce it as best as possible",
        "modal4_text2": "Press stop to stop recording.",
        "modal4_text3": "An error has occurred, please try again.",
        "modal4_text4": "Retry",
        "step3_sub_title": "Case information",
        "case_content": "Message content",
        "fullname": "Full name",
        "phonenumber": "Phone number",
        "send_btn": "Send Message",
        "step1_title": "Step 1: take pictures / videos",
        "step2_title": "Step 2: location of the job",
        "step3_title": "Step 3: incident information"
      },
      "home": {
        "tab1_title": "Order",
        "tab2_title": "Planning",
        "tab3_title": "Enviroment"
      },
      "timeline": {
        "department": 'Department',
        "header_title": "Processing",
        "employee": "Employee",
        "content": "Content"
      },
      "userinfo": {
        "header_title": "User information",
        "fullname": "Full name",
        "ID": "ID",
        "phonenumber": "Phone number",
        "businessName": "Business name",
        "address": "Address",
        "update_btn": "Update information"
      }
    }
  }
}
