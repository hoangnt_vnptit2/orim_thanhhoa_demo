import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { Storage } from '@ionic/storage';
import { GlobalHeroProvider } from '../providers/global-hero/global-hero';
import { PhoneVerificationPage } from '../pages/phone-verification/phone-verification';
import { SecondhomePage } from '../pages/secondhome/secondhome';
import { IntroPage } from '../pages/intro/intro';
import { ThirdHomePage } from '../pages/third-home/third-home';
import { UserinfoPage } from '../pages/userinfo/userinfo';
import { SentcasesPage } from '../pages/sentcases/sentcases';
import { LoginPage } from '../pages/login/login';
import { AssignPage } from '../pages/assign/assign';
import { ProcessPage } from '../pages/process/process';
import { ApprovalPage } from '../pages/approval/approval';
import { EmployeeinfoPage } from '../pages/employeeinfo/employeeinfo';
import { ChangepassPage } from '../pages/changepass/changepass';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { Http, Headers, RequestOptions } from '@angular/http';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { StatisticPage } from '../pages/statistic/statistic'
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: Array<{ title: string, component: any }>;

  constructor(private screenOrientation: ScreenOrientation,private http: Http, private push: Push, public alertCtrl: AlertController, private menuCtrl: MenuController, public global: GlobalHeroProvider, private storage: Storage, private imagePicker: ImagePicker, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    console.log(this.global.firstTimeRunning)
    // used for an example of ngFor and navigation
    this.pages = [
      // { title: 'Home', component: HomePage },
      { title: 'Home', component: SecondhomePage },
      { title: 'List', component: ListPage }
    ];
    this.userInfoSetUp();
  }
  languageChanged(lan) {
    this.global.chosenLanguage = lan;
    if (lan == 'vi')
      this.global.languageContent = this.global.languageList.vi;
    else
      this.global.languageContent = this.global.languageList.en;
  }
  promiseTest() {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        let randomNum = Math.floor(50 + (Math.random() * (100 - 50 + 1)))
        if (randomNum % 2 == 0)
          resolve('even: ' + randomNum);
        else
          reject('odd: ' + randomNum);
      }, 1000);
    });
  }
  userInfoSetUp() {
    for (let i = 0; i < 10; ++i) {
      this.promiseTest().then((res) => {
        console.log(res)
      })
        .catch((err) => {
          console.log(err)
        })
    }

    this.storage.get('autoLogin').then((val) => {
      if (val == null) {
        this.storage.set("autoLogin", false);
      }
    });
    this.storage.get('idEvents').then((val) => {
      if (val == null) {
        let id = [];
        this.storage.set("idEvents", JSON.stringify(id));
      }
    });
    this.storage.get('userName').then((val) => {
      if (val == null) {
        this.storage.set("userName", "")
      }
    });
    this.storage.get('userPhone').then((val) => {
      if (val == null) {
        this.storage.set("userPhone", "")
      }
    });
    this.storage.get('userCMND').then((val) => {
      if (val == null) {
        this.storage.set("userCMND", "")
      }
    });
    this.storage.get('userBusinessName').then((val) => {
      if (val == null) {
        this.storage.set("userBusinessName", "")
      }
    });
    this.storage.get('userWard').then((val) => {
      if (val == null) {
        this.storage.set("userWard", "")
      }
    });
    this.storage.get('userCity').then((val) => {
      if (val == null) {
        this.storage.set("userCity", "")
      }
    });
    this.storage.get('userDistrict').then((val) => {
      if (val == null) {
        this.storage.set("userDistrict", "")
      }
    });
    this.storage.get('userAddress').then((val) => {
      if (val == null) {
        this.storage.set("userAddress", "")
      }
    });
  }

  openPage(page) {
    console.log(page)
    switch (page) {
      case 'statistic':
        if (this.nav.getActive().component != StatisticPage) {
          this.nav.push(StatisticPage);
        }       
        break;
      case 'dashboard':
        if (this.nav.getActive().component != DashboardPage) {
          this.nav.push(DashboardPage);
        }      
        break;
      case 'login':
        if (this.nav.getActive().component != LoginPage) {
          this.nav.push(LoginPage);
        }      
        break;
      case 'post':
        if (this.nav.getActive().component != ThirdHomePage) {
          this.nav.popToRoot();
        }
        break;
      case 'userInfo':
        if (this.nav.getActive().component != UserinfoPage) {
          this.nav.push(UserinfoPage);
        }
        break;
      case 'sentCases':
        if (this.nav.getActive().component != SentcasesPage) {
          this.nav.push(SentcasesPage);
        }
        break;
      case 'intro':
        if (this.nav.getActive().component != IntroPage) {
          this.nav.push(IntroPage);
        }
        break;
      case 'assign':
        if (this.nav.getActive().component != AssignPage) {
          this.nav.push(AssignPage);
        }
        break;
      case 'process':
        if (this.nav.getActive().component != ProcessPage) {
          this.nav.push(ProcessPage);
        }
        break;
      case 'aprroval':
        if (this.nav.getActive().component != ApprovalPage) {
          this.nav.push(ApprovalPage);
        }
        break;
      case 'employeeInfo':
        if (this.nav.getActive().component != LoginPage) {
          this.nav.push(LoginPage);
        }
        break;
      case 'changePass':
        if (this.nav.getActive().component != ChangepassPage) {
          this.nav.push(ChangepassPage);
        }
        break;
      case 'logout':
        {
          let alert = this.alertCtrl.create({
            title: 'ORIM-X',
            message: 'Bạn muốn thoát khỏi chương trình?',
            buttons: [
              {
                text: 'Không',
                role: 'cancel',
                handler: () => {
                  alert.dismiss();
                }
              },
              {
                text: 'Có',
                handler: () => {
                  //do the logout here    
                  this.logout()
                  this.global.islogin = false
                  this.storage.set('autoLogin', false);
                  this.menuCtrl.enable(true, "menuBeforeLogin");
                  this.menuCtrl.enable(false, "menuAfterLogin");
                  this.nav.popToRoot();
                }
              }
            ]
          });
          alert.present();
          break;
        }
    }
    this.menuCtrl.close()
  }

  logout() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_LogOut;
      this.http
        .post(
          apiURL,
          {
            source: 'mobile'
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            console.log('logout ok')
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      console.log(navigator.userAgent)
      if(navigator.userAgent.toLowerCase().indexOf('crosswalk') > -1) {
       console.log('cross')
    } else {
      console.log('no-cross')
    }
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.storage.get('currentLanguage').then((val) => {
        if (val == null) {
          this.storage.set("currentLanguage", 'vi');
          this.languageChanged('vi')
        }
        else {
          this.global.chosenLanguage = val;
          this.languageChanged(val)
        }
      });

      this.platform.pause.subscribe(() => {
        this.storage.set("currentLanguage", this.global.chosenLanguage);
      });

      this.global.checkExternalLinksToDisplay();

      if (this.platform.is('ios'))
        this.global.device_type = 'i'
      else
        this.global.device_type = 'a'
      console.log('Thiết bị: ' + this.global.device_type)

      this.storage.get('1stTime').then((val) => {

        this.splashScreen.hide();
        if (val == null) {
          this.storage.set('1stTime', '1');
          this.rootPage = IntroPage
        }
        else {
          this.storage.get('agreement').then((val) => {
            if (val == null) {
              this.rootPage = IntroPage
            }
            else
              this.rootPage = ThirdHomePage
          });
        }
      });

      this.storage.get('DeviceToken').then((val) => {
        if (val == null) {
          console.log(val)
          this.storage.set("DeviceToken", "test123456");
          this.global.deviceID = "test123456"
          this.initPushNotification();
        }
        else {
          this.global.deviceID = val
          this.initPushNotification();
        }
      });

      this.statusBar.styleDefault();

    });
  }

  initPushNotification() {

    // to check if we have permission
    this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');

        } else {
          console.log('We don\'t have permission to send push notifications');
        }
      });

    // check the platform we're running on
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }

    // to initialize push notifications
    const options: PushOptions = {
      android: {
        senderID: '1002005674889',
        iconColor: '#1E88E5',
      },
      ios: {
        // senderID: '1002005674889',
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };

    const pushObject: PushObject = this.push.init(options);
    let topic = "greenhcmAndroid"

    pushObject.subscribe(topic).then((res: any) => {
      console.log("Success")
    }).catch((error: any) => {
      console.log("Failed")
    })

    if (this.platform.is('ios')) {
    }
    else {
      pushObject.on('notification').subscribe((data: any) => {
        console.log('push cho android')
        pushObject.getApplicationIconBadgeNumber().then((res: any) => {
          console.log("Current Badge number: " + res)
          pushObject.setApplicationIconBadgeNumber(1).then((res: any) => {
            console.log("Set Badge Success")
          }).catch((error: any) => {
            console.log("Set Badge Failed")
          })
        }).catch((error: any) => {
          console.log("Get Badge Failed")
        })
        let confirmAlert = this.alertCtrl.create({
          title: data.title,
          message: data.message,
          buttons: [{
            text: 'Bỏ qua',
            role: 'cancel',
            handler: () => {
            }
          }, {
            text: 'Đồng ý',
            role: 'cancel',
            handler: () => {
              if (this.global.islogin) {
                if (data.additionalData.action == "điều phối") {
                  this.nav.push(AssignPage)
                }
                else if (data.additionalData.action == "xử lý") {
                  this.nav.push(ProcessPage)
                }
                else {
                  this.nav.push(ApprovalPage)
                }
              }
              else {
                alert('Xin vui lòng đăng nhập vào tài khoản đã sử dụng gần nhất trên thiết bị này để xem tin báo!')
              }
            }
          }]
        });
        confirmAlert.present();
      });
    }

    pushObject.on('registration').
      subscribe((registration: any) => {
        if (registration.registrationId == null) {
          this.storage.set("DeviceToken", "test123456");
          this.global.deviceID = "test123456"
        }
        else {
          this.storage.set("DeviceToken", registration.registrationId);
          this.global.deviceID = registration.registrationId;
          console.log('Device registered', registration)
        }
      });

    pushObject.on('error').
      subscribe(error =>
        console.error('Error with Push plugin', error));
    this.platform.resume.subscribe(() => {
      // Handle event on resume
      pushObject.setApplicationIconBadgeNumber(0);
    });
  }
}
