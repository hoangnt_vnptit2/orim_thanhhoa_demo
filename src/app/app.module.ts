import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture';
import { Base64 } from '@ionic-native/base64';
import { SecondhomePage } from '../pages/secondhome/secondhome';
import { Map15Page } from '../pages/map15/map15';
import { IntroPage } from '../pages/intro/intro';
import { ThirdHomePage } from '../pages/third-home/third-home';
import { SubmitdataPage } from '../pages/submitdata/submitdata';
import { UserinfoPage } from '../pages/userinfo/userinfo';
import { AssignPage } from '../pages/assign/assign';
import { AssignconfirmPage } from '../pages/assignconfirm/assignconfirm';
import { SentcasesPage } from '../pages/sentcases/sentcases';
import { LoginPage } from '../pages/login/login';
import { ChatHistoryPage } from '../pages/chat-history/chat-history';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SmsPage } from '../pages/sms/sms';
import { ChatRoomPage } from '../pages/chat-room/chat-room';
import { ListPage } from '../pages/list/list';
import { PhoneVerificationPage } from '../pages/phone-verification/phone-verification';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { MediaPage } from '../pages/media/media'
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { File } from '@ionic-native/file';
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { GlobalHeroProvider } from '../providers/global-hero/global-hero';
import { CalendarModule } from 'ionic3-calendar-en';
import { ProcessPage } from '../pages/process/process';
import { ApprovalPage } from '../pages/approval/approval';
import { EmployeeinfoPage } from '../pages/employeeinfo/employeeinfo';
import { ChangepassPage } from '../pages/changepass/changepass';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { MapReviewPage } from '../pages/map-review/map-review';
import { CaseDetailPage } from '../pages/case-detail/case-detail';
import { ImgviewerPage } from '../pages/imgviewer/imgviewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { TimelineDisplayPage } from '../pages/timeline-display/timeline-display';
import { Ionic2RatingModule } from 'ionic2-rating';
import { ImgpickerPage } from '../pages/imgpicker/imgpicker';
import { DashboardPage } from '../pages/dashboard/dashboard';
// import { GoogleChartsModule } from 'angular-google-charts';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { Market } from '@ionic-native/market';

import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { Submitdata2Page } from '../pages/submitdata2/submitdata2';
import { CameraPreview } from '@ionic-native/camera-preview';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import { DatePicker } from '@ionic-native/date-picker';
import { FilePath } from '@ionic-native/file-path';
import {DashboardDetailPage} from '../pages/dashboard-detail/dashboard-detail'
import {DashboardListPage} from '../pages/dashboard-list/dashboard-list'
import { StatisticPage } from '../pages/statistic/statistic'
import {StatisticDetailPage} from '../pages/statistic-detail/statistic-detail'
// import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
// const config: SocketIoConfig = { url: 'ws://222.255.102.150:3000', options: {query: 'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX3Bob25lbnVtYmVyIjoiMDkwODAwMTAwMCIsImV4cCI6IkpvaG4gRG9lIn0.BaXKCuIediLfTaZ6S5m0sE2UtMn6b_GX-dVJYzFYxhE'} };
import { AppVersion } from '@ionic-native/app-version';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@NgModule({
  declarations: [
    StatisticPage,
    StatisticDetailPage,
    DashboardDetailPage,
    DashboardListPage,
    Submitdata2Page,
    DashboardPage,
    ImgpickerPage,
    TimelineDisplayPage,
    CaseDetailPage,
    MapReviewPage,
    SubmitdataPage,
    MyApp,
    HomePage,
    ListPage,
    MediaPage,
    PhoneVerificationPage,
    SmsPage,
    ChatRoomPage,
    ChatHistoryPage,
    SecondhomePage,
    Map15Page,
    IntroPage,
    ThirdHomePage,
    UserinfoPage,
    SentcasesPage,
    LoginPage,
    AssignPage,
    AssignconfirmPage,
    ProcessPage, ApprovalPage, EmployeeinfoPage, ChangepassPage,
    ImgviewerPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CalendarModule,
    Ionic2RatingModule,
    Ng2GoogleChartsModule
    // GoogleChartsModule.forRoot(),
    // SocketIoModule.forRoot(config)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    StatisticPage,
    StatisticDetailPage,
    DashboardDetailPage,
    DashboardListPage,
    Submitdata2Page,
    DashboardPage,
    ImgpickerPage,
    TimelineDisplayPage,
    ImgviewerPage,
    CaseDetailPage,
    MapReviewPage,
    UserinfoPage,
    SubmitdataPage,
    ThirdHomePage,
    IntroPage,
    MyApp,
    HomePage,
    ListPage,
    MediaPage,
    PhoneVerificationPage,
    SmsPage,
    ChatRoomPage,
    ChatHistoryPage, SecondhomePage,
    Map15Page,
    SentcasesPage,
    LoginPage,
    AssignPage,
    AssignconfirmPage,
    ProcessPage, ApprovalPage, EmployeeinfoPage, ChangepassPage
  ],
  providers: [
    ScreenOrientation,
    AppVersion,
    Market,  
    FilePath,
    DatePicker,
    CameraPreview,Base64ToGallery,  
    SpeechRecognition,
    InAppBrowser,
    Push,
    NativeGeocoder,
    AndroidPermissions,
    OpenNativeSettings,
    Diagnostic,
    File,
    Base64,
    ImagePicker,
    MediaCapture,
    Camera,
    Geolocation,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    GlobalHeroProvider
  ]
})
export class AppModule { }
