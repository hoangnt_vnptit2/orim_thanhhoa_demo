import { Component, ViewChild, NgZone } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  LoadingController, Slides, MenuController, ActionSheetController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { ElementRef } from '@angular/core';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { ImgviewerPage } from '../imgviewer/imgviewer';
//from old media
import { Camera, CameraOptions } from "@ionic-native/camera";
import {
  MediaCapture,
  MediaFile,
  CaptureError,
  CaptureImageOptions,
  CaptureVideoOptions
} from "@ionic-native/media-capture";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import "rxjs/add/operator/map";
import { Base64 } from "@ionic-native/base64";
import { File, FileEntry } from "@ionic-native/file";
declare var require: any;
declare var google;
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { CameraPreview, CameraPreviewPictureOptions, CameraPreviewOptions, CameraPreviewDimensions } from '@ionic-native/camera-preview';
import { Base64ToGallery, Base64ToGalleryOptions } from '@ionic-native/base64-to-gallery';

/**
 * Generated class for the Submitdata2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-submitdata2',
  templateUrl: 'submitdata2.html',
})
export class Submitdata2Page {

  @ViewChild('slidesImg') slidesImg: Slides;
  @ViewChild('slidesBtn') slidesBtn: Slides;
  masterFormdataToSend = new FormData();
  loading
  //Step1
  currentStep = 1
  listHasElements = false;
  hasVideos = false;
  imageURL = [];
  public getWidth: number;
  public getHeight: number;
  public calcWidth: number;
  cameraPreviewOpts: CameraPreviewOptions
  videoFilePath;
  rawVideoPath;
  PERMISSION = {
    WRITE_EXTERNAL: this.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
    CAMERA: this.diagnostic.permission.CAMERA,
    ACCESS_FINE_LOCATION: this.diagnostic.permission.ACCESS_FINE_LOCATION,
    ACCESS_COARSE_LOCATION: this.diagnostic.permission.ACCESS_COARSE_LOCATION,
  };

  //Step2
  @ViewChild('map') mapElement: ElementRef;
  voiceSearchModal = false;
  hasGPS = false
  geoCoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  map: any;
  markers = []
  currentLat
  currentLng
  mapOptions = {
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    styles: [
      {
        featureType: 'administrative',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit',
        // elementType: 'labels.icon',
        stylers: [{ visibility: 'off' }]
      },

    ]
  }

  //Step3

  contactName = '';
  contactPhone = '';
  caseContent = ''
  caseSentSuccessMsg = ''
  currentIDToSendVideos = ''
  subCatergory
  constructor(
    public cameraPreview: CameraPreview, private zone: NgZone, private base64ToGallery: Base64ToGallery,
    public speechRecognition: SpeechRecognition,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation,
    public loadingCtrl: LoadingController,
    private file: File,
    private base64: Base64,
    private imagePicker: ImagePicker,
    public sanitizer: DomSanitizer,
    private mediaCapture: MediaCapture,
    private camera: Camera,
    private menu: MenuController,
    private nativeGeocoder: NativeGeocoder,
    public actionSheetCtrl: ActionSheetController) {

    this.requestAllPermissions()
    this.zone.run(() => {
      this.getWidth = window.innerWidth;

      this.getHeight = window.innerHeight;
    });
    console.log('width', this.getWidth);

    this.calcWidth = this.getWidth - 80;  // Calculate the width of device and substract 80 from device width;

    console.log('calc width', this.calcWidth);
    this.cameraPreviewOpts = {
      x: 0,
      y: 0,
      width: window.innerWidth,
      height: 300,
      toBack: true,
      previewDrag: false,
      tapPhoto: true,
      camera: 'rear',
    }
  }

  ionViewDidEnter() {
    this.startCamera()
    this.storage.get("userName").then(val => {
      if (val != null || val != "") this.contactName = val;
    });
    this.storage.get("userPhone").then(val => {
      if (val != null || val != "") this.contactPhone = val;
    });
    this.currentLat = this.navParams.get('currentlat');
    this.currentLng = this.navParams.get('currentlng');
    this.hasGPS = this.navParams.get('hasGPS')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Submitdata2Page');
    if (this.caseContent == '')
      this.caseContent = this.navParams.get('type')
    this.subCatergory = this.navParams.get('subCatergory')
    console.log(this.subCatergory)
  }

  ionViewWillLeave() {
    this.stopCamera();
    this.global.currentCaseAddress = "";
  }

  ionViewDidLeave() {
    this.stopCamera();
  }
  previousStep() {
    console.log('bước hiện tại: ' + this.currentStep)
    if (this.currentStep == 1)
      this.navCtrl.pop()
    else if (this.currentStep == 2) {
      this.currentStep = this.currentStep - 1;
      this.startCamera()
    }
    else
      this.currentStep = this.currentStep - 1;

    if (this.currentStep == 2)
      this.step2Pepare()
  }
  nextStep() {
    console.log('bước hiện tại: ' + this.currentStep)
    if (this.currentStep == 1) {
      if (this.imageURL.length == 0 && (this.rawVideoPath == null || this.rawVideoPath == ''))
        this.presentToast('Xin vui lòng cung cấp ít nhất 1 bức ảnh hoặc video về sự việc.')
      else {
        this.stopCamera();
        this.currentStep = this.currentStep + 1
      }

      // this.stopCamera();
      // let self = this;
      // this.currentStep = this.currentStep + 1

    }
    else if (this.currentStep == 3) {
      this.currentStep = this.currentStep
    }
    else if (this.currentStep == 2) {
      if (this.global.currentCaseAddress == "")
        this.presentToast('Xin vui lòng cung cấp vị trí sự việc.')
      else
        this.currentStep = this.currentStep + 1
    }

    if (this.currentStep == 2)
      this.step2Pepare()
  }
  requestAllPermissions() {
    const permissions = Object.keys(this.PERMISSION).map(k => this.PERMISSION[k]);
    this.diagnostic.requestRuntimePermissions(permissions).then((status) => {
      console.log(JSON.stringify(status));
    }, error => {
      console.log('Error: ' + error);
    });
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  startCamera() {
    this.cameraPreview.startCamera(this.cameraPreviewOpts).then(
      (res) => {
        console.log(res)
      },
      (err) => {
        console.log(err)
      });
  }

  stopCamera() {
    this.cameraPreview.stopCamera().then(
      (res) => {
        console.log(res)
      },
      (err) => {
        console.log(err)
      });;
  }

  takePicture() {
    if (this.imageURL.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    }
    else {
      this.cameraPreview.takePicture()
        .then((imgData) => {
          // (<HTMLInputElement>document.getElementById('previewPicture')).src = 'data:image/jpeg;base64,' + imgData;
          let todecode = atob(imgData);
          let base64option: Base64ToGalleryOptions = {
            prefix: '_img',
            mediaScanner: false
          };
          this.base64ToGallery.base64ToGallery(btoa(todecode), base64option)
            .then(
              res => {
                console.log('Saved image to gallery ' + JSON.stringify(res));
                let src = JSON.stringify(res)
                if (src.includes('file://'))
                  this.imageURL.push(src.replace(/^"|"$/g, ''))
                else {
                  src = 'file://' + src.replace(/^"|"$/g, ''); this.imageURL.push(src);
                }
                console.log(src)
                this.listHasElements = true;
              },
              err => console.log('Error saving image to gallery ' + JSON.stringify(err))
            );
        },
          error => {
            alert(JSON.stringify(error));
          });
    }
  }
  delPic(i) {
    this.imageURL.splice(i, 1);
    if (this.imageURL.length >= 1) this.listHasElements = true;
    else this.listHasElements = false;
  }

  openImgZoom() {
    this.navCtrl.push(ImgviewerPage, { imgs: this.imageURL, fromPage: 'submitData' })
  }

  recordVideo() {
    if (this.hasVideos)
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      let options: CaptureVideoOptions = { limit: 1, quality: 0 };
      this.mediaCapture.captureVideo(options).then(
        (data: MediaFile[]) => {
          console.log(data);
          var i, path, len;
          for (i = 0, len = data.length; i < len; i += 1) {
            console.log(path);

            this.rawVideoPath = data[i].fullPath;
            this.videoFilePath = this.sanitizer.bypassSecurityTrustUrl(
              data[i].fullPath
            );
            console.log(this.videoFilePath);
          }
          if (this.videoFilePath != null) this.hasVideos = true;
          else this.hasVideos = false;
        },
        (err: CaptureError) => {
          console.error(err);
        }
      );
    }
  }
  delVideo() {
    this.videoFilePath = "";
    this.rawVideoPath = "";
    this.hasVideos = false;
  }
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Chọn từ thư viện thiết bị',
      buttons: [
        {
          text: 'Chọn ảnh',
          handler: () => {
            this.localImagePicker();
          }
        },
        {
          text: 'Chọn video',
          handler: () => {
            this.localVideoPicker();
          }
        },
        {
          text: 'Hủy',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }
  localVideoPicker() {
    if (this.hasVideos)
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      var options = {
        quality: 0,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: this.camera.MediaType.VIDEO
      };
      this.camera.getPicture(options).then(
        results => {
          this.rawVideoPath = results;
          this.videoFilePath = this.sanitizer.bypassSecurityTrustUrl(results);
          if (this.videoFilePath != null) this.hasVideos = true;
          else this.hasVideos = false;
        },
        err => {
          // Handle error
        }
      );
    }
  }

  localImagePicker() {
    if (this.imageURL.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
      // alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    } else {
      let count = 5 - this.imageURL.length;
      let options: ImagePickerOptions = {
        quality: 10,
        maximumImagesCount: count
      };
      this.imagePicker.getPictures(options).then(
        results => {
          for (let pic of results) {
            this.imageURL.push(pic);
          }
          if (this.imageURL.length >= 1) this.listHasElements = true;
          else this.listHasElements = false;
        },
        err => { }
      );

      // let options: CameraOptions = {
      //   quality: 10,
      //   destinationType: this.camera.DestinationType.FILE_URI,
      //   encodingType: this.camera.EncodingType.JPEG,
      //   mediaType: this.camera.MediaType.PICTURE,
      //   sourceType:0
      // };
      // this.camera.getPicture(options).then(
      //   imageData => {
      //     console.log(this.imageURL)
      //     this.global.convertNativeURI(imageData)
      //     .then(filePath => {
      //       console.log('link đã convert: ' + filePath);
      //       this.imageURL.push(filePath);
      //       if (this.imageURL.length >= 1) this.listHasElements = true;
      //     else this.listHasElements = false;
      //     })
      //     .catch(err => console.log(err)); 
      //   },
      //   err => {
      //     console.log(err);
      //   }
      // );
    }
  }


  SwitchCamera() {
    this.cameraPreview.switchCamera();
  }
  showCamera() {
    this.cameraPreview.show();
  }
  hideCamera() {
    this.cameraPreview.hide();
  }



  //Step2
  step2Pepare() {
    this.getSpeechPermission();

    if (this.hasGPS) {
      if (this.global.currentCaseAddress != "") {
        this.placeMarker(new google.maps.LatLng(this.global.currentLat, this.global.currentLng), this.global.currentCaseAddress)
      }
      else {
        this.findAddressBasedOnLatLnNoGPS(this.currentLat, this.currentLng, true, '')
        this.global.currentLat = this.currentLat;
        this.global.currentLng = this.currentLng;
      }
    }
    else {
      if (this.global.currentCaseAddress != "") {
        this.placeMarker(new google.maps.LatLng(this.global.currentLat, this.global.currentLng), this.global.currentCaseAddress)
      }
    }
    console.log(this.currentLat)
    console.log(this.currentLng)
    this.loadMap()
  }

  findLatLngBasedOnAddress(address) {
    this.nativeGeocoder.forwardGeocode(address, this.geoCoderOptions)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
        // this.findAddressBasedOnLatLn(coordinates[0].latitude,coordinates[0].longitude)
        this.clearOverlays()
        this.placeMarker(new google.maps.LatLng(coordinates[0].latitude, coordinates[0].longitude), address);
      }
      )
      .catch((error: any) => console.log(error));
  }
  findAddressBasedOnLatLn(lat, lon, firtTime, address) {

    this.nativeGeocoder.reverseGeocode(parseFloat(lat), parseFloat(lon), this.geoCoderOptions)
      .then((result: NativeGeocoderReverseResult[]) => {
        let res = result[0];
        let fullAdress = res.subThoroughfare + ' ' + res.thoroughfare + ', ' + res.subAdministrativeArea + ', ' + res.administrativeArea + ', ' + res.countryName;
        console.log(fullAdress);
        if (address != "")
          this.global.currentCaseAddress = address
        else
          this.global.currentCaseAddress = fullAdress;

        this.global.currentLat = lat;
        this.global.currentLng = lon;
        var marker = new google.maps.Marker({
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(lat, lon),
          map: this.map
        });
        marker.setMap(this.map);
        this.map.panTo(new google.maps.LatLng(lat, lon));
        this.map.setZoom(15)
        let content = "<h6>Vị trí sự việc: " + this.global.currentCaseAddress + "</h6>";
        this.addInfoWindow(marker, content);
        this.markers.push(marker);
      }
      )
      .catch((error: any) => console.log(error));
  }
  loadMap() {

    let latLng
    if (this.currentLat == 0)
      latLng = new google.maps.LatLng(10.4082476, 106.9461985);
    else
      latLng = new google.maps.LatLng(this.currentLat, this.currentLng);
    //set options for map
    this.mapOptions['center'] = latLng,
      //initialize the map
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);
    var input = document.getElementById('pac-input') as HTMLInputElement;
    var searchBox = new google.maps.places.SearchBox(input);
    let self = this;
    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces();
      console.log(places)
      if (places.length == 0) {
        return;
      }
      else {
        self.findLtnLnBasedOnAddressNoGPS(places[0].formatted_address)
        //in case of no GPS
        // if (!this.hasGPS)
        //   self.findLtnLnBasedOnAddressNoGPS(places[0].formatted_address)
        // else
        //   self.findLatLngBasedOnAddress(places[0].formatted_address)
      }
    });

    google.maps.event.addListener(self.map, 'click', function (event) {
      self.clearSearch();
      self.clearOverlays();
      self.placeMarker(event.latLng, '');
    });
  }
  findAddressBasedOnLatLnNoGPS(lat, lon, firtTime, address) {
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lon + '&key=AIzaSyBNsEPj9AAhs5hbXz2_tFMe-4pZt91fOzo'
      this.http
        .get(
          url,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json())
            console.log(res.json().results[0].formatted_address)
            if (address != "")
              this.global.currentCaseAddress = address
            else
              this.global.currentCaseAddress = res.json().results[0].formatted_address;
            this.global.currentLat = lat;
            this.global.currentLng = lon;
            var marker = new google.maps.Marker({
              animation: google.maps.Animation.DROP,
              position: new google.maps.LatLng(lat, lon),
              map: this.map

            });
            marker.setMap(this.map);
            this.map.panTo(new google.maps.LatLng(lat, lon));
            this.map.setZoom(15)
            let content = "<h6>Vị trí sự việc: " + this.global.currentCaseAddress + "</h6>";
            this.addInfoWindow(marker, content);
            this.markers.push(marker);
            resolve(res)
          },
          err => {
            reject(err);
          }
        );
    });
  }
  findLtnLnBasedOnAddressNoGPS(address) {

    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = 'https://cors-anywhere.herokuapp.com/' + 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address.replace(' ', '+') + '&key=AIzaSyBNsEPj9AAhs5hbXz2_tFMe-4pZt91fOzo';
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json())
            console.log(res.json().results[0].geometry.location.lat)
            console.log(res.json().results[0].geometry.location.lng)
            this.clearOverlays()
            this.placeMarker(new google.maps.LatLng(res.json().results[0].geometry.location.lat, res.json().results[0].geometry.location.lng), address);
            resolve(res)
          },
          err => {
            reject(err);
          }
        );
    });
  }
  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    infoWindow.open(this.map, marker);
  }

  placeMarker(location, adress) {
    console.log(location.lat());
    console.log(location.lng());
    // if (this.hasGPS)
    //   this.findAddressBasedOnLatLn(location.lat(), location.lng(), false, adress);
    // else
    this.findAddressBasedOnLatLnNoGPS(location.lat(), location.lng(), false, adress)
  }
  clearOverlays() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }

  myLocation() {
    this.geolocation.getCurrentPosition({
      timeout: 5000,
      enableHighAccuracy: true,
      maximumAge: Infinity
    }).then((resp) => {
      console.log(resp)
      this.clearOverlays();
      this.placeMarker(new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude), '')
    }).catch((error) => {
      alert('Xin vui lòng kiểm tra kết nối GPS');
    });
  }
  clearSearch() {
    var input = document.getElementById('pac-input') as HTMLInputElement;
    input.value = '';
  }

  tryAgain(index) {
    this.showHideVoiceInputComponents(false, index)
    this.speechRecording(index)
  }
  speechRecording(index) {
    if (index == 3) {
      this.showHideVoiceInputComponents(false, index)
      this.speechRecognition.startListening({
        language: "vi-VN",
        showPopup: false
      }).subscribe(matches => {
        document.getElementById('modalClose3').click();
        let input = document.getElementById('pac-input') as HTMLInputElement;
        input.value = matches[0];
        input.focus();
      },
        onerror => {
          console.log("Lỗi")
          this.showHideVoiceInputComponents(true, index)
        });
    }
    else {
      this.showHideVoiceInputComponents(false, index)
      this.speechRecognition.startListening({
        language: "vi-VN",
        showPopup: false
      }).subscribe(matches => {
        document.getElementById('modalClose4').click();
        this.caseContent = matches[0];
      },
        onerror => {
          console.log("Lỗi")
          this.showHideVoiceInputComponents(true, index)
        });
    }

  }
  stopSpeechRecognition() {
    this.speechRecognition.stopListening();
  }
  showHideVoiceInputComponents(hide, index) {
    if (hide) {
      let a = document.getElementById('voiceInputLabel' + index) as HTMLElement
      a.hidden = true
      a = document.getElementById('voiceInputImg' + index) as HTMLElement
      a.hidden = true
      a = document.getElementById('voiceInputErrorBtns' + index) as HTMLElement
      a.hidden = false
    }
    else {
      let a = document.getElementById('voiceInputLabel' + index) as HTMLElement
      a.hidden = false
      a = document.getElementById('voiceInputImg' + index) as HTMLElement
      a.hidden = false
      a = document.getElementById('voiceInputErrorBtns' + index) as HTMLElement
      a.hidden = true
    }
  }
  stopListening() {
    this.speechRecognition.stopListening().then(() => {
    });
  }
  getSpeechPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          this.speechRecognition.requestPermission();
        }
      });
  }

  //Step 3
  sentData() {

    console.log(
      this.global.currentCaseAddress + '/' +
      this.contactName + '/' +
      this.contactPhone + '/' +
      this.caseContent + '/' +
      this.global.currentLat + '/' +
      this.global.currentLng + '/');
    this.global.presentLoadingCustom('')
    let self=this;
    setTimeout(function(){ self.waitedTilLoadingHasBeenPresented() }, 1000);
  }
  waitedTilLoadingHasBeenPresented() {
    if (this.contactPhone == "" || this.contactName == '' || this.caseContent == '') {
      this.global.cancleCustomLoading()
      this.presentToast("Xin vui lòng cung cấp đầy đủ thông tin bắt buộc");
    }
    else if (this.global.currentCaseAddress == "") {
      this.global.cancleCustomLoading()
      this.presentToast("Xin hãy xác định vị trí sự việc bằng tính năng bản đồ")
    }
    else if (!this.global.validateSpecialCharacter(this.caseContent, "content").res) {
      this.global.cancleCustomLoading()
      this.presentToast(this.global.validateSpecialCharacter(this.caseContent, "content").msg);
    }
    else if (!this.global.validateSpecialCharacter(this.contactPhone, "phone").res) {
      this.global.cancleCustomLoading()
      this.presentToast(this.global.validateSpecialCharacter(this.contactPhone, "phone").msg);
    }
    else if (!this.global.validateSpecialCharacter(this.contactName, "name").res) {
      this.global.cancleCustomLoading()
      this.presentToast(this.global.validateSpecialCharacter(this.contactName, "name").msg);
    }
    else {

      //find catergory
      let parentCatergory
      for (let catergory of this.global.allCatergories) {
        if (this.subCatergory.parentCode == catergory.code) { parentCatergory = catergory; break; }
      }

      return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let apiURL = this.global.apiURL.func_GuiSuCo_New;
        console.log(apiURL)
        this.http
          .post(
            apiURL,
            {
              Content: this.caseContent,
              Level: 'RKC',
              Location: this.global.currentLat.toString() + ',' + this.global.currentLng.toString(),
              Areadetail: this.global.currentCaseAddress,
              Category: { "Code": parentCatergory.code, "Name": parentCatergory.name },
              Subcategory: { "Code": this.subCatergory.code, "Name": this.subCatergory.name },
              Contact: { "Name": this.contactName, "Phonenumber": this.contactPhone, "Address": "", "Email": "" },
              Attachment: [],
              ReceptionChannel: "mobile"
            },
            { headers: headers }
          )
          .subscribe(
            res => {
              console.log(res.json());
              resolve(res.json());
              if (res.json().code == 0) {
                this.global.cancleCustomLoading();
                console.log('gửi vụ việc fail')
              }
              else {
                console.log('gửi vụ việc thành công')
                this.storage.set("userName", this.contactName)
                this.storage.set("userPhone", this.contactPhone)

                this.caseSentSuccessMsg = res.json().msg
                if (res.json().data != null) {
                  let data = res.json().data;
                  this.storage.get('idEvents').then((val) => {
                    if (val == null) {
                      let id = [];
                      id.push(data.id);
                      this.storage.set("idEvents", JSON.stringify(id));
                    }
                    else {
                      let id = JSON.parse(val);
                      id.push(data.id);
                      this.storage.set("idEvents", JSON.stringify(id));
                    }
                  });

                  this.currentIDToSendVideos = data.id;
                  if (this.imageURL.length > 0) {
                    this.newImgsSender();
                  }
                  if (this.videoFilePath != null && this.videoFilePath != "") {

                    this.videosSender();
                  }
                }

                this.global.cancleCustomLoading();
                this.presentToast(this.caseSentSuccessMsg)
                this.navCtrl.pop();
              }
            },
            err => {
              this.global.cancleCustomLoading();
              console.log(err);
              alert("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.")
              reject(err);
            }
          );
      });
    }
  }
  //Send Imgs
  newImgsSender() {
    for (let i = 0; i < this.imageURL.length; ++i) {
      let pic = this.imageURL[i]
      console.log(pic);
      if (pic.startsWith("file")) {
        this.file
          .resolveLocalFilesystemUrl(pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      } else {
        this.file
          .resolveLocalFilesystemUrl("file://" + pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      }
    }
  }

  private OLDreadnewImgFile(file: any) {
    let orimForm = new FormData();
    const reader = new FileReader();
    reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], { type: file.type });
      orimForm.append("data", imgBlob);
      orimForm.append("id", this.currentIDToSendVideos);
      orimForm.append("token", '');
      this.postNewImgData(orimForm, file.name)
    };
    reader.readAsArrayBuffer(file);
  }

  private readnewImgFile(file: any, index) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], { type: file.type });
      this.masterFormdataToSend.append("data", imgBlob)
      if (index == this.imageURL.length - 1) {
        let self = this;
        setTimeout(function () {
          console.log("Timeout");
          self.masterFormdataToSend.append("id", self.currentIDToSendVideos);
          self.masterFormdataToSend.append("token", '');
          self.postNewImgData(self.masterFormdataToSend, 'abc')
        }, 2000);

      }
    };
    reader.readAsArrayBuffer(file);
  }
  private postNewImgData(formData: FormData, filename) {
    console.log('tên file: ' + filename);
    console.log("Here and ready to be uploaded - new img upload");
    console.log(formData.getAll('data'))
    let url = this.global.apiURL.gui_imgs_new + filename
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA"
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .map(res => res.json())
      .subscribe(
        data => {
          this.global.cancleCustomLoading();
          console.log(data);
          if (data.status == 1) {
            console.log('gửi ảnh thành công');
            // this.testUpdateImgURLtoDB(data.file_path, "img")
          }
          else {
            this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
            this.navCtrl.pop();
          }
        },
        err => {
          this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
          this.navCtrl.pop();
        }
      );
  }

  //Send videos

  videosSender() {
    console.log("file://" + this.rawVideoPath);
    if (this.rawVideoPath.startsWith("file")) {
      this.file
        .resolveLocalFilesystemUrl(this.rawVideoPath)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            console.log("video size : " + metadata.size);
            // check if the file size is larger then the allowed threshold
            if (metadata.size / 1000000 > 50) {
              this.global.cancleCustomLoading();
              this.presentToast("Sự việc đã gửi thành công nhưng dung lượng file video tối đa cho phép là 50MB, xin vui lòng lựa chọn 1 file khác.")

            } else {
              (entry as FileEntry).file(file => this.readFile(file));
            }
          });
        })
        .catch(err => console.log(err));
    } else {
      this.file
        .resolveLocalFilesystemUrl("file://" + this.rawVideoPath)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            if (metadata.size / 1000000 > 50) {
              this.global.cancleCustomLoading();
              this.presentToast("Sự việc đã gửi thành công nhưng dung lượng file video tối đa cho phép là 50MB, xin vui lòng lựa chọn 1 file khác.")

            } else {
              (entry as FileEntry).file(file => this.readFile(file));
            }
          });
        })
        .catch(err => console.log(err));
    }
  }

  private readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      formData.append("data", imgBlob);
      formData.append("id", this.currentIDToSendVideos);
      formData.append("token", '');
      this.postData(formData, file.name);
    };
    reader.readAsArrayBuffer(file);
  }

  private postData(formData: FormData, filename) {
    console.log("Here and ready to be uploaded");
    let url = this.global.apiURL.gui_imgs_new + filename
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA"
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .map(res => res.json())
      .subscribe(
        data => {
          this.global.cancleCustomLoading();
          console.log(data);
          if (data.status == 1) {
            console.log('gửi video thành công')
            // this.testUpdateImgURLtoDB(data.file_path, "video")
          }
          else {
            this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
            this.navCtrl.pop();
          }
        },
        err => {
          this.global.cancleCustomLoading();
          this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
          this.navCtrl.pop();
        }
      );
  }

  testUpdateImgURLtoDB(imgLink, type) {

    let dataToSend
    if (type == 'img') {
      console.log('img link to update DB: ' + imgLink)
      dataToSend = { Id: this.currentIDToSendVideos, Attachment: [{ Filepath: imgLink }], VideoAttachment: [] }
    }
    else {
      console.log('video link to update DB: ' + imgLink)
      dataToSend = { Id: this.currentIDToSendVideos, VideoAttachment: [{ Filepath: imgLink }], Attachment: [] }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = this.global.apiURL.update_attachment;
      this.http.post(apiURL, dataToSend, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          console.log('lưu url img thành công')
          // this.testGetCasesByID()
          resolve(res.json());
        },
        err => {
          console.log(err);
          console.log('lưu url img thất bại')
          reject(err);
        }
      );
    });
  }

  slideChanged() {
    // let index = this.slidesImg.getActiveIndex();
    // this.slidesBtn.slideTo(index, 500); 
  }
}
