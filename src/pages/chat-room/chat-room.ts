import { Component, ViewChild } from '@angular/core';
import { NavController, IonicPage, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { JwtHelper } from "jwt-simple";
// import * as io from 'socket.io-client';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ChatHistoryPage } from '../../pages/chat-history/chat-history';

declare var io;
declare var require: any;

/**
 * Generated class for the ChatRoomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-chat-room',
  templateUrl: 'chat-room.html',
})
export class ChatRoomPage {
  @ViewChild('content') content: any;
  modal
  chatHistory = []
  public myUserId: string;
  dialNumber: any;
  socket: any;
  chat_input = ""
  chats = []
  roomSelecting = '';
  callCenter = '';
  currentLat: any;
  currentLon: any;
  loading: any;
  hasChat = false
  firstConnectingTime = true
  connected = false
  timer
  constructor(private storage: Storage, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private geolocation: Geolocation, private navCtrl: NavController, private navParams: NavParams, private toastCtrl: ToastController) {

  }
  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Đang tiến hành kết nối.'
    });
    this.loading.present();
    var self = this
    this.timer = setTimeout(function () {
      if (!self.connected) {
        self.loading.dismiss()
        let alert = self.alertCtrl.create({
          title: 'Thông Báo',
          message: 'Không thể kết nối hoặc tổng đài viên đang bận. Xin vui lòng thử lại sau ít phút.',
          buttons: [
            {
              text: 'Đồng Ý',
              role: 'cancel',
              handler: () => {
                self.navCtrl.pop();
              }
            },
          ]
        });
        alert.present();
      }
    }, 30000);

    this.myUserId = this.navParams.get('nickname')
    this.dialNumber = this.navParams.get('dialNumber');
    if (this.myUserId == null) {
      this.myUserId = Date.now().toString();
    }

    this.getLocation();
    // this.socket = io('http://222.255.102.150:3000/socket.io', {query: 'token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImJjaDExNXUxIiwiYXV0aG9ydXJpIjoiIiwidXNlcl9pZCI6MjQsImFnZW50X2lkIjozMiwiYWdlbnRfZXh0ZW5zaW9uIjoxMTUwMDAxLCJhZ2VudF9jbGllbnRfaWQiOjEsImV4cCI6MTUzODE0MTg2OH0.kqrLd7JfRMn77IiM-WeqjPAr239uZMUyd_B6T4c5L4k' });
    // this.socket = io('http://222.255.102.150:3000/socket.io', {query: 'token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImJjaDExNXUxIiwiYXV0aG9ydXJpIjoiIiwidXNlcl9pZCI6MjQsImFnZW50X2lkIjozMiwiYWdlbnRfZXh0ZW5zaW9uIjoxMTUwMDAxLCJhZ2VudF9jbGllbnRfaWQiOjEsImV4cCI6MTUzNDEzMTg2OH0._EdGGAwViNjYmIWDtBDUzCedfU8ohIdLazynxRjVXyA&EIO' });
    // this.socket=io('https://hoangn.localtunnel.me');
    // this.socket=io('http://crm.eoc.vnptmedia.vn/js/socket-connect.js')
    // console.log('check 1', this.socket.connected);
    // this.socket.on('connect', function() {
    //   console.log('check 2', this.socket.connected);
    // });
    // this.socket.on('reconnect', (attemptNumber) => {
    //   console.log('check 3', this.socket.connected);
    // });

    // this.ioConnectFromMedia()
    // this.Receive()
  }

  goHistory() {

    if (this.chatHistory.length > 0) {
      this.modal = document.getElementById('savingModal') as HTMLElement
      this.modal.showModal()
      this.getAndSetHistory(true)
    }
    else
      this.navCtrl.push(ChatHistoryPage, { dialNumber: this.dialNumber })
  }
  presentToast(text, time) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: time,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  getLocation() {
    console.log(this.dialNumber)

    this.geolocation.getCurrentPosition({
      timeout: 5000,
      enableHighAccuracy: true,
      maximumAge: Infinity
    }).then((resp) => {
      console.log(resp)
      this.currentLat = resp.coords.latitude
      this.currentLon = resp.coords.longitude
      this.ioConnectFromMedia()
    }).catch((error) => {
      console.log('Error getting location', error);
      this.currentLat = 0
      this.currentLon = 0
      this.ioConnectFromMedia()
    });
  }

  getAndSetHistory(goHistory) {
    let oldHistory = [];
    var x=this;
    if (this.dialNumber == '113') {
      this.storage.get('113_ChatHistory').then((val) => {
        oldHistory = JSON.parse(val);
        for (let item of this.chatHistory) {
          oldHistory.push(item)
        }
        console.log(oldHistory)
        this.storage.set("113_ChatHistory", JSON.stringify(oldHistory)).then((val) => {
          if (goHistory) {
            setTimeout(function () {   x.modal.close();
              x.chatHistory=[];
              x.chats=[]
              x.navCtrl.push(ChatHistoryPage, { dialNumber: x.dialNumber }) }, 1500);
          }
        });
      });
    }
    else if (this.dialNumber == '114') {
      this.storage.get('114_ChatHistory').then((val) => {
        oldHistory = JSON.parse(val);
        for (let item of this.chatHistory) {
          oldHistory.push(item)
        }
        console.log(oldHistory)
        this.storage.set("114_ChatHistory", JSON.stringify(oldHistory)).then((val) => {
          if (goHistory) {
            setTimeout(function () {   x.modal.close();
              x.chatHistory=[];
              x.chats=[]
              x.navCtrl.push(ChatHistoryPage, { dialNumber: x.dialNumber }) }, 1500);      
          }
        });
      });
    }
    else {
      this.storage.get('115_ChatHistory').then((val) => {
        oldHistory = JSON.parse(val);
        for (let item of this.chatHistory) {
          oldHistory.push(item)
        }
        console.log(oldHistory)
        this.storage.set("115_ChatHistory", JSON.stringify(oldHistory)).then((val) => {
          if (goHistory) {
            setTimeout(function () {   x.modal.close();
              x.chatHistory=[];
              x.chats=[]
              x.navCtrl.push(ChatHistoryPage, { dialNumber: x.dialNumber }) }, 1500);
          }
        });
      });
    }
  }

  ionViewWillLeave() {
    if(this.navCtrl.getViews().length>=3)
    {
      console.log('Going to history')
    }
    else
    {
      if (this.chatHistory.length > 0) {
        this.getAndSetHistory(false)
      }    
    }
    this.socket.close()
    this.loading.dismiss()
    this.firstConnectingTime = true
    this.connected = false
    clearTimeout(this.timer)    
  }

  presentAlert(text) {
    let alert = this.alertCtrl.create({
      title: 'Thông Báo',
      message: text,
      buttons: [
        {
          text: 'Đồng Ý',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
      ]
    });
    alert.present();
  }

  JWTEncode(lat, lon, dialNumber, phone) {
    var jwt = require('jwt-simple');
    var payload = {
      "msisdn": phone,
      "dial_number": dialNumber + "",
      "lat": lat,
      "lng": lon
    };
    // var secret = 'NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ';
    var secret = 'pbxcrm@2018';
    // encode
    var token = jwt.encode(payload, secret);
    return token;
  }

  //Test with external IO JS library
  ioConnectFromMedia() {
    console.log('start');
    var token = this.JWTEncode(this.currentLat, this.currentLon, this.dialNumber, this.myUserId);
    console.log(token)
    this.socket = io.connect("http://222.255.102.150:3000", { query: 'token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzY290Y2guaW8iLCJleHAiOjIzMDA4MTkzODAsIm5hbWUiOiJDaHJpcyBTZXZpbGxlamEiLCJhZG1pbiI6dHJ1ZSwiZ3Vlc3RfcGhvbmUiOiIwOTAzOTA0NDg4In0.qs1Jwx5yXqJdynEP1IOAYasbcKiFER4YsRYbtNWeC0w' });
    // this.socket =ionic io.connect("http://localhost:4003");
    // this.socket=io.connect("https://hn162.localtunnel.me")

    // this.socket.on('connect', function () {
    //   console.log("Socket Event Connected!", "success");
    // });
    var self = this;
    this.socket.on('connect', function () {
      self.loading.dismiss()
      self.connected = true
      if (self.firstConnectingTime) {
        self.hasChat = true; self.firstConnectingTime = false
      }
      setTimeout(function () {
        console.log('socket connected');
        console.log(self.dialNumber)

        if (self.dialNumber == '113') {
          console.log('Create room 113-' + self.myUserId);
          self.socket.emit('guestJoinRoom', { room_id: '113-' + self.myUserId, call_center: '113', phone_number: self.myUserId });
          self.roomSelecting = '113-' + self.myUserId;
          self.callCenter = '113';
        }
        else if (self.dialNumber == '114') {
          console.log('Create room 114-' + self.myUserId);
          self.socket.emit('guestJoinRoom', { room_id: '114-' + self.myUserId, call_center: '114', phone_number: self.myUserId });
          self.roomSelecting = '114-' + self.myUserId;
          self.callCenter = '114';
        }
        else {
          console.log('Create room 115-' + self.myUserId);
          self.socket.emit('guestJoinRoom', { room_id: '115-' + self.myUserId, call_center: '115', phone_number: self.myUserId });
          self.roomSelecting = '115-' + self.myUserId;
          self.callCenter = '115';
        }

        self.socket.on('newMessage', function (data) {
          self.hasChat = false;
          var item = { "styleClass": "chat-message right", "msgStr": data.message };
          self.chats.push(item);
          var x = self;
          setTimeout(function () { x.content.scrollToBottom(300); }, 300);

          //Save to history
          var currentdate = new Date();
          var datetime = currentdate.getDate() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getFullYear() + " -- "
            + currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();
          self.chatHistory.push({ "from": self.dialNumber, "body": data.message, "time": datetime })
        });

        // message.addEventListener('keypress', function () {
        //   socket.emit('typing', { room_id: roomSelecting, phone_number: this.myUserId });
        // })

        // socket.on('typing', function (data) {
        //   feedback.innerHTML = '<p><em>' + data + ' is typing a message...</em></p>';
        // });
      })
    });

    this.socket.on("unauthorized", function (error, callback) {
      // self.loading.dismiss()
      // self.presentToast('Kết nối thất bại. Hệ thống sẽ tự động thử lại.',2000)
      if (error.data.type == "UnauthorizedError" || error.data.code == "invalid_token") {
        // redirect user to login page perhaps or execute callback:
        console.log("User's token has expired");
      }
    });

    // this.socket.on("unauthorized", function (error) {
    //   console.log(error);
    //   if (error.data.type == "UnauthorizedError" || error.data.code == "invalid_token") {
    //     // redirect user to login page perhaps?
    //     console.log("User's token has expired");
    //   }
    // });
    this.socket.on('disconnect', function () {
      console.log("Socket Event Disconnected!", "info");
      // self.loading.dismiss()
      // self.presentToast('Kết nối thất bại. Hệ thống sẽ tự động thử lại.',2000)
    });
    this.socket.on('connect_failed', function () {
      console.log("Socket Event Connect Failed !", "error");
      // self.loading.dismiss()
      // self.presentToast('Kết nối thất bại. Hệ thống sẽ tự động thử lại.',2000)
    });
    this.socket.on('connect_error', function () {
      console.log("Socket Event Connect Error !", "error");
      // self.loading.dismiss()
      // self.presentToast('Kết nối thất bại. Hệ thống sẽ tự động thử lại.',2000)
    });
    this.socket.on('error', function (err) {
      console.log(err);
      // self.loading.dismiss()
      // self.presentToast('Kết nối thất bại. Hệ thống sẽ tự động thử lại.',2000)
    });
  }

  // send(msg) {
  //   //default message sending
  //   console.log(this.socket)
  //   console.log(msg)
  //   if (msg != '') {
  //     // Assign user typed message along with generated user id
  //     let saltedMsg = this.myUserId + "#" + msg;
  //     // Push the message through socket 
  //     this.socket.emit('message', saltedMsg);
  //   }
  //   this.chat_input = '';
  // }

  send(msg) {
    if (msg != '') {

      this.socket.emit('sendMessage', {
        message: msg,
        room_id: this.roomSelecting,
        phone_number: this.myUserId,
        call_center: this.callCenter
      });

      var item = { "styleClass": "chat-message left", "msgStr": msg };
      this.hasChat = false;
      this.chats.push(item);
      var self = this
      setTimeout(function () { self.content.scrollToBottom(300); }, 300);
      this.chat_input = '';
      //Save to history
      var currentdate = new Date();
      var datetime = currentdate.getDate() + "/"
        + (currentdate.getMonth() + 1) + "/"
        + currentdate.getFullYear() + " -- "
        + currentdate.getHours() + ":"
        + currentdate.getMinutes() + ":"
        + currentdate.getSeconds();
      self.chatHistory.push({ "from": 'Bạn', "body": msg, "time": datetime })
    }
    else { return false }
  }
  Receive() {
    // Socket receiving method 
    this.socket.on('message', (msg) => {
      // separate the salted message with "#" tag 
      console.log(msg)
      let saletdMsgArr = msg.split('#');
      var item = {};
      // check the sender id and change the style class
      if (saletdMsgArr[0] == this.myUserId) {
        item = { "styleClass": "chat-message right", "msgStr": saletdMsgArr[1] };
      }
      else {
        item = { "styleClass": "chat-message left", "msgStr": saletdMsgArr[1] };
      }
      // push the bind object to array
      // Final chats array will iterate in the view  
      this.chats.push(item);
    });
  }
}
