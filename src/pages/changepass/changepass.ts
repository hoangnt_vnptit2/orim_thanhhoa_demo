
import { Component } from '@angular/core';
import { NavController, IonicPage, ToastController, AlertController, ModalController, NavParams, LoadingController, MenuController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MediaPage } from '../media/media'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { ChatRoomPage } from '../../pages/chat-room/chat-room';
import { Observable } from 'rxjs/Observable';
import { Map15Page } from '../map15/map15';
import { EmployeeinfoPage } from '../employeeinfo/employeeinfo';
import CryptoJS from 'crypto-js';

/**
 * Generated class for the ChangepassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-changepass',
  templateUrl: 'changepass.html',
})
export class ChangepassPage {

  oldPass = "";
  newPass = "";
  confirmPass = "";
  loading
  constructor(public menuCtrl: MenuController, public loadingCtrl: LoadingController, public menu: MenuController, public navParams: NavParams, public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private geolocation: Geolocation) {
  }
  ionViewWillEnter(){
   this.checkValidateToken();
  }
  checkValidateToken() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_DsSuCo_ChuaXuLy;

      let assignee = []
      assignee.push(this.global.currentUserInfo.nguoidung_id)
      assignee.push(this.global.currentUserInfo.donvi_id)

      this.http
        .post(
          apiURL,
          {
            listAssignStatus: ["0"],
            assignee: assignee,
            liststatus: ["13"]
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  openMenu() {
    this.menu.open()
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepassPage');
  }
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  updatePass() {
    if (this.newPass == "" || this.confirmPass == "")
      this.presentToast('Xin hãy cung cấp đầy đủ thông tin bắt buộc')
    else if (this.newPass != this.confirmPass)
      this.presentToast('Mật khẩu nhập lại không đúng.')
    else {
      this.global.presentLoadingCustom('')
      return new Promise((resolve, reject) => {
        console.log(this.oldPass)
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        headers.append("token", this.global.currentUserInfo.token)
        let apiURL = this.global.apiURL.func_ChangPass;
        this.http
          .post(
            apiURL,
            {
              password: this.passwordEncrypt(this.newPass),
            },
            { headers: headers }
          )
          .subscribe(
            res => {
              console.log(res.json());
              // if (res.json().code == 1) {
              //   //pass change info is ok
              //   this.loading.dismiss();
              // }
              // else if(res.json().code == 2)
              // {
              //   this.logoutDueToTokenExpired()
              // }
              // else {
              //   let self = this;
              //   self.loading.dismiss();
              //   this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
              // }
              this.presentToast('Mật khẩu của bạn đã được cập nhật thành công. Mật khẩu mới: ' + this.newPass)
              this.global.cancleCustomLoading()
              this.navCtrl.popToRoot();
              resolve(res.json());
            },
            err => {
              console.log(err);
              this.loading.dismiss();
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
              reject(err);
            }
          );
      });
    }
  }
  passwordEncrypt(pass) {
    let key = CryptoJS.enc.Utf8.parse('8080808080808080');
    let iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    let encryptedpass = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(pass), key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
    console.log('password đã mã hoá: ' + encryptedpass)
    return encryptedpass.toString();
  }

  logoutDueToTokenExpired() {
    alert("Phiên đăng nhập của bạn đã kết thúc, xin vui lòng đăng nhập lại.")
    this.global.islogin = false
    this.storage.set('autoLogin', false);
    this.menuCtrl.enable(true, "menuBeforeLogin");
    this.menuCtrl.enable(false, "menuAfterLogin");
    this.navCtrl.popToRoot();
  }

  


}
