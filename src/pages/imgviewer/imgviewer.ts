import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';

/**
 * Generated class for the ImgviewerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-imgviewer',
  templateUrl: 'imgviewer.html',
})
export class ImgviewerPage {
  
  imgs = []
  constructor(public global:GlobalHeroProvider,public menu: MenuController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ImgviewerPage');
    if(this.navParams.get('fromPage')=='submitData')
    {
      this.imgs=this.navParams.get('imgs');
    }
    else
    {
      for(let path of this.navParams.get('imgs'))
      {
        this.imgs.push(this.global.baseURLForIcon + path)
      }
      console.log(this.imgs)
      try {
        if (!!this.navParams.get('imgs')[0].filePath) {
          //its declared you can use it
          this.imgs = []
          for (let a of this.navParams.get('imgs')) {
            this.imgs.push(this.global.baseURLForIcon + a.filePath)
          }
        }     
      }
      catch (e) {
        //its not declared
      }
    }
    console.log(this.imgs)
  }
  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

}
