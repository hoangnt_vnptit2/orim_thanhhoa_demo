import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, ModalController, LoadingController, MenuController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { SubmitdataPage } from '../submitdata/submitdata';
import { Map15Page } from "../map15/map15";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmployeeinfoPage } from '../employeeinfo/employeeinfo';
import { Submitdata2Page } from '../submitdata2/submitdata2';


//test
import { AssignPage } from '../assign/assign';
import { ProcessPage } from '../process/process';
import { ApprovalPage } from '../approval/approval';
/**
 * Generated class for the ThirdHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//Slide Control
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Market } from '@ionic-native/market';

//app version
import { AppVersion } from '@ionic-native/app-version';


@Component({
  selector: 'page-third-home',
  templateUrl: 'third-home.html',
})
export class ThirdHomePage {
  browser
  @ViewChild(Slides) slides: Slides;
  loading


  constructor(private appVersion: AppVersion, private market: Market, private iab: InAppBrowser, public menu: MenuController, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private geolocation: Geolocation) {
    this.setupAutoLogin();
  }
  ionViewWillEnter() {
    if (this.platform.is('cordova') && this.global.appInfo.bundleID == '' && this.global.prodOrDemo)
      this.checkLocalVersion();
    this.getCatergoriesAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThirdHomePage');
  }
  ionViewDidEnter() {
    //  this.global.presentLoadingCustom('đây là loading')
  }
  fixIconLink(icon) {
    // let iconParts=icon.split('/')
    return this.global.baseURLForIcon + icon
  }
  gotoSubmitData(index) {
    if (this.global.testMode == 0) {
      console.log(index)
      this.navCtrl.push(SubmitdataPage, { index: index });
      this.geolocation.getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      }).then((resp) => {
        console.log(resp)
        this.global.currentLat = resp.coords.latitude;
        this.global.currentLng = resp.coords.longitude;
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }
    else {
      this.diagnostic.isGpsLocationEnabled()
        .then((state) => {
          if (!state) {
            console.log('noGPS enabled')
            let alert = this.alertCtrl.create({
              title: 'Cài Đặt GPS',
              message: 'Quý khách vui lòng cân nhắc mở GPS (\"Vị Trí/Location\") trong \"Cài Đặt/Settings\" của thiết bị để chúng tôi có thể phục vụ bạn tốt hơn.',
              buttons: [
                {
                  text: 'Tiếp tục',
                  role: 'cancel',
                  handler: () => {
                    this.navCtrl.push(SubmitdataPage, { index: index });
                  }
                },
                {
                  text: 'Cài Đặt',
                  handler: () => {
                    this.openNativeSettings.open('location');
                  }
                }
              ]
            });
            alert.present();
          }
          else {
            this.navCtrl.push(SubmitdataPage, { index: index });
            this.geolocation.getCurrentPosition({
              timeout: 5000,
              enableHighAccuracy: true,
              maximumAge: Infinity
            }).then((resp) => {
              console.log(resp)
              this.global.currentLat = resp.coords.latitude;
              this.global.currentLng = resp.coords.longitude;
            }).catch((error) => {
              console.log('Error getting location', error);
            });
          }
        }).catch(e => console.error(e));
    }

  }
  gotoSubmitData2(type) {
    let subCatergory
    for (let sub of this.global.allCatergories) {
      if (type == sub.name) {
        subCatergory = sub; break;
      }
    }
    console.log(type)
    this.global.presentLoadingCustom('Đang tiến hành xác định vị trí...')
    let self = this;
    setTimeout(function () {
      self.global.cancleCustomLoading()
    }, 1500);

    this.geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      })
      .then(resp => {
        this.navCtrl.push(Submitdata2Page, {
          type: type,
          currentlat: resp.coords.latitude,
          currentlng: resp.coords.longitude, hasGPS: true, subCatergory: subCatergory
        });
      })
      .catch(error => {
        console.log(error)
        this.navCtrl.push(Submitdata2Page, {
          type: type,
          currentlat: 0,
          currentlng: 0, hasGPS: false,
          subCatergory: subCatergory
        });
      });

  }
  testMap() {
    this.navCtrl.push(Map15Page, {
      currentlat: this.global.currentLat,
      currentlng: this.global.currentLng
    });
  }

  setupAutoLogin() {
    this.storage.get('autoLogin').then((val) => {
      if (val == true) {
        this.storage.get('EmployeeInfo').then((val) => {
          if (val != null) {
            this.global.currentUserInfo = JSON.parse(val);
            this.global.islogin = true;

            if (this.global.currentUserInfo.username.includes("quanly"))
              this.global.right_code = "PHAN_CONG";
            else
              this.global.right_code = "XU_LY";

            this.menu.enable(false, "menuBeforeLogin");
            this.menu.enable(true, "menuAfterLogin");
          }
        });

        this.storage.get('Permissions').then((val) => {
          if (val != null) {
            this.global.currentUserPermission = JSON.parse(val);
          }
        });
      }
    });
  }

  goWebview(url) {
    console.log(url)
    this.navCtrl.push(EmployeeinfoPage, { url: url })
  }

  moveToSlide(tabID) {
    let index
    for (let x = 0; x < this.global.parentServiceCatergories.length; ++x) {

      if (this.global.parentServiceCatergories[x].id == tabID) {
        index = x;
        break;
      }
    }
    this.slides.slideTo(index, 500);
    document.getElementById(this.global.parentServiceCatergories[index].id).style.backgroundColor = "tomato";
    for (let x = 0; x < this.global.parentServiceCatergories.length; ++x) {
      if (index != x)
        document.getElementById(this.global.parentServiceCatergories[x].id).style.backgroundColor = "#207ee3";
    }
  }
  slideChanged() {
    let index = this.slides.getActiveIndex();
    document.getElementById(this.global.parentServiceCatergories[index].id).style.backgroundColor = "tomato";
    for (let x = 0; x < this.global.parentServiceCatergories.length; ++x) {
      if (index != x)
        document.getElementById(this.global.parentServiceCatergories[x].id).style.backgroundColor = "#207ee3";
    }
  }

  testPhancongXuLyPheDuyetNoLogin(index) {
    if (index == 0) {
      this.navCtrl.push(AssignPage)
    }
    else if (index == 1) {
      this.navCtrl.push(ProcessPage)
    }
    else {
      this.navCtrl.push(ApprovalPage)
    }

  }
  getCatergoriesAll() {
    if (this.global.allCatergories.length == 0) {    
      this.global.presentLoadingCustom('');

      return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let apiURL = this.global.apiURL.getCatergoriesBasedOnArea + '149_1349'
        this.http
          .post(
            apiURL,
            {},
            { headers: headers }
          )
          .subscribe(
            res => {
              console.log(res.json());
              if (res.json().error == 0) {
                console.log('lấy catergories ok')
                this.global.cancleCustomLoading()
                this.global.allCatergories = res.json().list
                this.divideListPerCatergory();
              }
              else {
                this.global.cancleCustomLoading()
                this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
              }
              resolve(res.json());
            },
            err => {
              console.log(err);
              // this.global.cancleCustomLoading();
              this.global.cancleCustomLoading()
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
              reject(err);
            }
          );
      });
    }

  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  testSecondHome() {
    this.global.presentLoadingCustom('Đang tiến hành xác định vị trí...')
    this.geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      })
      .then(resp => {
        this.global.cancleCustomLoading()
        this.navCtrl.push(Submitdata2Page, {
          currentlat: resp.coords.latitude,
          currentlng: resp.coords.longitude, hasGPS: true
        });
      })
      .catch(error => {
        console.log(error)
        this.global.cancleCustomLoading()
        this.navCtrl.push(Submitdata2Page, {
          currentlat: 0,
          currentlng: 0, hasGPS: false
        });
      });

  }

  divideListPerCatergory() {
    for (let service of this.global.allCatergories) {
      if (service.parentCode == "")
        this.global.parentServiceCatergories.push(service)
    }
    console.log('lĩnh vực cha')
    console.log(this.global.parentServiceCatergories)
    if (this.global.parentServiceCatergories.length > 0) {
      let self = this
      setTimeout(function () {
        self.slideChanged();
        for (let x = 0; x < self.global.parentServiceCatergories.length; ++x) {
          let tempArray = []
          for (let service of self.global.allCatergories) {
            if (service.parentCode == self.global.parentServiceCatergories[x].code) {
              if (tempArray.length == 0) {
                tempArray.push([service])
              }
              else {
                if (tempArray[tempArray.length - 1].length == 1)
                tempArray[tempArray.length - 1].push(service)
                else
                tempArray.push([service])
              }
            }
          }
          self.global.subServicesOfParentService.push(tempArray)
        }
        console.log('done dividing services into sub-catergories');
        console.log(self.global.subServicesOfParentService)
      }, 1000);
    }

  }
  checkLocalVersion() {
    this.appVersion.getPackageName().then((val) => {
      if (val != null) {
        this.global.appInfo.bundleID = val;
        this.appVersion.getVersionNumber().then((val) => {
          if (val != null) {
            this.global.appInfo.localVersion = val;
            this.checkStoreVersion();
          }
        }); 
      }
    });
  }
  checkStoreVersion() {

    if (this.platform.is('ios')) {
      let url = 'https://cors-anywhere.herokuapp.com/' + 'https://itunes.apple.com/lookup?bundleId=' + this.global.appInfo.bundleID
      return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.http
          .post(
            url,
            {},
            { headers: headers }
          )
          .subscribe(
            res => {
              console.log(res.json());
              if (res.json().resultCount > 0) {
                this.global.appInfo.newestVersion = res.json().results[0].version
                if (this.global.appInfo.newestVersion != this.global.appInfo.localVersion) {
                  //call force update function
                  this.forceUpdate()
                }
              }
              resolve(res.json());
            },
            err => {
              console.log(err);
              reject(err);
            }
          );
      });
    }
    else {
      let url = 'https://cors-anywhere.herokuapp.com/' + `https://play.google.com/store/apps/details?hl=en&id=` + this.global.appInfo.bundleID;
      this.http.request(url).map(res => res.text()).subscribe(data => {
        let start = data.search('Current Version')
        let fromStartToVersion = data.substring(start, start + 86)
        let version = fromStartToVersion.substr(fromStartToVersion.length - 5)
        console.log(version)
        this.global.appInfo.newestVersion = version
        if (this.global.appInfo.newestVersion != this.global.appInfo.localVersion) {
          //call force update function
          this.forceUpdate()
        }
      });
    }
  }

  forceUpdate() {

    let confirmAlert = this.alertCtrl.create({
      title: 'Cập nhật ứng dụng',
      message: 'Ứng dụng đã có phiên bản mới, xin bạn hãy vui lòng cập nhật để đảm bảo có thể sử dụng đầy đủ các tính năng.',
      buttons: [{
        text: 'Bỏ qua',
        role: 'cancel',
        handler: () => {
        }
      }, {
        text: 'Đồng ý',
        role: 'cancel',
        handler: () => {
          this.market.open(this.global.appInfo.bundleID);
          this.platform.exitApp();
          // window.open('https://appstore.hcmtelecom.vn/it2/orimxlite_demo.html', '_system', 'location=yes'); return false;
        }
      }]
    });
    confirmAlert.present();
  }

}
