import { Component, ViewChild } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  LoadingController, Slides, MenuController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { ElementRef } from '@angular/core';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { ImgviewerPage } from '../imgviewer/imgviewer';

//from old media
import { Camera, CameraOptions } from "@ionic-native/camera";
import {
  MediaCapture,
  MediaFile,
  CaptureError,
  CaptureImageOptions,
  CaptureVideoOptions
} from "@ionic-native/media-capture";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import "rxjs/add/operator/map";
import { Base64 } from "@ionic-native/base64";
import { File, FileEntry } from "@ionic-native/file";
declare var require: any;
declare var google;
import { SpeechRecognition } from '@ionic-native/speech-recognition';

/**
 * Generated class for the SubmitdataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-submitdata",
  templateUrl: "submitdata.html"
})
export class SubmitdataPage {
  //for map only
  @ViewChild('map') mapElement: ElementRef;
  geoCoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  map: any;
  markers = []
  mapOptions = {
    // gestureHandling :'none',
    zoomControl: false,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    // center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    styles: [
      {
        featureType: 'administrative',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit',
        // elementType: 'labels.icon',
        stylers: [{ visibility: 'off' }]
      },

    ]
  }


  @ViewChild('slideZoomSliders') slideZoomSliders: Slides;
  locationToSendWithImgs = []
  ghiChuThem = ''
  checkIfMoveMap = false;
  imgFormData = new FormData()
  showSuccessMsgAfterSendingImgs = false
  index;
  gpsPick = true;
  caseType = "";
  caseLevel = "";
  locationPickType = "auto";
  ward;
  name = "";
  phone = "";
  address = "";
  imgSliders
  //from old media
  listHasElements = false;
  hasVideos = false;
  imageURL = [];
  base64ImgList = [];
  videoFilePath;
  rawVideoPath;
  number;
  toggle;
  verifiredPhoneNumber;
  loading;
  loadingcounter;
  currentLat;
  currentLon;
  currentIDToSendVideos = null
  caseSentSuccessMsg = ""

  constructor(
    public speechRecognition: SpeechRecognition,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation,
    public loadingCtrl: LoadingController,
    private file: File,
    private base64: Base64,
    private imagePicker: ImagePicker,
    public sanitizer: DomSanitizer,
    private mediaCapture: MediaCapture,
    private camera: Camera,
    private menu: MenuController,
    private nativeGeocoder: NativeGeocoder,
  ) { }


  closeImgZoom() {
    this.imgSliders.close()
    this.menu.swipeEnable(true)
  }
  openImgZoom() {
    // this.imgSliders.showModal();
    // this.slideZoomSliders.slideTo(0, 0);
    // this.menu.swipeEnable(false)
    this.navCtrl.push(ImgviewerPage, { imgs: this.imageURL })
  }
  ionViewDidEnter() {
    // this.mapPreviewSetup()
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad SubmitdataPage");
    this.imgSliders = document.getElementById('imageViewer') as HTMLElement;
  }
  ionViewDidLeave() {
    let hasMapPage = false;
    for (let view of this.navCtrl.getViews()) {
      if (view.component == Map15Page) {
        //has map page in the stack
        hasMapPage = true;
        console.log("has map"); this.checkIfMoveMap = true;
      }
    }
    if (!hasMapPage) {
      //no map page, need to remove data
      console.log("no map");
      this.global.currentCaseAddress = ""

      // this.imageURL = [];
      // this.base64ImgList = [];
      // this.videoFilePath = "";
      // this.rawVideoPath = "";
      // this.loadingcounter = 0;
    }
  }
  ionViewWillEnter() {
    // this.testGetCasesByID();
    this.getSpeechPermission();
    this.index = this.navParams.get("index");
    console.log(this.index);
    switch (this.index) {
      case 0:
        this.caseType = "Tụ tập buôn bán trái phép";
        for (let a of this.global.listOfPreferences.DmLoaiSuCo) {
          if (this.index == a.id) { this.caseLevel = a.phanloai; console.log(this.caseLevel); }
        }
        break;
      case 1:
        this.caseType = "Quảng cáo sai quy định";
        for (let a of this.global.listOfPreferences.DmLoaiSuCo) {
          if (this.index == a.id) { this.caseLevel = a.phanloai; console.log(this.caseLevel); }
        }
        break;
      case 2:
        this.caseType = "Đậu xe lấn chiếm lòng lề đường";
        for (let a of this.global.listOfPreferences.DmLoaiSuCo) {
          if (this.index == a.id) { this.caseLevel = a.phanloai; console.log(this.caseLevel); }
        }
        break;
      case 3:
        this.caseType = "Xây dựng không phép";
        for (let a of this.global.listOfPreferences.DmLoaiSuCo) {
          if (this.index == a.id) { this.caseLevel = a.phanloai; console.log(this.caseLevel); }
        }
        break;
      case 4:
        this.caseType = "Đổ rác thải sai quy định";
        for (let a of this.global.listOfPreferences.DmLoaiSuCo) {
          if (this.index == a.id) { this.caseLevel = a.phanloai; console.log(this.caseLevel); }
        }
        break;
      case 5:
        this.caseType = "Vấn đề khác";
        for (let a of this.global.listOfPreferences.DmLoaiSuCo) {
          if (this.index == a.id) { this.caseLevel = a.phanloai; console.log(this.caseLevel); }
        }
        break;
    }
    if (!this.checkIfMoveMap)
      this.ghiChuThem = this.caseType
    //get saved info if presented
    let hasmap = false
    for (let view of this.navCtrl.getViews()) {
      if (view.component == Map15Page) {
        //has map page in the stack
        hasmap = true;
        console.log("has map");
      }
    }
    if (!hasmap) {
      this.storage.get("userName").then(val => {
        if (val != null || val != "") this.name = val;
      });
      this.storage.get("userPhone").then(val => {
        if (val != null || val != "") this.phone = val;
      });
    }

    //from old media
    this.loadingcounter = 0;
    this.toggle = "picture";

    if (this.imageURL.length >= 1) this.listHasElements = true;
    else this.listHasElements = false;
    if (this.videoFilePath != null) this.hasVideos = true;
    else this.hasVideos = false;

    if (this.global.wardList == null)
      // this.getWardInfo()
      if (this.global.caseDegreeList == null)
        this.getCaseLevels()

  }
  getCaseLevels() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.func_laydsDM_MucDo;
      this.http.get(apiURL, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          if (res.json().code == 1) {
            this.global.caseDegreeList = res.json().data
            let self = this;
            // self.loading.dismiss();
          } else {
            let self = this;
            // self.loading.dismiss();
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
          }
          resolve(res.json());
        },
        err => {
          console.log(err);
          // this.loading.dismiss();
          this.presentToast("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.");
          reject(err);
        }
      );
    });
  }
  getWardInfo() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.func_laydsPhuong_Quan;
      this.http.get(apiURL, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          if (res.json().code == 1) {
            this.global.wardList = res.json().data
            let self = this;
            // self.loading.dismiss();
          } else {
            let self = this;
            // self.loading.dismiss();
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
          }
          resolve(res.json());
        },
        err => {
          console.log(err);
          // this.loading.dismiss();
          this.presentToast("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.");
          reject(err);
        }
      );
    });
  }
  sentData() {
    console.log(this.locationToSendWithImgs)
    console.log(this.name);
    console.log(this.phone);
    this.loading = this.loadingCtrl.create({
    });
    this.loading.present();
    if (this.phone == "" || this.phone == null || this.caseType == '') {
      this.loading.dismiss()
      this.presentToast("Xin vui lòng cung cấp đầy đủ thông tin bắt buộc");
    }
    else if (this.base64ImgList.length == 0 && this.videoFilePath != "") {
      this.loading.dismiss()
      this.presentToast("Bạn hãy cung cấp thêm ít nhất 1 hình ảnh hoặc video về sự việc");
    }
    else if (this.global.currentCaseAddress == "") {
      this.loading.dismiss()
      this.presentToast("Xin hãy xác định vị trí sự việc bằng tính năng bản đồ")
    }
    else {
      //get Input Type
      let inputype = 0
      if (this.locationPickType == "auto")
        inputype = 1;
      else
        inputype = 2;
      //get case Type
      let caseCode = ""
      for (let type of this.global.listOfPreferences.DmLoaiSuCo) {
        if (type.name == this.caseType) {
          caseCode = type.code; break;
        }
      }
      console.log(
        inputype + '/' +
        this.global.currentCaseAddress + '/' +
        caseCode + '/' +
        this.caseLevel + '/' +
        this.name + '/' +
        this.phone + '/' +
        this.ghiChuThem + '/' +
        this.global.currentLat + '/' +
        this.global.currentLng + '/' +
        this.base64ImgList
      )

      return new Promise((resolve, reject) => {

        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
        let apiURL = this.global.apiURL.func_GuiSuCo_New;
        console.log(apiURL)
        this.http
          .post(
            apiURL,
            {
              Content: this.ghiChuThem,
              Level: this.caseLevel,
              Location: this.global.currentLat.toString() + ',' + this.global.currentLng.toString(),
              Areadetail: this.global.currentCaseAddress,
              Category: { "Code": "trattudothi", "Name": "Trật tự đô thị" },
              Subcategory: { "Code": "buonbanviahe", "Name": "Buôn bán vỉa hè" },
              Contact: { "Name": this.name, "Phonenumber": this.phone, "Address": "", "Email": "" },
              Attachment: [],
              ReceptionChannel: "mobile"
            },
            { headers: headers }
          )
          .subscribe(
            res => {
              console.log(res.json());
              resolve(res.json());
              if (res.json().code == 0) {
                this.loading.dismiss();
                console.log('gửi vụ việc fail')
              }
              else {
                console.log('gửi thành công')
                this.storage.set("userName", this.name);
                this.storage.set("userPhone", this.phone);
                this.caseSentSuccessMsg = res.json().msg
                if (res.json().data != null) {
                  let data = res.json().data;
                  this.storage.get('idEvents').then((val) => {
                    if (val == null) {
                      let id = [];
                      id.push(data.id);
                      this.storage.set("idEvents", JSON.stringify(id));
                    }
                    else {
                      let id = JSON.parse(val);
                      id.push(data.id);
                      this.storage.set("idEvents", JSON.stringify(id));
                    }
                  });

                  this.currentIDToSendVideos = data.id;
                  // this.testUpdateImgURLtoDB();
                  if (this.imageURL.length > 0) {
                    this.showSuccessMsgAfterSendingImgs = true;
                    this.newImgsSender();
                  }
                  if (this.videoFilePath != null && this.videoFilePath != "") {
                    console.log('có video cần gửi')
                    console.log(this.videoFilePath)
                    this.videosSender();
                  }
                }
                this.navCtrl.pop();
              }
            },
            err => {
              this.loading.dismiss();
              console.log(err);
              alert("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.")
              reject(err);
            }
          );
      });
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  onlocationPickTypeChange(selectedValue: any) {
    console.log("Selected", selectedValue);
    if (selectedValue == "manual") this.gpsPick = false;
    else this.gpsPick = true;
  }

  goMap() {
    this.loading = this.loadingCtrl.create({
      content:
        "Đang tiến hành xác định vị trí..."
    });
    this.loading.present()
    this.geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      })
      .then(resp => {
        this.loading.dismiss()
        this.navCtrl.push(Map15Page, {
          currentlat: resp.coords.latitude,
          currentlng: resp.coords.longitude, hasGPS: true
        });
      })
      .catch(error => {
        console.log(error)
        this.loading.dismiss()
        this.presentToast(
          "Không thể lấy được vị trí hiện tại của bạn. Xin kiểm tra kết nối GPS của thiết bị"
        );
        this.navCtrl.push(Map15Page, {
          currentlat: 0,
          currentlng: 0, hasGPS: false
        });
      });
  }
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  //from old media
  onSwipe() {
    // if (this.toggle == "picture") this.toggle = "video";
    // else this.toggle = "picture";
  }
  delPic(pic) {
    for (var i = 0; i < this.imageURL.length; ++i) {
      if (pic == this.imageURL[i]) {
        this.imageURL.splice(i, 1);
        this.base64ImgList.splice(i, 1);
        this.locationToSendWithImgs.splice(i, 1);
        break;
      }
    }
    if (this.imageURL.length >= 1) this.listHasElements = true;
    else this.listHasElements = false;
  }
  takePhoto() {
    if (this.imageURL.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    } else {
      let options: CameraOptions = {
        quality: 10,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };
      this.camera.getPicture(options).then(
        imageData => {
          console.log(imageData)
          this.getCurrentLocationToSendWithImgs(true);
          this.imageURL.push(imageData);
          this.imgToBase64(imageData);
          this.listHasElements = true;
        },
        err => {
          console.log(err);
        }
      );
    }
  }
  localImagePicker() {
    if (this.imageURL.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
      // alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    } else {
      let count = 5 - this.imageURL.length;
      let options: ImagePickerOptions = {
        quality: 10,
        maximumImagesCount: count
      };
      this.imagePicker.getPictures(options).then(
        results => {
          // for (var i = 0; i < results.length; i++) {
          //   console.log('Image URI: ' + results[i]);
          // }
          for (let pic of results) {
            this.imageURL.push(pic);
            this.imgToBase64(pic);
            this.getCurrentLocationToSendWithImgs(false);
          }
          if (this.imageURL.length >= 1) this.listHasElements = true;
          else this.listHasElements = false;
        },
        err => { }
      );
    }
  }

  sendPics_Videos() {
    if (
      this.imageURL.length < 1 &&
      (this.videoFilePath == "" || this.videoFilePath == null)
    )
      // alert("Bạn hãy chọn ít nhất 1 tấm ảnh hay video để có thể tiến hành gửi cho " + this.number)
      alert(
        "Bạn hãy chọn ít nhất 1 video để có thể tiến hành gửi cho " +
        this.number
      );
    else {
      this.loading = this.loadingCtrl.create({
        content:
          "Đang tiến hành gửi nội dung, xin vui lòng đừng tắt ứng dụng hay thiết bị."
      });
      this.loading.present();
      if (this.videoFilePath != null && this.videoFilePath != "") {
        //call videos sender
        this.loadingcounter += 1;
        this.videosSender();
      } else if (this.imageURL.length >= 1) {
        //call images sender
        this.loadingcounter += 1;
        this.imagesSender();
      }
    }
  }

  imagesSender() {
    let url = "http://crm.eoc.vnptmedia.vn/api/app11x/upload-images";
    var headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");
    let options = new RequestOptions({ headers: headers });
    let body = "data=" + this.JWTEncode("imgs");
    for (let pic of this.base64ImgList) {
      body = body + "&images[]=" + pic;
    }
    console.log(body);
    return this.http
      .post(url, body, options)
      .map(res => res.json())
      .subscribe(
        data => {
          console.log(data);
          if (data.success == true) {
            this.loadingcounter -= 1;
            if (this.loadingcounter == 0) {
              this.loading.dismiss();
              this.presentAlert(
                "Thông báo",
                "Đã gửi thành công. Xin cảm ơn thông tin của bạn."
              );
              this.navCtrl.pop();
            }
          }
        },
        err => {
          this.loading.dismiss();
          this.presentAlert("Thông báo", err);
          this.navCtrl.pop();
        }
      );
  }

  imgToBase64(pic) {
    let filePath: string = pic;
    this.base64.encodeFile(filePath).then(
      (base64File: string) => {
        console.log(base64File);
        this.base64ImgList.push(base64File);
      },
      err => {
        console.log(err);
      }
    );
  }

  videosSender() {
    console.log("file://" + this.rawVideoPath);
    if (this.rawVideoPath.startsWith("file")) {
      this.file
        .resolveLocalFilesystemUrl(this.rawVideoPath)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            console.log("video size : " + metadata.size);
            // check if the file size is larger then the allowed threshold
            if (metadata.size / 1000000 > 50) {
              this.loading.dismiss();
              this.presentAlert(
                "Thông báo",
                "Sự việc đã gửi thành công nhưng dung lượng file video tối đa cho phép là 50MB, xin vui lòng lựa chọn 1 file khác."
              );
            } else {
              (entry as FileEntry).file(file => this.readFile(file));
            }
          });
        })
        .catch(err => console.log(err));
    } else {
      this.file
        .resolveLocalFilesystemUrl("file://" + this.rawVideoPath)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            if (metadata.size / 1000000 > 50) {
              this.loading.dismiss();
              this.presentAlert(
                "Thông báo",
                "Sự việc đã gửi thành công nhưng dung lượng file video tối đa cho phép là 50MB, xin vui lòng lựa chọn 1 file khác."
              );
            } else {
              (entry as FileEntry).file(file => this.readFile(file));
            }
          });
        })
        .catch(err => console.log(err));
    }
  }

  private readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      formData.append("data", imgBlob);
      this.postData(formData, file.name);
    };
    reader.readAsArrayBuffer(file);
  }

  private postData(formData: FormData, filename) {
    console.log("Here and ready to be uploaded");
    let url = this.global.apiURL.gui_imgs_new + filename
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA"
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .map(res => res.json())
      .subscribe(
        data => {
          this.loading.dismiss();
          console.log(data);
          if (data.status == 1) {
            console.log('gửi video thành công')
            this.testUpdateImgURLtoDB(data.file_path, "video")
          }
          else {
            this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
            this.navCtrl.pop();
          }
        },
        err => {
          this.loading.dismiss();
          this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
          this.navCtrl.pop();
        }
      );
  }

  JWTEncode(type) {
    var jwt = require("jwt-simple");
    var secret = "NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ";
    var payload;
    var token;
    if (type == "imgs") {
      payload = {
        msisdn: this.verifiredPhoneNumber,
        dial_number: this.number,
        "file_type ": "images",
        lat: this.currentLat + "",
        lng: this.currentLon + ""
      };
      // encode
      token = jwt.encode(payload, secret);
      console.log(token);
      return token;
      // decode
      // var decoded = jwt.decode(token, secret);
    }
    if (type == "videos") {
      payload = {
        msisdn: this.verifiredPhoneNumber,
        dial_number: this.number,
        "file_type ": "videos",
        lat: this.currentLat + "",
        lng: this.currentLon + ""
      };
      secret = "NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ";
      // encode
      token = jwt.encode(payload, secret);
      console.log(token);
      return token;
      // decode
      // var decoded = jwt.decode(token, secret);
    }
  }
  presentAlert(title, message) {
    let alert = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: title,
      message: message,
      buttons: [
        {
          text: "Đóng",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    alert.present();
  }
  recordVideo() {
    if (this.hasVideos)
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      let options: CaptureVideoOptions = { limit: 1, quality: 0 };
      this.mediaCapture.captureVideo(options).then(
        (data: MediaFile[]) => {
          console.log(data);
          var i, path, len;
          for (i = 0, len = data.length; i < len; i += 1) {
            console.log(path);

            this.rawVideoPath = data[i].fullPath;
            this.videoFilePath = this.sanitizer.bypassSecurityTrustUrl(
              data[i].fullPath
            );
            console.log(this.videoFilePath);
          }
          if (this.videoFilePath != null) this.hasVideos = true;
          else this.hasVideos = false;
        },
        (err: CaptureError) => {
          console.error(err);
        }
      );
    }
  }
  delVideo() {
    this.videoFilePath = "";
    this.rawVideoPath = "";
    this.hasVideos = false;
  }
  localVideoPicker() {
    if (this.hasVideos)
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      var options = {
        quality: 0,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: this.camera.MediaType.VIDEO
      };
      this.camera.getPicture(options).then(
        results => {
          this.rawVideoPath = results;
          this.videoFilePath = this.sanitizer.bypassSecurityTrustUrl(results);
          if (this.videoFilePath != null) this.hasVideos = true;
          else this.hasVideos = false;
        },
        err => {
          // Handle error
        }
      );
    }
  }


  //Maps Preview

  mapPreviewSetup() {
    this.geolocation
      .getCurrentPosition({
        timeout: 5000,
        enableHighAccuracy: true,
        maximumAge: Infinity
      })
      .then(resp => {
        this.loadMap(resp.coords.latitude, resp.coords.longitude);
        this.findAddressBasedOnLatLn(resp.coords.latitude, resp.coords.longitude, '')
        this.global.currentLat = resp.coords.latitude;
        this.global.currentLng = resp.coords.longitude;
      })
      .catch(error => {
        this.presentToast(
          "Không thể lấy được vị trí hiện tại của bạn. Xin kiểm tra kết nối và thử lại."
        );
      });
  }
  findAddressBasedOnLatLn(lat, lon, address) {

    this.nativeGeocoder.reverseGeocode(parseFloat(lat), parseFloat(lon), this.geoCoderOptions)
      .then((result: NativeGeocoderReverseResult[]) => {
        let res = result[0];
        let fullAdress = res.subThoroughfare + ' ' + res.thoroughfare + ', ' + res.subAdministrativeArea + ', ' + res.administrativeArea + ', ' + res.countryName;
        console.log(fullAdress);
        if (address != "")
          this.global.currentCaseAddress = address
        else
          this.global.currentCaseAddress = fullAdress;
      }
      )
      .catch((error: any) => console.log(error));
  }
  loadMap(lat, lng) {
    console.log('load map')
    let latLng = new google.maps.LatLng(lat, lng, lat, lng);
    //set options for map
    this.mapOptions['center'] = latLng,
      //initialize the map
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

    var officesMarker;
    officesMarker = new google.maps.Marker({
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(lat, lng, lat, lng),
      title: 'Vị trí sự việc'
    });
    this.markers.push(officesMarker)
    officesMarker.setMap(this.map);
    let content = "<h6>Vị trí sự việc: " + this.address + "</h6>";
    // this.addInfoWindow(officesMarker, content);
  }
  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    infoWindow.open(this.map, marker);
  }


  newImgsSender() {
    for (let i = 0; i < this.imageURL.length; ++i) {
      let pic = this.imageURL[i]
      console.log(pic);
      if (pic.startsWith("file")) {
        this.file
          .resolveLocalFilesystemUrl(pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file));
          })
          .catch(err => console.log(err));
      } else {
        this.file
          .resolveLocalFilesystemUrl("file://" + pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file));
          })
          .catch(err => console.log(err));
      }

      // if (i == this.imageURL.length - 1) {
      //   let self = this;
      //   setTimeout(function () { self.postNewImgData(self.imgFormData) }, 2000);
      // }

    }
  }

  private readnewImgFile(file: any) {
    let orimForm = new FormData();
    const reader = new FileReader();
    reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], { type: file.type });
      orimForm.append("data", imgBlob); "location"
      this.postNewImgData(orimForm, file.name)
    };
    reader.readAsArrayBuffer(file);
  }

  private postNewImgData(formData: FormData, filename) {
    console.log('tên file: ' + filename);
    console.log("Here and ready to be uploaded - new img upload");
    console.log(formData.getAll('data'))
    let locationPart = JSON.stringify(this.locationToSendWithImgs).replace(/["]+/g, '');
    locationPart = locationPart.split("},").join("};");
    // let url = this.global.apiURL.gui_imgs_new + this.currentIDToSendVideos + '&location=' + locationPart;
    let url = this.global.apiURL.gui_imgs_new + filename
    console.log('Location kèm ảnh: ' + locationPart);
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA"
    });
    // headers.append('location', JSON.stringify(this.locationToSendWithImgs));
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .map(res => res.json())
      .subscribe(
        data => {
          this.loading.dismiss();
          console.log(data);
          if (data.status == 1) {
            console.log('gửi ảnh thành công');
            // this.testUpdateImgURLtoDB(this.global.apiURL.img_to_db + data.file_path);
            this.testUpdateImgURLtoDB(data.file_path, "img")
          }
          else {
            this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
            this.navCtrl.pop();
          }
        },
        err => {
          this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
          this.navCtrl.pop();
        }
      );
  }

  getCurrentLocationToSendWithImgs(takePhotoFromCamera) {
    if (takePhotoFromCamera) {
      this.geolocation
        .getCurrentPosition({
          timeout: 5000,
          enableHighAccuracy: true,
          maximumAge: Infinity
        })
        .then(resp => {
          this.locationToSendWithImgs.push({ lat: resp.coords.latitude, lon: resp.coords.longitude })
        })
        .catch(error => {
          this.locationToSendWithImgs.push({ lat: "", lon: "" });
          console.log(
            "Không thể lấy được vị trí hiện tại khi chụp ảnh của bạn. Xin kiểm tra kết nối và thử lại."
          );
        });
    }
    else {
      this.locationToSendWithImgs.push({ lat: "", lon: "" })
    }
  }

  testUpdateImgURLtoDB(imgLink, type) {

    let dataToSend
    if (type == 'img') {
      console.log('img link to update DB: ' + imgLink)
      dataToSend = { Id: this.currentIDToSendVideos, Attachment: [{ Filepath: imgLink }], VideoAttachment:[] }
    }
    else {
      console.log('video link to update DB: ' + imgLink)
      dataToSend = { Id: this.currentIDToSendVideos, VideoAttachment: [{ Filepath: imgLink }], Attachment:[] }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = this.global.apiURL.update_attachment;
      this.http.post(apiURL, dataToSend, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          console.log('lưu url img thành công')
          // this.testGetCasesByID()
          resolve(res.json());
        },
        err => {
          console.log(err);
          console.log('lưu url img thất bại')
          reject(err);
        }
      );
    });
  }

  testGetCasesByID() {
    return new Promise((resolve, reject) => {
      console.log('ID gửi ảnh: ' + this.currentIDToSendVideos)
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = 'http://222.255.102.205:5001/api/core/v1/issue/get-by-listid';
      let cases = "'" + '17c775fd-db72-45a1-9a54-7ddc0e05d19e' + "'"
      console.log(cases)
      this.http.post(apiURL, { listid: cases }, { headers: headers }).subscribe(
        res => {
          console.log(JSON.parse(res.json().lastResult.data));
          console.log('lấy case thành công')
          resolve(res.json());
        },
        err => {
          console.log(err);
          console.log('lấy case thất bại')
          reject(err);
        }
      );
    });
  }


  //speechToText
  tryAgain() {
    this.showHideVoiceInputComponents(false)
    this.speechRecording()
  }
  speechRecording() {
    this.showHideVoiceInputComponents(false)
    this.speechRecognition.startListening({
      language: "vi-VN",
      showPopup: false
    }).subscribe(matches => {
      document.getElementById('modalClose').click();
      let noidung = document.getElementById('noiDungThongBaoAn') as HTMLTextAreaElement;
      this.ghiChuThem = matches[0];
      noidung.focus()
    },
      onerror => {
        console.log("Lỗi")
        this.showHideVoiceInputComponents(true)
      });
  }
  showHideVoiceInputComponents(hide) {
    if (hide) {
      let a = document.getElementById('voiceInputLabel') as HTMLElement
      a.hidden = true
      a = document.getElementById('voiceInputImg') as HTMLElement
      a.hidden = true
      a = document.getElementById('voiceInputErrorBtns') as HTMLElement
      a.hidden = false
    }
    else {
      let a = document.getElementById('voiceInputLabel') as HTMLElement
      a.hidden = false
      a = document.getElementById('voiceInputImg') as HTMLElement
      a.hidden = false
      a = document.getElementById('voiceInputErrorBtns') as HTMLElement
      a.hidden = true
    }
  }
  stopListening() {
    this.speechRecognition.stopListening().then(() => {
    });
  }
  getSpeechPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          this.speechRecognition.requestPermission();
        }
      });
  }
}
