import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  LoadingController,
  MenuController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
/**
 * Generated class for the UserinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-userinfo",
  templateUrl: "userinfo.html"
})
export class UserinfoPage {
  address = "";
  ward = "";
  district = "";
  city = "";
  businessName = "";
  phone = "";
  cmnd = "";
  name = "";
  loading;
  // apiURL = {
  //   function_gui_anh_xu_ly: 'http://222.255.102.166:9097/api/App/func_UpLoadImages_XuLy/?id=',
  //   func_Gui_DanhGia_Event:'http://222.255.102.166:9097/api/App/func_Gui_DanhGia_Event',
  //   func_layQuaTrinhXuLy_theoSuCo: 'http://222.255.102.166:9097/api/ThongKe/func_layQuaTrinhXuLy_theoSuCo',
  //   informCaseProcessingStartTime:'http://222.255.102.166:9097/api/App/func_ClickView_Event',
  //   gui_video:'http://222.255.102.166:9097/api/App/func_UpLoadVideo/?id=',
  //   gui_imgs_new: 'http://222.255.102.166:9097/api/App/func_UpLoadImages/?id=',
  //   func_XuLy_SuCo_HoanTat: 'http://222.255.102.166:9097/api/App/func_XuLy_SuCo_HoanTat',
  //   func_LogOut: ' http://222.255.102.166:9097/api/App/func_LogOut',
  //   func_XuLy_SuCo_PheDuyetHoanThanh: ' http://222.255.102.166:9097/api/App/func_XuLy_SuCo_PheDuyetHoanThanh',
  //   func_PhanCong: ' http://222.255.102.166:9097/api/App/func_PhanCong',
  //   func_XuLy_SuCo_BaoRac: ' http://222.255.102.166:9097/api/App/func_XuLy_SuCo_BaoRac',
  //   func_DsDonVi_CoTheHoTro: ' http://222.255.102.166:9097/api/App/func_DsDonVi_CoTheHoTro',
  //   func_DsNhanVien: ' http://222.255.102.166:9097/api/App/func_DsNhanVien',
  //   func_laydsDM_MucDo: ' http://222.255.102.166:9097/api/App/func_laydsDM_MucDo',
  //   func_laydsPhuong_Quan: ' http://222.255.102.166:9097/api/App/func_laydsPhuong_Quan',
  //   func_laydsTinh: ' http://222.255.102.166:9097/api/App/func_laydsTinh',
  //   func_laydsQuan_All: ' http://222.255.102.166:9097/api/App/func_laydsQuan_All',
  //   func_laydsPhuong_All: ' http://222.255.102.166:9097/api/App/func_laydsPhuong_All',
  //   func_DsSuCo_ChuaXuLy: ' http://222.255.102.166:9097/api/App/func_DsSuCo_ChuaXuLy',
  //   func_ChangPass: ' http://222.255.102.166:9097/api/App/func_ChangPass',
  //   func_DsSuCo_DaGui: ' http://222.255.102.166:9097/api/App/func_DsSuCo_DaGui',
  //   func_GuiSuCo_New: ' http://222.255.102.166:9097/api/App/func_GuiSuCo_New',
  //   func_DangKiThongTin_Vip: ' http://222.255.102.166:9097/api/App/func_DangKiThongTin_Vip',
  //   func_KiemTraPassword_BaBuoc: ' http://222.255.102.166:9097/api/App/func_KiemTraPassword_BaBuoc',
  //   func_KiemTraMaNguoiDung_BaBuoc: ' http://222.255.102.166:9097/api/App/func_KiemTraMaNguoiDung_BaBuoc'
  // };

  constructor(
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation
  ) { }
  openMenu() {
    this.menu.open();
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad UserinfoPage");
  }
  ionViewWillEnter() {
    this.getSavedInfo();
  }
  getSavedInfo() {
    this.global.presentLoadingCustom('')
    let a = this.storage.get("userName").then(val => {
      this.name = val;
    });
    let b = this.storage.get("userPhone").then(val => {
      this.phone = val;
    });
    let c = this.storage.get("userCMND").then(val => {
      this.cmnd = val;
    });
    let d = this.storage.get("userBusinessName").then(val => {
      this.businessName = val;
    });
    let e = this.storage.get("userWard").then(val => {
      this.ward = val;
    });
    let f = this.storage.get("userCity").then(val => {
      this.city = val;
    });
    let g = this.storage.get("userDistrict").then(val => {
      this.district = val;
    });
    let h = this.storage.get("userAddress").then(val => {
      this.address = val;
    });
    // let i=this.getCityInfo();
    // let j=this.getWardInfo();
    // let k=this.getDistrictInfo();
    let self = this;
    // Promise.all([i]).then(function(values) {
    //   console.log(values);      
    // });
    Promise.all([a, b, c, d, e, f, g, h,]).then(function (values) {
      console.log(values);
      self.global.cancleCustomLoading();
    });

  }
  getWardInfo(code) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.func_laydsPhuong_Quan + '/' + code;
      this.http.get(apiURL, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          if (res.json().code == 1) {
            this.global.wardList = res.json().data
            let self = this;
            // this.global.cancleCustomLoading();
          } else {
            let self = this;
            // this.global.cancleCustomLoading();
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
          }
          resolve(res.json());
        },
        err => {
          console.log(err);
          // this.global.cancleCustomLoading();
          this.presentToast("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.");
          reject(err);
        }
      );
    });
  }
  onDistrictChange(code) {
    console.log(code)
    this.getWardInfo(code);
  }
  getDistrictInfo(code) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.func_laydsQuan_All + '/' + code;
      this.http.get(apiURL, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          if (res.json().code == 1) {
            let self = this;
            this.global.districtList = res.json().data
            // this.global.cancleCustomLoading();
          } else {
            let self = this;
            // this.global.cancleCustomLoading();
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
          }
          resolve(res.json());
        },
        err => {
          console.log(err);
          // this.global.cancleCustomLoading();
          this.presentToast("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.");
          reject(err);
        }
      );
    });
  }
  onCityChange(code) {
    console.log(code)
    this.getDistrictInfo(code);
  }
  getCityInfo() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.func_laydsTinh;
      this.http.get(apiURL, { headers: headers }).subscribe(
        res => {
          console.log(res.json());
          if (res.json().code == 1) {
            this.global.cityList = res.json().data
            let self = this;
            // this.global.cancleCustomLoading();
          } else {
            let self = this;
            // this.global.cancleCustomLoading();
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
          }
          resolve(res.json());
        },
        err => {
          console.log(err);
          // this.global.cancleCustomLoading();
          this.presentToast("Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.");
          reject(err);
        }
      );
    });
  }
  showInfo() {
    this.presentToast(
      "Thông tin người dùng chỉ nhập 1 lần, chỉnh sửa thông tin tại menu chính"
    );
  }
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  updateInfo() {
    if (
      this.name == "" ||
      this.phone == "" ||
      this.cmnd == "" 
    )
      this.presentToast(
        "Xin bạn vui lòng cung cấp đầy đủ các thông tin bắt buộc"
      );
    else if (!this.global.validateSpecialCharacter(this.name, "name").res) {
      this.presentToast(this.global.validateSpecialCharacter(this.name, "name").msg);
    }
    else if (!this.global.validateSpecialCharacter(this.phone, "phone").res) {
      this.presentToast(this.global.validateSpecialCharacter(this.phone, "phone").msg);
    }
    else if (!this.global.validateSpecialCharacter(this.cmnd, "cmnd").res) {
      this.presentToast(this.global.validateSpecialCharacter(this.cmnd, "CMND").msg);
    }
    else {
      console.log(this.ward + '/' + this.city + '/' + this.district)
      this.global.presentLoadingCustom('')

      //without VIP user
      let a = this.storage.set("userName", this.name);
      let b = this.storage.set("userPhone", this.phone);
      let c = this.storage.set("userCMND", this.cmnd);
      let d = this.storage.set("userBusinessName", this.businessName);
      let e = this.storage.set("userWard", this.ward);
      let f = this.storage.set("userCity", this.city);
      let g = this.storage.set("userDistrict", this.district);
      let h = this.storage.set("userAddress", this.address);
      let self = this;
      Promise.all([a, b, c, d, e, f, g, h]).then(function (values) {
        console.log(values);
        this.global.cancleCustomLoading();
        self.presentToast('Thông tin của bạn đã được cập nhật thành công.')
      });

      //With VIP user
      // let nameParts = this.name.split(" ");
      // return new Promise((resolve, reject) => {
      //   let headers = new Headers();
      //   headers.append("Content-Type", "application/json");
      //   // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      //   let apiURL = this.global.apiURL.func_DangKiThongTin_Vip;
      //   this.http
      //     .post(
      //       apiURL,
      //       {
      //         MA_DANG_KY: "",
      //         MA_XAC_THUC: "",
      //         MA_KHACH_HANG: "",
      //         HO: nameParts[0],
      //         TEN: nameParts[1],
      //         SO_DIEN_THOAI: this.phone,
      //         lon: "",
      //         lat: "",
      //         DIA_DIEM_DC: this.address,
      //         DIA_DIEM_PHUONG: this.ward,
      //         DIA_DIEM_QUAN: this.district,
      //         DIA_DIEM_TINH: this.city,
      //         TEN_DN: this.businessName
      //       },
      //       { headers: headers }
      //     )
      //     .subscribe(
      //       res => {
      //         console.log(res.json());
      //         if (res.json().code == 1) {
      //           let a = this.storage.set("userName", this.name);
      //           let b = this.storage.set("userPhone", this.phone);
      //           let c = this.storage.set("userCMND", this.cmnd);
      //           let d = this.storage.set("userBusinessName", this.businessName);
      //           let e = this.storage.set("userWard", this.ward);
      //           let f = this.storage.set("userCity", this.city);
      //           let g = this.storage.set("userDistrict", this.district);
      //           let h = this.storage.set("userAddress", this.address);
      //           let self = this;
      //           Promise.all([a, b, c, d, e, f, g, h]).then(function(values) {
      //             console.log(values);
      //             this.global.cancleCustomLoading();
      //           });
      //         } else {
      //           let self = this;
      //           this.global.cancleCustomLoading();
      //           this.presentToast(
      //             "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
      //           );
      //         }
      //         resolve(res.json());
      //       },
      //       err => {
      //         console.log(err);
      //         this.global.cancleCustomLoading();
      //         this.presentToast(
      //           "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
      //         );
      //         reject(err);
      //       }
      //     );
      // });
    }
  }
}
