import { Component } from '@angular/core';
import { NavController, IonicPage, ToastController, AlertController, ModalController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MediaPage } from '../media/media'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { ChatRoomPage } from '../../pages/chat-room/chat-room';
import { Observable } from 'rxjs/Observable';
import { Map15Page } from '../map15/map15';
import { AssignconfirmPage } from '../assignconfirm/assignconfirm';
import { Slides } from 'ionic-angular';
import { ViewChild } from '@angular/core';
/**
 * Generated class for the ProcessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-process',
  templateUrl: 'process.html',
})
export class ProcessPage {
  @ViewChild('mySlider') slides: Slides;

  toggle = 'slide1'
  tokenOut=false;
  showCalendar=false
  prevPickdate = "";
  calendarDialog
  hasCalendar = false
  pickdate
  loading
  listOfWaitingTobeProcessed=[]
  listOfWaitingTobeSupported=[]
  originalList=[];
  constructor(public menuCtrl:MenuController,public loadingCtrl:LoadingController,public menu: MenuController, public navParams: NavParams, public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private geolocation: Geolocation) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    // this.ionViewDidEnter()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SentcasesPage');
    this.calendarDialog = document.getElementById('calendarDialog') as HTMLElement;
  }
  ionViewWillEnter() {
    this.pickdate = new Date(Date.now()).toLocaleDateString('en-GB')
    this.getUnassignedCases();
    this.getUnassignedSupportCases();
    this.showCalendar=false;
  } 
  onDaySelect(event) {
    console.log(event);
    let dateForCheck = event.date + "/" + (event.month + 1) + "/" + event.year;
    if (this.global.compare2Dates(dateForCheck)) {
      this.pickdate = dateForCheck;
    } else
      this.presentToast(
        "Xin chọn ngày tìm kiếm nhỏ hơn hoặc bằng ngày hiện tại."
      );
  }

  closeCalendar() {
    // this.calendarDialog.close();
    this.showCalendar=false;
    this.searchByDate();
  }

  searchByDate() {
    if (this.prevPickdate == "" && this.pickdate != "") {
      console.log("search lần đầu");
      this.prevPickdate = this.pickdate;
      this.listOfWaitingTobeProcessed=this.global.searchDate(this.pickdate,this.originalList);      
    } else if (this.prevPickdate != this.pickdate) {
      console.log("search các lần tiếp theo");
      this.prevPickdate = this.pickdate;
      this.listOfWaitingTobeProcessed=this.global.searchDate(this.pickdate,this.originalList);
    } else {
      console.log("trùng ngày hoặc ngày ko hợp lệ ko search");
    }
  }
  openCalendar() {
    // this.calendarDialog.showModal()
    this.showCalendar=true;
  }
  openMenu() {
    this.menu.open()
  }
  showHideCalendarButton() {
    if (this.hasCalendar)
      this.hasCalendar = false
    else
      this.hasCalendar = true
  }
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  
  getUnassignedCases()
  {
   this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token",this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_DsSuCo_ChuaXuLy;  
      let assignee=[]
      assignee.push(this.global.currentUserInfo.nguoidung_id)
      this.http
        .post(
          apiURL,
          {
            listAssignStatus: ["0"],
            assignee: assignee,
            liststatus:["12"]
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            if (res.json().code==1) {
              //get list cases is ok
              this.global.cancleCustomLoading();
              this.listOfWaitingTobeProcessed=res.json().data;
              this.originalList=this.listOfWaitingTobeProcessed;
            }
            else if (res.json().code==2) {
              this.logoutDueToTokenExpired()
            }
            else {
              let self = this;
              this.global.cancleCustomLoading();
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading();
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }


  getUnassignedSupportCases()
  {
    this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token",this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_DsSuCo_ChuaXuLy;
      let assignee=[]
      assignee.push(this.global.currentUserInfo.nguoidung_id)
      this.http
        .post(
          apiURL,
          {
            listAssignStatus: ["0"],
            assignee: assignee,
            liststatus:["22"]
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            if (res.json().code==1) {
              //get list cases is ok
              this.global.cancleCustomLoading();
              this.listOfWaitingTobeSupported=res.json().data;
              // this.originalList=this.listOfWaitingTobeProcessed;
            }
            else if (res.json().code==2) {
              this.logoutDueToTokenExpired()
            }
            else {
              let self = this;
              this.global.cancleCustomLoading();
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading();
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  goToAssignConfirm(item)
  {
    console.log(item)
    this.navCtrl.push(AssignconfirmPage,{case:item, page:'process'})
  }

  slideChanged() {
    let active = this.slides.getActiveIndex();
    if (active == 0)
      this.toggle = 'slide1'
    else
      this.toggle = 'slide2'
  }
  changeTab(slide) {
    if (slide == 'slide1')
      this.slides.slideTo(0);
    else
      this.slides.slideTo(1);
  }
  logoutDueToTokenExpired() {
    this.global.cancleCustomLoading();
    if(!this.tokenOut)
    {
      this.tokenOut=true;
      alert("Phiên đăng nhập của bạn đã kết thúc, xin vui lòng đăng nhập lại.")
      this.global.islogin = false
      this.storage.set('autoLogin', false);
      this.menuCtrl.enable(true, "menuBeforeLogin");
      this.menuCtrl.enable(false, "menuAfterLogin");
      this.navCtrl.popToRoot();
    }
  }
}
