import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser'
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Base64 } from '@ionic-native/base64';
import { File, FileEntry } from '@ionic-native/file';
import { AlertController } from 'ionic-angular';
import { JwtHelper } from "jwt-simple";
import { Geolocation } from '@ionic-native/geolocation';

declare var require: any;
/**
 * Generated class for the MediaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-media',
  templateUrl: 'media.html',
})
export class MediaPage {
  listHasElements = false;
  hasVideos = false;
  imageURL = []
  base64ImgList = []
  videoFilePath
  rawVideoPath
  number
  toggle
  verifiredPhoneNumber
  loading
  loadingcounter
  currentLat
  currentLon
  constructor(private geolocation: Geolocation, private alertCtrl: AlertController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, private file: File, private base64: Base64, public http: Http, private imagePicker: ImagePicker, public sanitizer: DomSanitizer, private mediaCapture: MediaCapture, private camera: Camera, public navCtrl: NavController, public navParams: NavParams) {
  }
  presentAlert(title, message) {
    let alert = this.alertCtrl.create({
      enableBackdropDismiss: false,
      title: title,
      message: message,
      buttons: [
        {
          text: 'Đóng',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    alert.present();
  }

  ionViewWillEnter() {
    this.loadingcounter = 0;
    this.number = this.navParams.get('number')
    this.verifiredPhoneNumber = this.navParams.get('verifiredPhoneNumber')
    console.log(this.number)
    this.toggle = "picture"

    if (this.imageURL.length >= 1)
      this.listHasElements = true
    else
      this.listHasElements = false
    if (this.videoFilePath != null)
      this.hasVideos = true
    else
      this.hasVideos = false

    this.geolocation.getCurrentPosition({
      timeout: 5000,
      enableHighAccuracy: true,
      maximumAge: Infinity
    }).then((resp) => {
      this.currentLat=resp.coords.latitude
      this.currentLon=resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MediaPage');
  }
  onSwipe() {
    if (this.toggle == "picture")
      this.toggle = "video"
    else
      this.toggle = "picture"
  }
  delPic(pic) {
    for (var i = 0; i < this.imageURL.length; ++i) {
      if (pic == this.imageURL[i]) {
        this.imageURL.splice(i, 1)
        this.base64ImgList.splice(i, 1)
        break;
      }
    }
    if (this.imageURL.length >= 1)
      this.listHasElements = true
    else
      this.listHasElements = false
  }
  takePhoto() {
    if (this.imageURL.length == 5) {
      alert("Bạn chỉ được gửi tối đa 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    }
    else {
      let options: CameraOptions = {
        quality: 10,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      }
      this.camera.getPicture(options).then((imageData) => {
        this.imageURL.push(imageData)
        this.imgToBase64(imageData)
        this.listHasElements = true
      }, (err) => {
        console.log(err); 
      });
    }
  }
  localImagePicker() {
    if (this.imageURL.length == 5) {
      alert("Bạn chỉ được gửi tối đa 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")

      // alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    }
    else {
      let count = 5 - this.imageURL.length
      let options: ImagePickerOptions = { quality: 10, maximumImagesCount: count };
      this.imagePicker.getPictures(options).then((results) => {
        // for (var i = 0; i < results.length; i++) {
        //   console.log('Image URI: ' + results[i]);
        // }
        for (let pic of results) {
          this.imageURL.push(pic)
          this.imgToBase64(pic)
        }
        if (this.imageURL.length >= 1)
          this.listHasElements = true
        else
          this.listHasElements = false
      }, (err) => { });
    }
  }

  sendPics_Videos() {
    if (this.imageURL.length < 1 && (this.videoFilePath == "" || this.videoFilePath == null))
      // alert("Bạn hãy chọn ít nhất 1 tấm ảnh hay video để có thể tiến hành gửi cho " + this.number)
      alert("Bạn hãy chọn ít nhất 1 video để có thể tiến hành gửi cho " + this.number)
    else {
      this.loading = this.loadingCtrl.create({
        content: 'Đang tiến hành gửi nội dung, xin vui lòng đừng tắt ứng dụng hay thiết bị.'
      });
      this.loading.present();
      if (this.videoFilePath != null && this.videoFilePath != "") {
        //call videos sender
        this.loadingcounter += 1;
        this.videosSender()
      }
      else if (this.imageURL.length >= 1) {
        //call images sender
        this.loadingcounter += 1;
        this.imagesSender()
      }
    }
  }

  imagesSender() {

    let url = "http://crm.eoc.vnptmedia.vn/api/app11x/upload-images";
    var headers = new Headers()
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers });
    let body = "data=" + this.JWTEncode("imgs");
    for (let pic of this.base64ImgList) {
      body = body + "&images[]=" + pic
    }
    console.log(body)
    return this.http.post(url, body, options).map(res => res.json()).subscribe(
      data => {
        console.log(data);
        if (data.success == true) {
          this.loadingcounter -= 1
          if (this.loadingcounter == 0) {
            this.loading.dismiss();
            this.presentAlert('Thông báo', "Đã gửi thành công. Xin cảm ơn thông tin của bạn.")
            this.navCtrl.pop()
          }
        }
      },
      err => {
        this.loading.dismiss();
        this.presentAlert('Thông báo', err)
        this.navCtrl.pop()
      }
    );
  }

  ionViewWillLeave() {
    this.imageURL = []
    this.base64ImgList = []
    this.videoFilePath = ""
    this.rawVideoPath = ""
    this.loadingcounter = 0
  }
  imgToBase64(pic) {
    let filePath: string = pic;
    this.base64.encodeFile(filePath).then((base64File: string) => {
      console.log(base64File);
      this.base64ImgList.push(base64File)
    }, (err) => {
      console.log(err);
    });
  }

  videosSender() {
    // var fs = require('fs')
    // let url = "https://cors-anywhere.herokuapp.com/http://demo.pbxcrm.frontend/api/app11x/upload-videos";
    // let httpOptions = {
    //   headers: new Headers({
    //     'enctype': 'multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA'
    //   })
    // };

    // let formData = new FormData();
    // formData.append('data', this.JWTEncode("videos"));    
    // formData.append('uploadedFiles', fs.createReadStream(this.rawVideoPath));      

    // console.log("post photo to URL at " + url);
    // return this.http
    //   .post<SimpleResponse>(
    //     url,
    //     formData,
    //     httpOptions
    //   );
    // this.error = null;
    // this.loading = this.loadingCtrl.create({
    //   content: 'Uploading...'
    // });

    // this.loading.present();

    console.log("file://" + this.rawVideoPath)
    if (this.rawVideoPath.startsWith("file")) {
      this.file.resolveLocalFilesystemUrl(this.rawVideoPath)
        .then(entry => {
          (entry as FileEntry).getMetadata((metadata) => {
            console.log("video size : " + metadata.size);
            // check if the file size is larger then the allowed threshold
            if (metadata.size / 1000000 > 50) {
              this.loading.dismiss();
              this.presentAlert('Thông báo', "Dung lượng file tối đa cho phép là 50MB, xin vui lòng lựa chọn 1 file khác.")
              this.loadingcounter = 0
            }
            else {
              (entry as FileEntry).file(file => this.readFile(file))
              if (this.imageURL.length >= 1) {
                //call images sender
                this.loadingcounter += 1;
                this.imagesSender()
              }
            }

          });
        })
        .catch(err => console.log(err));
    }
    else {
      this.file.resolveLocalFilesystemUrl("file://" + this.rawVideoPath)
        .then(entry => {
          (entry as FileEntry).getMetadata((metadata) => {
            if (metadata.size / 1000000 > 50) {
              this.loading.dismiss();
              this.presentAlert('Thông báo', "Dung lượng file tối đa cho phép là 50MB, xin vui lòng lựa chọn 1 file khác.")
              this.loadingcounter = 0
            }
            else {
              (entry as FileEntry).file(file => this.readFile(file))
              if (this.imageURL.length >= 1) {
                //call images sender
                this.loadingcounter += 1;
                this.imagesSender()
              }
            }
          });
        })
        .catch(err => console.log(err));
    }
  }

  private readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      formData.append('uploadedFiles[]', imgBlob, file.name);
      formData.append('data', this.JWTEncode("videos"))
      this.postData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

  private postData(formData: FormData) {
    console.log("Here and ready to be uploaded")
    // let url = "https://cors-anywhere.herokuapp.com/http://crm.eoc.vnptmedia.vn/api/app11x/upload-videos";
    let url = "http://crm.eoc.vnptmedia.vn/api/app11x/upload-videos";
    let headers = new Headers({
      'enctype': 'multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA'
    })
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .map(res => res.json())
      .subscribe(
        data => {
          console.log(data);
          if (data.success == true) {
            this.loadingcounter -= 1
            if (this.loadingcounter == 0) {
              this.loading.dismiss();
              this.presentAlert('Thông báo', "Đã gửi thành công. Xin cảm ơn thông tin của bạn.")
              this.navCtrl.pop()
            }
          }
          else {
            this.loadingcounter -= 1
            if (this.loadingcounter == 0) {
              this.loading.dismiss();
              this.presentAlert('Thông báo', data.message)
              this.navCtrl.pop()
            }

          }
        },
        err => {
          this.loading.dismiss();
          this.presentAlert('Thông báo', err)
          this.navCtrl.pop()
        }
      );
  }

  JWTEncode(type) {
    if (type == "imgs") {
      var jwt = require('jwt-simple');
      
      var payload = {
        "msisdn": this.verifiredPhoneNumber,
        "dial_number": this.number,
        "file_type ": "images",
        "lat": this.currentLat+"",
        "lng": this.currentLon+""
      };
      var secret = 'NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ';
      // encode
      var token = jwt.encode(payload, secret);
      console.log(token)
      return token;
      // decode
      // var decoded = jwt.decode(token, secret);
    }
    if (type == "videos") {
      var jwt = require('jwt-simple');
      var payload = {
        "msisdn": this.verifiredPhoneNumber,
        "dial_number": this.number,
        "file_type ": "videos",
        "lat": this.currentLat+"",
        "lng": this.currentLon+""
      };
      var secret = 'NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ';
      // encode
      var token = jwt.encode(payload, secret);
      console.log(token)
      return token;
      // decode
      // var decoded = jwt.decode(token, secret);
    }
  }

  recordVideo() {
    if (this.hasVideos)
      alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    else {
      let options: CaptureVideoOptions = { limit: 1, quality: 0 };
      this.mediaCapture.captureVideo(options)
        .then((data: MediaFile[]) => {
          console.log(data);
          var i, path, len;
          for (i = 0, len = data.length; i < len; i += 1) {
            console.log(path);
            // data[i].fullPath = "file:/storage/extSdCard/DCIM/Camera/20160827_225041.mp4"
            // data[i].localURL = "cdvfile://localhost/root/storage/extSdCard/DCIM/Camera/20160827_225041.mp4"
            // How do I display this video to the user?
            this.rawVideoPath = data[i].fullPath;
            this.videoFilePath = this.sanitizer.bypassSecurityTrustUrl(data[i].fullPath);
            console.log(this.videoFilePath)
          }
          if (this.videoFilePath != null)
            this.hasVideos = true
          else
            this.hasVideos = false
        },
          (err: CaptureError) => {
            console.error(err);
          }
        );
    }

  }
  delVideo() {
    this.videoFilePath = ""
    this.rawVideoPath = ""
    this.hasVideos = false
  }
  localVideoPicker() {
    if (this.hasVideos)
      alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    else {
      var options = {
        quality: 0,
        destinationType: this.camera.DestinationType.FILE_URI,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: this.camera.MediaType.VIDEO
      };
      this.camera.getPicture(options).then((results) => {
        this.rawVideoPath = results
        this.videoFilePath = this.sanitizer.bypassSecurityTrustUrl(results);
        if (this.videoFilePath != null)
          this.hasVideos = true
        else
          this.hasVideos = false
      }, (err) => {
        // Handle error
      });

    }
  }

  showToast(data: any) {
    let toast = this.toastCtrl.create({
      message: data,
      duration: 4000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
