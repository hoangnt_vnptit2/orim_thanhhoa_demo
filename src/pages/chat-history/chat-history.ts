import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ChatHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-chat-history',
  templateUrl: 'chat-history.html',
})

export class ChatHistoryPage {
  @ViewChild('content') content: any;

  loading
  dialNumber
  oldHistory = [];
  empty = true;
  shouldScrollDown = true
  lastScrollTop = 0
  anyScroll =false
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage, private loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatHistoryPage');
  }

  ionViewWillEnter() {
    this.dialNumber = this.navParams.get('dialNumber');
    console.log(this.dialNumber)
    this.loading = this.loadingCtrl.create({
      content: 'Đang lấy lịch sử tin nhắn...'
    });
    this.loading.present();
    this.getHistory()

  }


  ionViewDidEnter() {
    this.scrollingDetection()
    // this.myFunction()
  }

  scrollingDetection() {
    this.content.ionScrollEnd.subscribe((data) => {

      if(!this.anyScroll)
      {
        let arrow= document.getElementById('addQA') as HTMLElement
        arrow.hidden=false
      }
      let dimensions = this.content.getContentDimensions();

      let scrollTop = this.content.scrollTop;
      let contentHeight = dimensions.contentHeight;
      let scrollHeight = dimensions.scrollHeight;

      if ((scrollTop + contentHeight + 20) > scrollHeight) {
        this.shouldScrollDown = false;
        let arrow= document.getElementById('addQA') as HTMLElement       
        arrow.innerHTML='<img  src=\"imgs/aUp.png\"/>'
        console.log('up')
      } else {
        this.shouldScrollDown = true;
        let arrow= document.getElementById('addQA') as HTMLElement       
        arrow.innerHTML='<img  src=\"imgs/aDown.png\"/>'
        console.log('down')
      }

    });
  }
  scrollTopBottom()
  {
    console.log(this.shouldScrollDown)
    if(this.shouldScrollDown)
      this.content.scrollToBottom()
    else
      this.content.scrollToTop()
  }
 
  // doInfinite(infiniteScroll) {
  //   console.log('Begin async operation');

  //   setTimeout(() => {
  //     // for (let i = 0; i < 30; i++) {
  //     //   this.items.push( this.items.length );
  //     // }

  //     console.log('Async operation has ended');
  //     infiniteScroll.complete();
  //   }, 500);
  // }

  getHistory() {

    if (this.dialNumber == '113') {
      this.storage.get('113_ChatHistory').then((val) => {
        this.oldHistory = JSON.parse(val);
        console.log(this.oldHistory)
        if (this.oldHistory.length > 0)
          this.empty = false
        this.loading.dismiss()
      });
    }
    else if (this.dialNumber == '114') {
      this.storage.get('114_ChatHistory').then((val) => {
        this.oldHistory = JSON.parse(val);
        console.log(this.oldHistory)
        if (this.oldHistory.length > 0)
          this.empty = false
        this.loading.dismiss()
      });
    }
    else {
      this.storage.get('115_ChatHistory').then((val) => {
        this.oldHistory = JSON.parse(val);
        console.log(this.oldHistory)
        if (this.oldHistory.length > 0)
          this.empty = false
        this.loading.dismiss()
      });
    }
  }


}
