import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, ModalController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MediaPage } from '../media/media'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { SmsPage } from '../../pages/sms/sms';
import { ChatRoomPage } from '../../pages/chat-room/chat-room';
import { Observable } from 'rxjs/Observable';

declare var require: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  phoneNumber: any;
  username = ""
  password = ""
  currentOTPCode = 0
  phoneNumberVerified: any;
  verifedNumber = null;
  tempNumber = "";
  firstTimeRunning = null
  connection: Observable<any>;

  PERMISSION = {
    WRITE_EXTERNAL: this.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
    CAMERA: this.diagnostic.permission.CAMERA,
    CALL_PHONE: this.diagnostic.permission.CALL_PHONE,
    ACCESS_FINE_LOCATION: this.diagnostic.permission.ACCESS_FINE_LOCATION,
    ACCESS_COARSE_LOCATION: this.diagnostic.permission.ACCESS_COARSE_LOCATION,
  };

  constructor(public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private callNumber: CallNumber, private geolocation: Geolocation) {
    if (this.platform.is('android')) {
      this.requestAllPermissions();
    }
  }

  requestAllPermissions() {
    const permissions = Object.keys(this.PERMISSION).map(k => this.PERMISSION[k]);
    this.diagnostic.requestRuntimePermissions(permissions).then((status) => {
      console.log(JSON.stringify(status));
    }, error => {
      console.log('Error: ' + error);
    });
  }

  ionViewWillEnter() {
    this.phoneNumberVerified = true;
    this.verifedNumber = this.global.number;
    console.log(this.verifedNumber)
  }

  ionViewDidEnter() {

  }
  checkBlackList() {
    return true;
  }

  savePhoneNumber(num) {
    this.verifedNumber = num;
    this.storage.set('verifiredPhoneNumber', num)
  }
  informOTP() {
    if (this.checkBlackList()) {
      if (this.phoneValidator(this.phoneNumber)) {
        this.OTPcall()
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: 'Mã OTP',
          cssClass: 'custom-alert',
          message: 'Một mã OTP sẽ được gửi đến số điện thoại của bạn. Xin nhập mã được cung cấp (mã sẽ hết hiệu lực sau 5 phút) vào khung bên dưới và nhấn \'Xác Thực\' để hoàn tất:',
          inputs: [
            {
              name: 'OTP_Code',
              placeholder: 'Mã OTP'
            }
          ],
          buttons: [
            {
              text: 'Hủy',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Gửi Lại Mã',
              handler: () => {
                this.currentOTPCode = 0;
                this.informOTP();
              }
            },
            {
              text: 'Xác Thực',
              handler: data => {
                this.OTPVerification(data.OTP_Code);
              }
            }
          ]
        });
        alert.present();
      }

      else {
        this.presentToast("Định dạng số điện thoại không đúng. Xin vui lòng kiểm tra lại")
      }

    }
  }

  phoneValidator(phone) {
    var filter = /^[0-9-+]+$/;
    if (filter.test(phone)) {
      if (phone.length == 10 || phone.length == 11) {
        if (phone.length == 10) {
          if (phone.substring(0, 2) == "09" || phone.substring(0, 2) == "08") {
            return true;
          } else {
            return false;
          }
        } else if (phone.substring(0, 2) == "01") {
          return true;
        } else {
          return false;
        }
      }
      else {
        return false;
      }
    }
  }

  OTPcheckk() {
    return new Promise((resolve, reject) => { })
  }

  OTPcall() {
    console.log(this.phoneNumber)
    return new Promise((resolve, reject) => {
      if (this.currentOTPCode == 0) {
        this.generateOTPCode();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
        let apiURL = `http://123.20.207.45/safecity_dev/api/QLSuCo/func_GuiSMS`;
        this.http.post(apiURL, { username: this.username, password: this.password, sdt: this.phoneNumber, noidung: this.currentOTPCode }, { headers: headers })

          .subscribe(res => {
            console.log(res.json());
            var start = res.json().msg.indexOf('[');
            var end = res.json().msg.indexOf(']');
            var temp = res.json().msg.substring(start + 1, end)
            console.log(temp)
            this.tempNumber = temp;
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
      }
    });
  }

  OTPVerification(OTPInput) {
    if (OTPInput == this.currentOTPCode) {
      this.phoneNumberVerified = true;
      if (this.tempNumber != "")
        this.savePhoneNumber(this.tempNumber)
      let alert = this.alertCtrl.create({
        title: 'Mã OTP hợp lệ',
        message: 'Số điện thoại của bạn đã được xác thực. Bạn đã có thể bắt đầu sử dụng ứng dụng. Xin cảm ơn.',
        buttons: [
          {
            text: 'Đồng ý',
            role: 'cancel',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Mã OTP không hợp lệ',
        message: 'Mã số bạn nhập không hợp lệ. Xin vui lòng thử lại',
        buttons: [
          {
            text: 'Đồng ý',
            role: 'cancel',
            handler: () => {
              this.informOTP();
            }
          },
          {
            text: 'Bỏ qua',
            role: 'cancel',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
    }
  }

  sendLocation(lat, lon, dialNumber) {
    let url = "http://crm.eoc.vnptmedia.vn/api/app11x/update-phonebook";
    var headers = new Headers()
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers });
    let body = "data=" + this.JWTEncode(lat, lon, dialNumber);
    return this.http.post(url, body, options).map(res => res.json()).subscribe(
      data => {
        console.log("location's been sent")
        console.log(data);
      },
      err => {
        console.log("ERROR!: ", err);
      }
    );
  }

  JWTEncode(lat, lon, dialNumber) {

    switch (dialNumber) {
      case '113':
        dialNumber = "02836228113"
        break;
      case '114':
        dialNumber = "02836228114"
        break;
      case '115':
        dialNumber = "02836228115"
        break;
    }

    var jwt = require('jwt-simple');
    var payload = {
      "msisdn": this.verifedNumber,
      "dial_number": dialNumber + "",
      "lat": lat,
      "lng": lon
    };
    var secret = 'NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ';
    // encode
    var token = jwt.encode(payload, secret);
    console.log(token)
    return token;
    // decode
    // var decoded = jwt.decode(token, secret);
  }
  generateOTPCode() {
    let val = Math.floor(1000 + Math.random() * 9000);
    if (this.currentOTPCode != 0) {
      if (val != this.currentOTPCode) {
        this.currentOTPCode = val;
        setTimeout(function () { this.currentOTPCode = 0; }, 500000);
      }
      else
        this.generateOTPCode();
    }
    else {
      this.currentOTPCode = val;
      setTimeout(function () { this.currentOTPCode = 0; }, 500000);
    }
    console.log(this.currentOTPCode)
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  presentToastWithCall(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'top'
    });

    // toast.onDidDismiss(() => {
    //   this.callNumber.callNumber("01298120089", true)
    //     .then(res => console.log('Launched dialer!', res))
    //     .catch(err => console.log('Error launching dialer', err));
    // });
    toast.present();
  }

  smsSend(number) {
    this.connection = this.http.get('https://jsonplaceholder.typicode.com/todos/1');
    this.connection
      .subscribe(data => {
        console.log(data);
        //if data connection is available, then send messages using OTT
        this.joinChat(number);
      }, error => {
        console.log('no connection')
        //if data connection is not available, then send messages using regular SMS
        // let myModal = this.modalCtrl.create(SmsPage, { parentPage: this, dialNumber: number }, { enableBackdropDismiss: false });
        // myModal.present();
        this.presentToast("Hiện không có kết nối mạng, xin bạn vui lòng thử lại sau.")
      })
  }

  joinChat(number) {
    // var a=this.socket.connect()
    // console.log(a)
    // this.socket.emit('set-nickname', this.verifedNumber);
    this.navCtrl.push(ChatRoomPage, { nickname: this.verifedNumber, dialNumber: number });
  }
  // callMaking(number) {
  //   var self = this
  //   if (this.phoneNumberVerified && this.verifedNumber != null) {
  //     setTimeout(function () {
  //       debugger
  //       switch (number) {
  //         case '113':
  //           self.callNumber.callNumber("02836228113", true)
  //             .then(res => console.log('Launched dialer!', res))
  //             .catch(err => console.log('Error launching dialer', err));
  //           break;
  //         case '114':
  //           self.callNumber.callNumber("02836228114", true)
  //             .then(res => console.log('Launched dialer!', res))
  //             .catch(err => console.log('Error launching dialer', err));
  //           break;
  //         case '115':
  //           self.callNumber.callNumber("02836228115", true)
  //             .then(res => console.log('Launched dialer!', res))
  //             .catch(err => console.log('Error launching dialer', err));
  //           break;
  //       }
  //     },
  //       2000);

  //     this.geolocation.getCurrentPosition({
  //       timeout: 5000,
  //       enableHighAccuracy: true,
  //       maximumAge: Infinity
  //     }).then((resp) => {
  //       this.sendLocation(resp.coords.latitude, resp.coords.longitude, number);
  //       this.presentToastWithCall("Số DT: " + this.verifedNumber + " Vị trí: " + "lat: " + resp.coords.latitude + " long: " + resp.coords.longitude)
  //     }).catch((error) => {
  //       console.log('Error getting location', error);
  //     });
  //   }
  //   else {
  //     this.presentToast("Bạn cần nhập và xác thực số điện thoại của mình trước khi có thể sử dụng ứng dụng.")
  //   }
  // }
  callMaking(number) {
    var self = this
    if (this.phoneNumberVerified && this.verifedNumber != null) {
      this.diagnostic.isGpsLocationEnabled()
        .then((state) => {
          if (!state) {
            console.log('noGPS enabled')
            let alert = this.alertCtrl.create({
              title: 'Cài Đặt GPS',
              message: 'Quý khách vui lòng cân nhắc mở GPS (\"Vị Trí/Location\") trong \"Cài Đặt/Settings\" của thiết bị để chúng tôi có thể phục vụ bạn tốt hơn.',
              buttons: [
                {
                  text: 'Tiếp tục',
                  role: 'cancel',
                  handler: () => {
                    switch (number) {
                      case '113':
                        self.callNumber.callNumber("02836228113", true)
                          .then(res => console.log('Launched dialer!', res))
                          .catch(err => console.log('Error launching dialer', err));
                        break;
                      case '114':
                        self.callNumber.callNumber("02836228114", true)
                          .then(res => console.log('Launched dialer!', res))
                          .catch(err => console.log('Error launching dialer', err));
                        break;
                      case '115':
                        self.callNumber.callNumber("02836228115", true)
                          .then(res => console.log('Launched dialer!', res))
                          .catch(err => console.log('Error launching dialer', err));
                        break;
                    }
                  }
                },
                {
                  text: 'Cài Đặt',
                  handler: () => {
                    this.openNativeSettings.open('location')
                  }
                }
              ]
            });
            alert.present();
          }

          else {
            setTimeout(function () {

              switch (number) {
                case '113':
                  self.callNumber.callNumber("02836228113", true)
                    .then(res => console.log('Launched dialer!', res))
                    .catch(err => console.log('Error launching dialer', err));
                  break;
                case '114':
                  self.callNumber.callNumber("02836228114", true)
                    .then(res => console.log('Launched dialer!', res))
                    .catch(err => console.log('Error launching dialer', err));
                  break;
                case '115':
                  self.callNumber.callNumber("02836228115", true)
                    .then(res => console.log('Launched dialer!', res))
                    .catch(err => console.log('Error launching dialer', err));
                  break;
              }

            },
              1000);

            this.geolocation.getCurrentPosition({
              timeout: 5000,
              enableHighAccuracy: true,
              maximumAge: Infinity
            }).then((resp) => {
              this.sendLocation(resp.coords.latitude, resp.coords.longitude, number);
              this.presentToastWithCall("Số DT: " + this.verifedNumber + " Vị trí: " + "lat: " + resp.coords.latitude + " long: " + resp.coords.longitude)
            }).catch((error) => {
              console.log('Error getting location', error);
            });
          }
        }).catch(e => console.error(e));
    }
    else {
      this.presentToast("Bạn cần nhập và xác thực số điện thoại của mình trước khi có thể sử dụng ứng dụng.")
    }
  }
  goRecord(num) {
    console.log(num)
    if (this.phoneNumberVerified && this.verifedNumber != null) {
      this.navCtrl.push(MediaPage, { number: num, verifiredPhoneNumber: this.verifedNumber })
    }
    else
      this.presentToast("Bạn cần nhập và xác thực số điện thoại của mình trước khi có thể sử dụng ứng dụng.")
  }
}
