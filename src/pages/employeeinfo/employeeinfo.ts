import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the EmployeeinfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-employeeinfo',
  templateUrl: 'employeeinfo.html',
})
export class EmployeeinfoPage {
  url
  display=false;
  display1=false;
  display2=false;
  constructor(private sanitizer: DomSanitizer,public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillEnter(){
    // this.url=this.getSafeUrl(this.navParams.get('url'));
    this.url=this.navParams.get('url')    
    console.log(this.url)
    if(this.url=='https://dichvucong.hochiminhcity.gov.vn/icloudgate/version4/ps/page/bs/home.cpx')
    this.display1=true;
    else
    this.display2=true;
  }
 ionViewDidLeave(){
  this.display1=false;

  this.display2=false;
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EmployeeinfoPage');
  }
  getSafeUrl(link) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(link);
}
}
