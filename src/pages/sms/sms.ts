import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { SMS } from '@ionic-native/sms';
import { JwtHelper } from "jwt-simple";
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

declare var require: any;
/**
 * Generated class for the SmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-sms',
  templateUrl: 'sms.html',
})
export class SmsPage {
  connection: Observable<any>;
  dialNumber
  smsContent = ""
  currentLat
  currentLon
  loading
  constructor(public loadingCtrl: LoadingController, public http: Http, private golbal: GlobalHeroProvider, private geolocation: Geolocation, private toastCtrl: ToastController, private sms: SMS, public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillEnter() {
    this.dialNumber = this.navParams.get('dialNumber');
    // switch (this.dialNumber) {
    //   case '113':
    //     this.dialNumber = "02836228113"
    //     break;
    //   case '114':
    //     this.dialNumber = "02836228114"
    //     break;
    //   case '115':
    //     this.dialNumber = "02836228115"
    //     break;
    // }
    console.log(this.dialNumber)

    this.geolocation.getCurrentPosition({
      timeout: 5000,
      enableHighAccuracy: true,
      maximumAge: Infinity
    }).then((resp) => {
      this.currentLat = resp.coords.latitude
      this.currentLon = resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SmsPage');
  }
  closeSMS() {
    this.navCtrl.pop();
  }
  JWTEncode(text) {
    var jwt = require('jwt-simple');
    var payload;
    if (text != "") {
      payload = {
        "msisdn": this.golbal.number,
        "dial_number": this.dialNumber,
        "text": text
      };
      var secret = 'NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ';
      // encode
      var token = jwt.encode(payload, secret);
      console.log(token)
      return token;
    }
    else {
      var jwt = require('jwt-simple');
      payload = {
        "msisdn": this.golbal.number,
        "dial_number": this.dialNumber,
        "lat": this.currentLat + "",
        "lng": this.currentLon + ""
      };
      var secret = 'NcRuoO6DjZDfzyxGZkVHOdYaMxxFlJ';
      // encode
      var token = jwt.encode(payload, secret);
      console.log(token)
      return token;
    }

  }
  sendLocation() {
    let url = "http://crm.eoc.vnptmedia.vn/api/app11x/update-phonebook";
    var headers = new Headers()
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    let options = new RequestOptions({ headers: headers });
    let body = "data=" + this.JWTEncode("");
    return this.http.post(url, body, options).map(res => res.json()).subscribe(
      data => {
        console.log("location's been sent")
        console.log(data);
      },
      err => {
        console.log("ERROR!: ", err);
      }
    );
  }

  sendSMS() {
    console.log(this.smsContent)
    if (this.smsContent.length > 0) {

      this.loading = this.loadingCtrl.create({
        content: 'Đang tiến hành gửi nội dung, xin vui lòng đừng tắt ứng dụng hay thiết bị.'
      });
      this.loading.present();
      //calling an url to see if the data connection is available or not
      this.connection = this.http.get('https://jsonplaceholder.typicode.com/todos/1');
      this.connection
        .subscribe(data => {
          console.log(data);
          //if data connection is available, then send messages using OTT
          var options = {
            replaceLineBreaks: false,
            android: {
              intent: ''
            }
          };
          this.sms.send('8083', this.dialNumber + " " + this.smsContent, options)
            .then(res => {
              this.sendLocation()
              this.loading.dismiss()
              console.log('SMS sent!', res)
              this.presentToast('Xin cảm ơn tin nhắn của bạn. Chúng tôi sẽ phản hồi trong thời gian sớm nhất.')
              this.navCtrl.pop()
            })
            .catch(err => {
              this.loading.dismiss()
              console.log('Error launching SMS', err)
              this.presentToast('Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại.')
              this.navCtrl.pop()
            });

        }, error => {
          console.log('no connection')
          //if data connection is not available, then send messages using regular SMS
          var options = {
            replaceLineBreaks: false, // true to replace \n by a new line, false by default
            android: {
              // intent: 'INTENT'  // send SMS with the native android SMS messaging
              intent: '' // send SMS without open any other app
            }
          };
          this.sms.send('8083', this.dialNumber + " " + this.smsContent, options)
            .then(res => {
              this.sendLocation()
              this.loading.dismiss()
              console.log('SMS sent!', res)
              this.presentToast('Xin cảm ơn tin nhắn của bạn. Chúng tôi sẽ phản hồi trong thời gian sớm nhất.')
              this.navCtrl.pop()
            })
            .catch(err => {
              this.loading.dismiss()
              console.log('Error launching SMS', err)
              this.presentToast('Có lỗi xảy ra trong quá trình gửi. Xin vui lòng thử lại.')
              this.navCtrl.pop()
            });
        })
    }
    else {
      this.presentToast('Nội dung không thể bỏ trống.')
    }
  }
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
