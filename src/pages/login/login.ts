import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, ModalController, NavParams, LoadingController, MenuController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MediaPage } from '../media/media'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { ChatRoomPage } from '../../pages/chat-room/chat-room';
import { Observable } from 'rxjs/Observable';
import { Map15Page } from '../map15/map15';
import { EmployeeinfoPage } from '../employeeinfo/employeeinfo';
import CryptoJS from 'crypto-js';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username = ""
  loading
  password = ""
  usernameChecked = true
  passwordForCompare = ""
  constructor(public loadingCtrl: LoadingController, public menu: MenuController, public navParams: NavParams, public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private geolocation: Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  openMenu() {
    this.menu.open()
  }
  loginSuccess(userdata) {
    this.storage.set("autoLogin", true);

    this.global.currentUserInfo = {
      nguoidung_id: userdata.accountId,
      donvi_id: userdata.infoDepartment[0].id,
      ten_donvi: userdata.infoDepartment[0].name,
      username: userdata.userName,
      ho_ten: userdata.fullName,
      token: userdata.token,
      didong: userdata.personalInformation.phoneNumber,
      email: userdata.personalInformation.email,
      accountID: userdata.accountId,
      area: userdata.organization.area
    }   

    this.getPermission();
    this.storage.set('EmployeeInfo', JSON.stringify(this.global.currentUserInfo))
    this.menu.enable(false, "menuBeforeLogin");
    this.menu.enable(true, "menuAfterLogin");
    this.global.islogin = true;
    console.log("Current User Info")
    console.log(this.global.currentUserInfo)
  }
  passwordEncrypt(pass) {
    let key = CryptoJS.enc.Utf8.parse('8080808080808080');
    let iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    let encryptedpass = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(pass), key,
      {
        keySize: 128 / 8,
        iv: iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
      console.log('password đã mã hoá: ' + encryptedpass)
    return encryptedpass.toString();
  }
  logout() {
    debugger
    this.menu.enable(true, "menuBeforeLogin");
    this.menu.enable(false, "menuAfterLogin");
    this.global.islogin = false;
    this.storage.set("autoLogin", false);
    return new Promise((resolve, reject) => {
      console.log(this.username + '/' + this.password)
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token",this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_LogOut;
      this.http
        .post(
          apiURL,
          {
            source:'mobile'
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            console.log('logout ok')          
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }
 
  checkPassword() {
    //login mới chỉ sử dụng 1 hàm duy nhất
    if (this.password == "" || this.username == "")
      this.presentToast('Xin vui lòng cung cấp đầy đủ tên đăng nhập và mật khẩu của bạn.')
    else {
  
      this.global.presentLoadingCustom('')
      return new Promise((resolve, reject) => {
        console.log(this.username + '/' + this.password)
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
        let apiURL = this.global.apiURL.newLogin;
        this.http
          .post(
            apiURL,
            {
              Username: this.username,
              Password: this.passwordEncrypt(this.password),
              Source: 'mobile',
              DeviceId: this.global.deviceID,
              OperatingSystem:this.global.device_type
            },
            { headers: headers }
          )
          .subscribe(
            res => {
              console.log(res.json());
              this.global.cancleCustomLoading();
              this.loginSuccess(res.json());
              this.presentToast('Đăng nhập thành công')
              resolve(res.json());
            },
            err => {
              console.log(err);
              this.global.cancleCustomLoading();
              this.presentToast('Đã xảy ra lỗi kết nối hoặc thông tin bạn cung cấp không chính xác, xin vui lòng thử lại.');
              reject(err);
            }
          );
      });
    }
  }
  
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  getPermission()
  {
    this.global.cancleCustomLoading()
    return new Promise((resolve, reject) => {
      console.log(this.username + '/' + this.password)
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = this.global.apiURL.getPermissions + this.global.currentUserInfo.token + '&accountId=' +this.global.currentUserInfo.nguoidung_id;
      this.http
        .get(
          apiURL,       
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            this.global.cancleCustomLoading();
            this.global.currentUserPermission=res.json().menuitems;
            this.storage.set('Permissions', JSON.stringify(this.global.currentUserPermission))
            console.log('lấy permission thành công')
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading();
            this.presentToast('Đã xảy ra lỗi kết nối, xin vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }

}
