import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  MenuController,
  LoadingController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { SmsPage } from "../../pages/sms/sms";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { MapReviewPage } from "../map-review/map-review";
import { ImgviewerPage } from '../imgviewer/imgviewer';

/**
 * Generated class for the CaseDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dashboard-detail',
  templateUrl: 'dashboard-detail.html',
})
export class DashboardDetailPage {
  case = null;
  imglist = [];
  videoList = [];
  imgSliders = null;
  imgInsideViewer;
  dialogForImgPicking = false;

  constructor(
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation
  ) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CaseDetailPage");
  }
  
  ionViewDidEnter() {
    this.imgSliders = document.getElementById(
      "imageViewer"
    ) as HTMLElement;
  }

  closeImgZoom() {
    this.imgSliders.close();
    this.menu.swipeEnable(true);
  }

  goMap() {
    this.navCtrl.push(MapReviewPage, {
      lat: this.case.dia_diem_toado_x,
      lon: this.case.dia_diem_toado_y,
      address:
        this.case.dia_diem_diachi +
        ", " +
        this.case.dia_diem_phuong +
        ", " +
        this.case.dia_diem_quan
    });
  }

  openImgZoom(index) {
    this.navCtrl.push(ImgviewerPage, { imgs: this.imglist })
  }
  ionViewWillEnter() {
    this.case = this.navParams.get("case");
    console.log(this.case);

    let names = this.case.ten_file.split(";");
    for (let i = 0; i < names.length; ++i) {
      if (i != names.length - 1)
        this.imglist.push(this.global.baseURL + this.case.file_url + names[i]);
    }

    if (this.case.video_url != null && this.case.video_url != "") {
      let links = this.case.video_url;
      for (let i = 0; i < links.length; ++i) {
        this.videoList.push(this.case.video_url[i]);
      }
    }
  }
  ionViewWillLeave() {
    this.imglist = [];
  }

  openImgProcessZoom(index) {

    let imgSendToViewer = []
    for (let pic of this.case.hinhanh_xuly) {
      imgSendToViewer.push(pic.src)
    }
    this.navCtrl.push(ImgviewerPage, { imgs: imgSendToViewer })
  }

  backBtn()
  {
    this.navCtrl.pop()
  }
}

