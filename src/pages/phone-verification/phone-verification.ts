import { Component } from '@angular/core';
import { NavController, ToastController, AlertController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MediaPage } from '../media/media'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { HomePage } from '../home/home';
import { SecondhomePage } from '../secondhome/secondhome';

/**
 * Generated class for the PhoneVerificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-phone-verification',
  templateUrl: 'phone-verification.html',
})
export class PhoneVerificationPage {
  phoneNumber: any;
  username = ""
  password = ""
  currentOTPCode = 0
  phoneNumberVerified: any;
  verifedNumber = null;
  tempNumber = "";
  firstTimeRunning = null

  PERMISSION = {
    WRITE_EXTERNAL: this.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
    CAMERA: this.diagnostic.permission.CAMERA,
    CALL_PHONE: this.diagnostic.permission.CALL_PHONE,
    ACCESS_FINE_LOCATION: this.diagnostic.permission.ACCESS_FINE_LOCATION,
    ACCESS_COARSE_LOCATION: this.diagnostic.permission.ACCESS_COARSE_LOCATION,
    SEND_SMS:this.diagnostic.permission.SEND_SMS
  };
  constructor(public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private callNumber: CallNumber, private geolocation: Geolocation) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PhoneVerificationPage');
    if (this.platform.is('android')) {
      this.requestAllPermissions();
    }
  }
  ionViewWillEnter() {
    this.phoneNumberVerified = false;
    this.storage.get('verifiredPhoneNumber').then((val) => {
      if (val != null) {
        console.log(val)
        this.phoneNumberVerified = true;
        this.verifedNumber = val;
        console.log('verified')
      }
    });
  }
  requestAllPermissions() {
    const permissions = Object.keys(this.PERMISSION).map(k => this.PERMISSION[k]);
    this.diagnostic.requestRuntimePermissions(permissions).then((status) => {
      console.log(JSON.stringify(status));
    }, error => {
      console.log('Error: ' + error);
    });
  }
  savePhoneNumber(num) {
    this.global.number=num
    this.verifedNumber = num;
    this.storage.set('verifiredPhoneNumber', num)
  }
  checkBlackList() {
    return true;
  }
  informOTP() {
    if (this.checkBlackList()) {
      if (this.phoneValidator(this.phoneNumber)) {
        this.OTPcall()
        let alert = this.alertCtrl.create({
          enableBackdropDismiss: false,
          title: 'Mã OTP',
          cssClass: 'custom-alert',
          message: 'Một mã OTP sẽ được gửi đến số điện thoại của bạn. Xin nhập mã được cung cấp (mã sẽ hết hiệu lực sau 5 phút) vào khung bên dưới và nhấn \'Xác Thực\' để hoàn tất:',
          inputs: [
            {
              name: 'OTP_Code',
              placeholder: 'Mã OTP'
            }
          ],
          buttons: [
            {
              text: 'Hủy',
              role: 'cancel',
              handler: () => {
                console.log('Cancel clicked');
              }
            },
            {
              text: 'Gửi Lại Mã',
              handler: () => {
                this.currentOTPCode = 0;
                this.informOTP();
              }
            },
            {
              text: 'Xác Thực',
              handler: data => {
                this.OTPVerification(data.OTP_Code);
              }
            }
          ]
        });
        alert.present();
      }

      else {
        this.presentToast("Định dạng số điện thoại không đúng. Xin vui lòng kiểm tra lại")
      }

    }
  }

  phoneValidator(phone) {
    var filter = /^[0-9-+]+$/;
    if (filter.test(phone)) {
      if (phone.length == 10 || phone.length == 11) {
        if (phone.length == 10) {
          if (phone.substring(0, 2) == "09" || phone.substring(0, 2) == "08") {
            return true;
          } else {
            return false;
          }
        } else if (phone.substring(0, 2) == "01") {
          return true;
        } else {
          return false;
        }
      }
      else {
        return false;
      }
    }
  }

  OTPcheckk() {
    return new Promise((resolve, reject) => { })
  }
  generateOTPCode() {
    let val = Math.floor(1000 + Math.random() * 9000);
    if (this.currentOTPCode != 0) {
      if (val != this.currentOTPCode) {
        this.currentOTPCode = val;
        // this.presentToast(this.currentOTPCode)
        setTimeout(function () { this.currentOTPCode = 0; }, 500000);
      }
      else
        this.generateOTPCode();
    }
    else {
      this.currentOTPCode = val;
      // this.presentToast(this.currentOTPCode)
      setTimeout(function () { this.currentOTPCode = 0; }, 500000);
    }
    console.log(this.currentOTPCode)
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  OTPcall() {
    console.log(this.phoneNumber)
    return new Promise((resolve, reject) => {
      if (this.currentOTPCode == 0) {
        this.generateOTPCode();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
        let apiURL = `http://123.20.207.45/safecity_dev/api/QLSuCo/func_GuiSMS`;
        this.http.post(apiURL, { username: this.username, password: this.password, sdt: this.phoneNumber, noidung: 'Mã OTP cho ứng dụng HCMC EOC: ' +  this.currentOTPCode }, { headers: headers })

          .subscribe(res => {
            console.log(res.json());
            var start = res.json().msg.indexOf('[');
            var end = res.json().msg.indexOf(']');
            var temp = res.json().msg.substring(start + 1, end)
            console.log(temp)
            this.tempNumber = temp;
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
      }
    });
  }

  OTPVerification(OTPInput) {
    debugger
    if (OTPInput == this.currentOTPCode) {
      this.phoneNumberVerified = true;
      if (this.tempNumber != "")
        this.savePhoneNumber(this.tempNumber)
      let alert = this.alertCtrl.create({
        enableBackdropDismiss:false,
        title: 'Mã OTP hợp lệ',
        message: 'Số điện thoại của bạn đã được xác thực. Bạn đã có thể bắt đầu sử dụng ứng dụng. Xin cảm ơn.',
        buttons: [
          {
            text: 'Đồng ý',
            role: 'cancel',
            handler: () => {
              this.navCtrl.setRoot(SecondhomePage)
            }
          }
        ]
      });
      alert.present();
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Mã OTP không hợp lệ',
        message: 'Mã số bạn nhập không hợp lệ. Xin vui lòng thử lại',
        buttons: [
          {
            text: 'Đồng ý',
            role: 'cancel',
            handler: () => {
              this.informOTP();
            }
          },
          {
            text: 'Bỏ qua',
            role: 'cancel',
            handler: () => {
            }
          }
        ]
      });
      alert.present();
    }
  }

}
