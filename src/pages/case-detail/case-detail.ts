import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  MenuController,
  LoadingController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { SmsPage } from "../../pages/sms/sms";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { MapReviewPage } from "../map-review/map-review";
import { ImgviewerPage } from '../imgviewer/imgviewer';

/**
 * Generated class for the CaseDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-case-detail",
  templateUrl: "case-detail.html"
})
export class CaseDetailPage {
  case = null;
  imglist = [];
  videoList = [];
  imgSliders = null;
  imgInsideViewer;
  dialogForImgPicking = false;
  mark = 5; rate = 5;
  feedback = "";
  txtcounter = 0
  constructor(
    public menuCtrl:MenuController,
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation
  ) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CaseDetailPage");
  }
  ionViewDidEnter() {
    this.imgSliders = document.getElementById(
      "imageViewer"
    ) as HTMLMapElement;
  }
  closeImgZoom() {
    this.imgSliders.close();
    this.menu.swipeEnable(true);
  }
  goMap() {
    let parts = this.case.location.split(',');
    this.navCtrl.push(MapReviewPage, {
      lat: parts[1],
      lon: parts[0],
      address: this.case.areadetail
    });
  }
  openImgZoom(index) {
    this.navCtrl.push(ImgviewerPage, { imgs: this.imglist })
  }
  ionViewWillEnter() {
    this.case = this.navParams.get("case");
    console.log(this.case);
    for (let pic of this.case.attachment) {
      this.imglist.push(pic.filePath)
    }
    if (this.case.video_url != null && this.case.video_url != "") {
      // this.videoList.push(this.case.video_url);
      let links = this.case.video_url;
      for (let i = 0; i < links.length; ++i) {
        this.videoList.push(this.case.video_url[i]);
      }
    }
    if (this.case.rating != null && this.case.status == '14') {
      this.rate = parseInt(this.case.rating);
    }
  }
  ionViewWillLeave() {
    this.imglist = [];
  }

  openImgProcessZoom(index) {
    let imgSendToViewer = []
    for (let pic of this.case.resolveAttachment) {
      imgSendToViewer.push(pic.filePath)
    }
    this.navCtrl.push(ImgviewerPage, { imgs: imgSendToViewer })
  }
  onModelChange(e) {
    this.mark = e;
  }
  sendFeedback() {
    debugger
    console.log(this.mark)
    console.log(this.feedback)
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = this.global.apiURL.func_Gui_DanhGia_Event;
      this.http
        .post(
          apiURL,
          {
            Id: this.case.id,
            Rating: this.mark,
            RatingComment: this.feedback
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            console.log('send feedback ok');
            this.presentToast('Gửi thành công. Xin cảm ơn phản hồi của bạn.')
            this.navCtrl.pop();
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối. Xin vui lòng thử lại.')
            reject(err);
          }
        );
    });
  }
  maxLengthCheck(e) {
    console.log(e)
    this.txtcounter = e.length
  }
  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }
  logoutDueToTokenExpired() {
    alert("Phiên đăng nhập của bạn đã kết thúc, xin vui lòng đăng nhập lại.")
    this.global.islogin = false
    this.storage.set('autoLogin', false);
    this.menuCtrl.enable(true, "menuBeforeLogin");
    this.menuCtrl.enable(false, "menuAfterLogin");
    this.navCtrl.popToRoot();
  }
}
