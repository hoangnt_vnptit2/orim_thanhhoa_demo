import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, Platform, NavParams, ActionSheetController, LoadingController, Loading } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from "@angular/http";
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { normalizeURL } from 'ionic-angular'; 

//from old media
import { Camera, CameraOptions } from "@ionic-native/camera";
import {
  MediaCapture,
  MediaFile,
  CaptureError,
  CaptureImageOptions,
  CaptureVideoOptions
} from "@ionic-native/media-capture";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import "rxjs/add/operator/map";
import { Base64 } from "@ionic-native/base64";
import { File, FileEntry } from "@ionic-native/file";
/**
 * Generated class for the SetAvatarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//Slide Control
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

declare var cordova: any;

@Component({
  selector: 'page-imgpicker',
  templateUrl: 'imgpicker.html',
})
export class ImgpickerPage {
  loading: Loading;
  tempImg=[]
  processedImgURL= []
  @ViewChild(Slides) slides: Slides;

  constructor(public global: GlobalHeroProvider, public http: Http, public navCtrl: NavController, public navParams: NavParams, private camera: Camera,
    private file: File,
    private base64: Base64,
    private imagePicker: ImagePicker,
    public sanitizer: DomSanitizer,
    private mediaCapture: MediaCapture,
    public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController,
    public platform: Platform, public loadingCtrl: LoadingController, public storage: Storage) {
  }

  ionViewWillEnter() {
    console.log(this.global.imageURL)
    this.tempImg=this.global.imageURL;
    for(let pic of this.tempImg)
    {
      this.processImgAfterTaking(pic)
    }
  }

  ionViewWillLeave(){
   this.global.imageURL=this.tempImg
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetAvatarPage');
  }

  ionViewDidLeave(){
  }
 
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Xin Chọn Nguồn Ảnh',
      buttons: [
        {
          text: 'Từ Thư Viện',
          handler: () => {
            this.localImagePicker();
          }
        },
        {
          text: 'Từ Camera',
          handler: () => {
            this.takePhoto();
          }
        },
        {
          text: 'Hủy',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  /////////////////////////////Special Photos Treatment for iOS
  processImgAfterTaking(imagePath) {
    var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.processedImgURL.push(this.pathForImage(newFileName))
    }, error => {
      console.log('Có lỗi khi lưu ảnh');
    });
  }
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let result = normalizeURL(cordova.file.dataDirectory + img);
      console.log(result)
      return result
    }
  }
  /////////////////////////////////

  takePhoto() {
    if (this.tempImg.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    } else {
      let options: CameraOptions = {
        quality: 10,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };
      this.camera.getPicture(options).then(
        imageData => {
          console.log('file uri: ' + imageData)
          this.tempImg.push(imageData);
          this.processImgAfterTaking(imageData);
        },
        err => {
          console.log(err);
        }
      );
    }
  }

  localImagePicker() {
    if (this.tempImg.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
      // alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    } else {
      let count = 5 - this.tempImg.length;
      let options: ImagePickerOptions = {
        quality: 10,
        maximumImagesCount: count
      };
      this.imagePicker.getPictures(options).then(
        results => {
          for (let pic of results) {
            this.tempImg.push(pic); 
            this.processImgAfterTaking(pic);
          }
        },
        err => { }
      );
      // let options: CameraOptions = {
      //   quality: 10,
      //   destinationType: this.camera.DestinationType.FILE_URI,
      //   encodingType: this.camera.EncodingType.JPEG,
      //   mediaType: this.camera.MediaType.PICTURE,
      //   sourceType:0
      // };
      // this.camera.getPicture(options).then(
      //   imageData => {
      //     this.global.convertNativeURI(imageData)
      //     .then(filePath => {
      //       console.log('link đã convert: ' + filePath);
      //       this.tempImg.push(filePath); 
      //       this.processImgAfterTaking(filePath);
      //     })
      //     .catch(err => console.log(err)); 
      //   },
      //   err => {
      //     console.log(err);
      //   }
      // );
    }
  }

  thumbnailClick(index) {
    console.log(index)
    this.slides.slideTo(index, 500);
  }

  confirmImg() {
    this.global.imageURL=this.tempImg;
    this.navCtrl.pop();    
  }

  delPic(index)
  {
    this.tempImg.splice(index,1);
    this.processedImgURL.splice(index,1);
    if(this.tempImg.length>0)
    this.slides.slideTo(0, 500);
  }


}
