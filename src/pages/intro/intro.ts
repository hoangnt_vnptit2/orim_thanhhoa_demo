import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ThirdHomePage } from '../third-home/third-home';
import { Storage } from '@ionic/storage';
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
  agreement = false
  @ViewChild(Slides) slides: Slides;
  introContent: any;

  PERMISSION = {
    WRITE_EXTERNAL: this.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
    CAMERA: this.diagnostic.permission.CAMERA,
    // CALL_PHONE: this.diagnostic.permission.CALL_PHONE,
    ACCESS_FINE_LOCATION: this.diagnostic.permission.ACCESS_FINE_LOCATION,
    ACCESS_COARSE_LOCATION: this.diagnostic.permission.ACCESS_COARSE_LOCATION,
    // SEND_SMS:this.diagnostic.permission.SEND_SMS
  };
  constructor(private global:GlobalHeroProvider,private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic,public storage: Storage, public navCtrl: NavController, public navParams: NavParams) {
  }
  requestAllPermissions() {
    const permissions = Object.keys(this.PERMISSION).map(k => this.PERMISSION[k]);
    this.diagnostic.requestRuntimePermissions(permissions).then((status) => {
      console.log(JSON.stringify(status));
    }, error => {
      console.log('Error: ' + error);
    }); 
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
    this.requestAllPermissions();
  }
  ionViewWillEnter() {
    this.storage.get('agreement').then((val) => {
      if (val == null) {
        this.storage.set("agreement", false)
      }
      else
        this.agreement = true
    });
  }
  checkAgrrement() {
    console.log(this.agreement)
  }
  slideChanged() {
    // let currentIndex = this.slides.getActiveIndex();
    // var paragraph=document.getElementById("textIntro") as HTMLElement
    // if(currentIndex==0)
    // // this.introContent=this.default
    // paragraph.innerHTML=this.default
    // else if(currentIndex==1)
    // paragraph.innerHTML="Để xem menu các trương mục chi tiết. Bạn hãy ấn vào biểu tượng " +"<img style=\"vertical-align:middle; height:25px; width:25px \" src=\"https://s33.postimg.cc/elfk9cbjj/menu_Btn.png\"/>" 
    // + " nằm ở góc trên cùng bên trái. Menu cũng là nơi giúp bạn điều hướng sang các trương mục khác nhau của sổ tay."
    // else if(currentIndex==2)
    // paragraph.innerHTML="Một số trương mục sẽ có thể kèm theo menu dạng thẻ dưới cùng, chứa các mô tả rõ ràng hơn về trương mục đó " + "<br><img style=\"vertical-align:middle; height:auto; width:300px; margin-top: 5px; \" src=\"https://s33.postimg.cc/525vfj0sf/tab_Menu.png\"/>" 
    // else
    // paragraph.innerHTML="Khi cần gửi phản hồi hay góp ý cho chúng tôi, xin hãy sử dụng trương mục 'Gửi phản hồi'"
    // console.log('Current index is', currentIndex);
  }
  goHome() {
    if (!this.agreement) {
      this.slides.slideTo(4)
    }
    else {
      this.navCtrl.setRoot(ThirdHomePage)
      this.storage.set('agreement', true)
    }
  }
}
