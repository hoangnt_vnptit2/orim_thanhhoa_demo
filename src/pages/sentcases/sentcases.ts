import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  MenuController,
  LoadingController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { CaseDetailPage } from '../case-detail/case-detail';

/**
 * Generated class for the SentcasesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-sentcases",
  templateUrl: "sentcases.html"
})
export class SentcasesPage {
  originalList
  listOfSentCases = []
  calendarDialog;
  hasCalendar = false;
  prevPickdate = "";
  pickdate = "";
  loading;
  casesList = [];
  showCalendar = false
  constructor(
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation
  ) { }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    // this.ionViewDidEnter()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewDidLoad() {
    console.log("ionViewDidLoad SentcasesPage");
    this.calendarDialog = document.getElementById(
      "calendarDialog"
    ) as HTMLElement;
  }

  ionViewWillEnter() {
    this.showCalendar = false
    this.pickdate = new Date(Date.now()).toLocaleDateString("en-GB");
    this.checkIfAnyCaseAvailable();
  }

  checkIfAnyCaseAvailable() {
    this.global.presentLoadingCustom('')
    this.storage.get("idEvents").then(val => {
      this.global.cancleCustomLoading()
      console.log(val);
      let cases = ''
      if (val == null || JSON.parse(val).length == 0) {
        this.presentToast("Hiện chưa báo sự việc nào nào");
      } else {
        console.log('danhs sách sự việc đã saved')
        console.log(JSON.parse(val))
        for (let event of JSON.parse(val)) {
          if (cases == '')
            cases = "'" + event + "'"
          else
            cases = cases + ',' + "'" + event + "'"
        }
        console.log(cases)
        this.getSentCases(null, cases);
      }
    });

    // this.getSentCases(null,'')
  }

  getSentCases(date, events) {
    this.global.presentLoadingCustom('')
    if (date == null) {
      return new Promise((resolve, reject) => {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        let apiURL = this.global.apiURL.func_DsSuCo_DaGui;
        this.http.post(apiURL, { listid: events }, { headers: headers }).subscribe(
          res => {
            console.log(res.json());
            this.global.cancleCustomLoading()
            if (res.json().code == 1) {
              //get list of cases ok
              this.listOfSentCases = res.json().data;
              console.log(this.listOfSentCases[0])
              console.log('get list unsent cases OK')
              console.log(this.listOfSentCases)
              this.originalList = this.listOfSentCases;
            } else {
              this.global.cancleCustomLoading()
              this.presentToast(
                "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
              );
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading()
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
            reject(err);
          }
        );
      });
    } else {
    }
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }

  onDaySelect(event) {
    console.log(event);
    let dateForCheck = event.date + "/" + (event.month + 1) + "/" + event.year;
    console.log("ngày đã chọn từ lịch: " + dateForCheck)
    if (this.global.compare2Dates(dateForCheck)) {
      this.pickdate = dateForCheck;
    } else
      this.presentToast(
        "Xin chọn ngày tìm kiếm nhỏ hơn hoặc bằng ngày hiện tại."
      );
  }
  searchByDate() {
    if (this.prevPickdate == "" && this.pickdate != "") {
      console.log("search lần đầu");
      this.prevPickdate = this.pickdate;
      this.listOfSentCases = this.global.searchDate(this.pickdate, this.originalList);

    } else if (this.prevPickdate != this.pickdate) {
      console.log("search các lần tiếp theo");
      this.prevPickdate = this.pickdate;
      this.listOfSentCases = this.global.searchDate(this.pickdate, this.originalList);
    } else {
      console.log("trùng ngày hoặc ngày ko hợp lệ ko search");
    }
  }
  closeCalendar() {
    // this.calendarDialog.close();
    this.showCalendar = false;
    this.searchByDate();
  }
  openCalendar() {
    // this.calendarDialog.showModal();
    this.showCalendar = true;
  }
  openMenu() {
    this.menu.open();
  }
  showHideCalendarButton() {
    if (this.hasCalendar) this.hasCalendar = false;
    else this.hasCalendar = true;
  }
  goDetail(casedetail) {
    console.log(casedetail)
    this.navCtrl.push(CaseDetailPage, { case: casedetail })
  }
}
