import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Http, Headers, RequestOptions } from "@angular/http";
import { SpeechRecognition } from '@ionic-native/speech-recognition';

/**
 * Generated class for the Map15Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

@Component({
  selector: 'page-map15',
  templateUrl: 'map15.html',
})
export class Map15Page {
  @ViewChild('map') mapElement: ElementRef;
  voiceSearchModal = false;
  hasGPS
  geoCoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  map: any;
  markers = []
  currentLat
  currentLng
  mapOptions = {
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    // center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    styles: [
      {
        featureType: 'administrative',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit',
        // elementType: 'labels.icon',
        stylers: [{ visibility: 'off' }]
      },

    ]
  }

  constructor(private speechRecognition: SpeechRecognition, private http: Http, private nativeGeocoder: NativeGeocoder, public global: GlobalHeroProvider, public geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Map15Page');
  }

  ionViewWillEnter() {
    this.getPermission();

    this.currentLat = this.navParams.get('currentlat');
    this.currentLng = this.navParams.get('currentlng');
    this.hasGPS = this.navParams.get('hasGPS')
    if (this.hasGPS) {
      if (this.global.currentCaseAddress != "") {
        this.placeMarker(new google.maps.LatLng(this.global.currentLat, this.global.currentLng), this.global.currentCaseAddress)
      }
      else {
        this.findAddressBasedOnLatLn(this.currentLat, this.currentLng, true, '')
        this.global.currentLat = this.currentLat;
        this.global.currentLng = this.currentLng;
      }
    }
    else {
      if (this.global.currentCaseAddress != "") {
        this.placeMarker(new google.maps.LatLng(this.global.currentLat, this.global.currentLng), this.global.currentCaseAddress)
      }
    }
    console.log(this.currentLat)
    console.log(this.currentLng)
    this.loadMap()
  }

  findLatLngBasedOnAddress(address) {
    this.nativeGeocoder.forwardGeocode(address, this.geoCoderOptions)
      .then((coordinates: NativeGeocoderForwardResult[]) => {
        console.log('The coordinates are latitude=' + coordinates[0].latitude + ' and longitude=' + coordinates[0].longitude)
        // this.findAddressBasedOnLatLn(coordinates[0].latitude,coordinates[0].longitude)
        this.clearOverlays()
        this.placeMarker(new google.maps.LatLng(coordinates[0].latitude, coordinates[0].longitude), address);
      }
      )
      .catch((error: any) => console.log(error));
  }
  findAddressBasedOnLatLn(lat, lon, firtTime, address) {

    this.nativeGeocoder.reverseGeocode(parseFloat(lat), parseFloat(lon), this.geoCoderOptions)
      .then((result: NativeGeocoderReverseResult[]) => {
        let res = result[0];
        let fullAdress = res.subThoroughfare + ' ' + res.thoroughfare + ', ' + res.subAdministrativeArea + ', ' + res.administrativeArea + ', ' + res.countryName;
        console.log(fullAdress);
        if (address != "")
          this.global.currentCaseAddress = address
        else
          this.global.currentCaseAddress = fullAdress;

        this.global.currentLat = lat;
        this.global.currentLng = lon;
        var marker = new google.maps.Marker({
          animation: google.maps.Animation.DROP,
          position: new google.maps.LatLng(lat, lon),
          map: this.map
        });
        marker.setMap(this.map);
        this.map.panTo(new google.maps.LatLng(lat, lon));
        this.map.setZoom(15)
        let content = "<h6>Vị trí sự việc: " + this.global.currentCaseAddress + "</h6>";
        this.addInfoWindow(marker, content);
        this.markers.push(marker);
      }
      )
      .catch((error: any) => console.log(error));
  }
  loadMap() {
    let latLng
    if (this.currentLat == 0)
      latLng = new google.maps.LatLng(10.4082476, 106.9461985);
    else
      latLng = new google.maps.LatLng(this.currentLat, this.currentLng);
    //set options for map
    this.mapOptions['center'] = latLng,
      //initialize the map
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);
    var input = document.getElementById('pac-input') as HTMLInputElement;
    var searchBox = new google.maps.places.SearchBox(input);
    let self = this;
    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces();
      console.log(places)
      if (places.length == 0) {
        return;
      }
      else {
        //in case of no GPS
        if (!this.hasGPS)
          self.findLtnLnBasedOnAddressNoGPS(places[0].formatted_address)
        else
          self.findLatLngBasedOnAddress(places[0].formatted_address)
      }
    });

    google.maps.event.addListener(self.map, 'click', function (event) {
      self.clearSearch();
      self.clearOverlays();
      self.placeMarker(event.latLng, '');
    });
    // var officesMarker;
    // officesMarker = new google.maps.Marker({
    //   animation: google.maps.Animation.DROP,
    //   position: new google.maps.LatLng(this.currentLat, this.currentLng),
    //   title: 'Vị trí vi phạm'
    // });
    // this.markers.push(officesMarker)
    // officesMarker.setMap(this.map);
    // let content = "<h6>Vị trí hiện tại</h6>";
    // this.addInfoWindow(officesMarker, content);
  }
  findAddressBasedOnLatLnNoGPS(lat, lon, firtTime, address) {
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lon + '&key=AIzaSyBNsEPj9AAhs5hbXz2_tFMe-4pZt91fOzo'
      this.http
        .get(
          url,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json())
            console.log(res.json().results[0].formatted_address)
            if (address != "")
              this.global.currentCaseAddress = address
            else
              this.global.currentCaseAddress = res.json().results[0].formatted_address;
            this.global.currentLat = lat;
            this.global.currentLng = lon;
            var marker = new google.maps.Marker({
              animation: google.maps.Animation.DROP,
              position: new google.maps.LatLng(lat, lon),
              map: this.map

            });
            marker.setMap(this.map);
            this.map.panTo(new google.maps.LatLng(lat, lon));
            this.map.setZoom(15)
            let content = "<h6>Vị trí sự việc: " + this.global.currentCaseAddress + "</h6>";
            this.addInfoWindow(marker, content);
            this.markers.push(marker);
            resolve(res)
          },
          err => {
            reject(err);
          }
        );
    });
  }
  findLtnLnBasedOnAddressNoGPS(address) {
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      let apiURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address.replace(' ', '+') + '&key=AIzaSyBNsEPj9AAhs5hbXz2_tFMe-4pZt91fOzo';
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json())
            console.log(res.json().results[0].geometry.location.lat)
            console.log(res.json().results[0].geometry.location.lng)
            this.clearOverlays()
            this.placeMarker(new google.maps.LatLng(res.json().results[0].geometry.location.lat, res.json().results[0].geometry.location.lng), address);
            resolve(res)
          },
          err => {
            reject(err);
          }
        );
    });
  }
  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    infoWindow.open(this.map, marker);
  }

  placeMarker(location, adress) {
    console.log(location.lat());
    console.log(location.lng());
    if (this.hasGPS)
      this.findAddressBasedOnLatLn(location.lat(), location.lng(), false, adress);
    else
      this.findAddressBasedOnLatLnNoGPS(location.lat(), location.lng(), false, adress)
  }
  clearOverlays() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }
  backBtn() {
    this.navCtrl.pop()
  }
  myLocation() {
    this.geolocation.getCurrentPosition({
      timeout: 5000,
      enableHighAccuracy: true,
      maximumAge: Infinity
    }).then((resp) => {
      console.log(resp)
      this.clearOverlays();
      this.placeMarker(new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude), '')
    }).catch((error) => {
      alert('Xin vui lòng kiểm tra kết nối GPS');
    });
  }
  clearSearch() {
    var input = document.getElementById('pac-input') as HTMLInputElement;
    input.value = '';
  }

  tryAgain() {
    this.showHideVoiceInputComponents(false)
    this.speechRecording()
  }
  speechRecording() {
    this.showHideVoiceInputComponents(false)
    this.speechRecognition.startListening({
      language: "vi-VN",
      showPopup: false
    }).subscribe(matches => {
      document.getElementById('modalClose2').click();
      let input = document.getElementById('pac-input') as HTMLInputElement;
      input.value =  matches[0];  
      input.focus();   
    },
      onerror => {
        console.log("Lỗi")
        this.showHideVoiceInputComponents(true)
      });
  }
  showHideVoiceInputComponents(hide) {
    if (hide) {
      let a = document.getElementById('voiceInputLabel2') as HTMLElement
      a.hidden = true
      a = document.getElementById('voiceInputImg2') as HTMLElement
      a.hidden = true
      a = document.getElementById('voiceInputErrorBtns2') as HTMLElement
      a.hidden = false
    }
    else {
      let a = document.getElementById('voiceInputLabel2') as HTMLElement
      a.hidden = false
      a = document.getElementById('voiceInputImg2') as HTMLElement
      a.hidden = false
      a = document.getElementById('voiceInputErrorBtns2') as HTMLElement
      a.hidden = true
    }
  }
  stopListening() {
    this.speechRecognition.stopListening().then(() => {
    });
  }
  getPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          this.speechRecognition.requestPermission();
        }
      });
  }
}
