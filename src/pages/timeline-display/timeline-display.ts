import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';

/**
 * Generated class for the TimelineDisplayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-timeline-display',
  templateUrl: 'timeline-display.html',
})
export class TimelineDisplayPage {
  items = []
  progress = []
  constructor(public global:GlobalHeroProvider,public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillEnter() {
    this.progress = this.navParams.get('progress');
    console.log(this.progress)
    for(let step of this.progress)
    {
      this.items.push({
        department: step.assigner.departmentName,
        title: step.assigner.name,
        content: step.comment,
        icon: 'calendar',
        time: { subtitle: this.global.dateDisplayFix(step.createDate).toString().split(' ')[1],
         title: this.global.dateDisplayFix(step.createDate).toString().split(' ')[0]}
      });
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TimelineDisplayPage');
  }

}
