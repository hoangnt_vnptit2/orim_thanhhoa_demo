import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  MenuController,
  LoadingController,
  Form
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { SmsPage } from "../../pages/sms/sms";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { global } from "@angular/core/src/util";
import { CastExpr } from "@angular/compiler";
import { MapReviewPage } from "../map-review/map-review";
import { TimelineDisplayPage } from '../timeline-display/timeline-display';
import { ImgpickerPage } from '../imgpicker/imgpicker';

//from old media
import { Camera, CameraOptions } from "@ionic-native/camera";
import {
  MediaCapture,
  MediaFile,
  CaptureError,
  CaptureImageOptions,
  CaptureVideoOptions
} from "@ionic-native/media-capture";
import { DomSanitizer, SafeUrl } from "@angular/platform-browser";
import { ImagePicker, ImagePickerOptions } from "@ionic-native/image-picker";
import "rxjs/add/operator/map";
import { Base64 } from "@ionic-native/base64";
import { File, FileEntry } from "@ionic-native/file";
import { ImgviewerPage } from "../imgviewer/imgviewer";
/**
 * Generated class for the AssignconfirmPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-assignconfirm",
  templateUrl: "assignconfirm.html"
})
export class AssignconfirmPage {
  masterFormdataToSend = new FormData();
  tokenOut = false;
  showIfNotSupportType = true;
  typeOftheCurrentCase = ''
  assignTarget = "cv"
  imgFormData = new FormData
  approveComment = "";
  processComment = "";
  typeOfProcess = "";
  supportComment = "";
  assignComment = "";
  hasReportPermission = false;
  pickedEmployee = "";
  pickedDepartment = "";
  pickedAssignedDepartment = ""
  case = null;
  loading;
  deparmentList = null;
  listOfPickedAssignedDepartment = [];
  employeeList = null;
  listOfPickedEmployee = [];
  listOfPickedDepartment = [];
  listOfSupportDepartment = null;
  imglist = [];
  imgSliders;
  imgInsideViewer;
  fromPage = "";
  currentCaseProgress = []

  //img Picker
  imagePick;
  listHasElements = false;
  hasVideos = false;
  imageURL = [];
  base64ImgList = [];
  videoFilePath;
  rawVideoPath;
  number;
  toggle;
  dialogForImgPicking = false
  oldProcessEmployee = []
  oldSupportDepartment = []
  savedApproveComment = ""
  constructor(
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation,
    private file: File,
    private base64: Base64,
    private imagePicker: ImagePicker,
    public sanitizer: DomSanitizer,
    private mediaCapture: MediaCapture,
    private camera: Camera,
  ) { }

  ionViewDidEnter() {
    console.log("ionViewDidLoad AssignconfirmPage");
    this.imgSliders = document.getElementById("imageViewer") as HTMLElement;
    this.imagePick = document.getElementById("vcl") as HTMLElement;
    console.log(this.imagePick)
  }
  ionViewWillEnter() {
    this.case = this.navParams.get("case");
    if (this.case.status == 12 && this.case.currentExecDepartment.id != this.global.currentUserInfo.donvi_id) {
      this.showIfNotSupportType = false;
    }
    this.fromPage = this.navParams.get("page");
    if (this.navParams.get('type') != null)
      this.typeOftheCurrentCase = this.navParams.get('type')

    console.log("From page: " + this.fromPage);
    console.log("Loại case: " + this.typeOftheCurrentCase);

    console.log(this.case);
    if (this.fromPage == 'process' || this.fromPage == 'approve') {
      this.informCaseProcessingStartTime();
    }

    if (this.fromPage == 'assign') {
      this.getEmployeeList();
      this.getDeparmentList();
      this.getSupportDepartmant();
    }

    this.setupImgs();
    if (this.global.currentUserInfo.donvi_id != this.case.currentExecDepartment.id)
      this.hasReportPermission = true;
    else this.hasReportPermission = false;

    //for imgs
    this.toggle = "picture";

    if (this.imageURL.length >= 1) this.listHasElements = true;
    else this.listHasElements = false;
    if (this.videoFilePath != null) this.hasVideos = true;
    else this.hasVideos = false;

    if (this.fromPage == 'approve' && this.approveComment == "")
      this.approveComment = this.case.hoan_tat_ghichu
    else
      this.approveComment = this.savedApproveComment

    //log start time for processing
    if (this.fromPage == 'process')
      this.loggingStartProcessingTime()
  }

  ionViewWillLeave() {
    this.savedApproveComment = this.approveComment
  }

  ionViewDidLeave() {
    let check = false;
    for (let view of this.navCtrl.getViews()) {
      if (view.component == ImgpickerPage || view.component == MapReviewPage || view.component == TimelineDisplayPage) {
        //has imgPicker-mappreview-timeline page in the stack
        check = true; break;
      }
    }
    if (!check) { this.global.imageURL = []; }
  }

  loggingStartProcessingTime() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.informCaseProcessingStartTime;
      this.http
        .post(
          apiURL,
          {
            NGUOI_THUC_HIEN_ID: this.global.currentUserInfo.nguoidung_id,
            TINBAO_ID: this.case.id
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  checkID_XULY_OLD() {

    if (this.case.olderAssignees != null && this.case.olderAssignees.length > 0) {
      for (let person of this.employeeList) {
        if (this.case.olderAssignees[0].id == person.id.toString()) {
          this.pickedEmployee = person.fullname
          this.onSelectEmployeeChange(person.fullname)
          this.oldProcessEmployee.push(person.fullname)
          console.log(this.oldProcessEmployee)
        }
      }
    }
  }

  testTimeline() {
    this.navCtrl.push(TimelineDisplayPage, { progress: this.currentCaseProgress })
  }

  informCaseProcessingStartTime() {
    console.log('báo time cho: ' + this.case.id)
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      // headers.append('API_KEY', '6358EBEA574E98FF5405B59EF130AA06');
      let apiURL = this.global.apiURL.func_layQuaTrinhXuLy_theoSuCo;
      this.http
        .post(
          apiURL,
          {
            issueid: this.case.id
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            console.log('get status ok')
            this.currentCaseProgress = res.json().data;
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  checkIfPickOldProcessEmployeeAgain(person) {
    let check = false

    if (this.case.olderAssignees != null && this.case.olderAssignees.length > 0) {
      for (let a of this.oldProcessEmployee) {
        if (a == person)
          check = true
      }
    }
    return check
  }

  checkID_XULY_SUPPORT_OLD() {
    if (this.case.olderSupports != null && this.case.olderSupports.length > 0) {
      for (let x = 0; x < this.case.olderSupports.length; ++x) {
        for (let y = 0; y < this.listOfSupportDepartment.length; ++y) {
          if (this.case.olderSupports[x].id == this.listOfSupportDepartment[y].id.toString()) {
            this.pickedDepartment = this.listOfSupportDepartment[y].name
            this.onSelectDepartmentChange(this.listOfSupportDepartment[y].name)
            this.oldSupportDepartment.push(this.listOfSupportDepartment[y].name)
          }
        }
      }
    }
    console.log(this.oldSupportDepartment)
  }

  checkDepartShowUp(id) {
    if (this.case.olderSupports != null) {
      let check = true;
      if (this.case.olderSupports.length > 0) {
        for (let x = 0; x < this.case.olderSupports.length; ++x) {
          if (this.case.olderSupports[x].id == id) {
            check = false; break;
          }
        }
      }
      else {
        return check;
      }

      return check;
    }
    else return true;
  }

  checkIfPickOldSupportDepartmentAgain(department) {
    let check = false

    if (this.case.olderSupports != null && this.case.olderSupports.length > 0) {
      for (let a of this.oldSupportDepartment) {
        if (a == department)
          check = true
      }
    }
    return check
  }
  onSelectTypeOfProcessChange(e) {
    console.log(this.typeOfProcess)
  }

  //For imgs Picker
  delPic(pic) {
    for (var i = 0; i < this.imageURL.length; ++i) {
      if (pic == this.imageURL[i]) {
        this.imageURL.splice(i, 1);
        this.base64ImgList.splice(i, 1);
        break;
      }
    }
    if (this.imageURL.length >= 1) this.listHasElements = true;
    else this.listHasElements = false;
  }

  imgToBase64(pic) {
    let filePath: string = pic;
    this.base64.encodeFile(filePath).then(
      (base64File: string) => {
        console.log(base64File);
        this.base64ImgList.push(base64File);
      },
      err => {
        console.log(err);
      }
    );
  }
  takePhoto() {
    if (this.imageURL.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    } else {
      let options: CameraOptions = {
        quality: 10,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };
      this.camera.getPicture(options).then(
        imageData => {
          this.imageURL.push(imageData);
          this.imgToBase64(imageData);
          this.listHasElements = true;
        },
        err => {
          console.log(err);
        }
      );
    }
  }
  localImagePicker() {
    if (this.imageURL.length == 5) {
      alert(
        "Bạn chỉ được gửi tối đa 5 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
      // alert("Bạn chỉ được gửi tối đa 5 tấm hình và 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn")
    } else {
      // let count = 5 - this.imageURL.length;
      // let options: ImagePickerOptions = {
      //   quality: 10,
      //   maximumImagesCount: count
      // };
      // this.imagePicker.getPictures(options).then(
      //   results => {
      //     // for (var i = 0; i < results.length; i++) {
      //     //   console.log('Image URI: ' + results[i]);
      //     // }
      //     for (let pic of results) {
      //       this.imageURL.push(pic);
      //       this.imgToBase64(pic);
      //     }
      //     if (this.imageURL.length >= 1) this.listHasElements = true;
      //     else this.listHasElements = false;
      //   },
      //   err => { }
      // );
      let options: CameraOptions = {
        quality: 10,
        destinationType: this.camera.DestinationType.FILE_URI,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        sourceType: 0
      };
      this.camera.getPicture(options).then(
        imageData => {
          console.log(this.imageURL)
          this.global.convertNativeURI(imageData)
            .then(filePath => {
              console.log('link đã convert: ' + filePath);
              this.imageURL.push(filePath);
              if (this.imageURL.length >= 1) this.listHasElements = true;
              else this.listHasElements = false;
            })
            .catch(err => console.log(err));
        },
        err => {
          console.log(err);
        }
      );
    }
  }


  goMap() {
    let parts = this.case.location.split(',')
    this.navCtrl.push(MapReviewPage, {
      lat: parts[1],
      lon: parts[0],
      address:
        this.case.areadetail
    });
  }

  setupImgs() {
    let result = this.global.checkImgSrc(this.case.hinhanh_phananh);
    if (result.length > 0) this.imglist = result;
  }

  getEmployeeList() {
    this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("token", this.global.currentUserInfo.token)
      headers.append("departmentId", this.global.currentUserInfo.donvi_id.toString())
      let apiURL = this.global.apiURL.func_DsNhanVien + this.global.currentUserInfo.donvi_id;
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lay ds nhanvien ok')
            console.log(res.json());
            //get list cases is ok
            this.global.cancleCustomLoading();
            this.employeeList = res.json();
            this.checkID_XULY_OLD()
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading();
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
            reject(err);
          }
        );
    });
  }

  getDeparmentList() {

    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      headers.append("departmentId", this.global.currentUserInfo.donvi_id.toString())

      let apiURL = this.global.apiURL.fucn_laydsDonViPhancong + this.global.currentUserInfo.donvi_id;
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lay ds phong ban ok')
            console.log(res.json());
            //get list cases is ok
            this.deparmentList = res.json();
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast(
              "Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại."
            );
            reject(err);
          }
        );
    });
  }

  getSupportDepartmant() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("departmentId", this.global.currentUserInfo.donvi_id.toString())
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_DsDonVi_CoTheHoTro + this.global.currentUserInfo.donvi_id;
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lay ds hotro ok')
            console.log(res.json());
            this.listOfSupportDepartment = res.json()
            if (res.json().code == 1) {
              this.checkID_XULY_SUPPORT_OLD()
            } else {
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: "bottom"
    });
    toast.present();
  }
  onSelectDepartmentChange(selectedValue: any) {
    console.log("Selected", selectedValue);
    let exist = false;
    for (let department of this.listOfPickedDepartment) {
      if (selectedValue == department) {
        exist = true;
        break;
      }
    }
    if (!exist) {
      this.listOfPickedDepartment.push(selectedValue);
    }
  }
  removePickedDepartment(department) {
    this.listOfPickedDepartment.splice(
      this.listOfPickedDepartment.indexOf(department),
      1
    );
  }

  onSelectAssignedDeparmentChange(selectedValue: any) {
    console.log("Selected", selectedValue);
    let exist = false;
    for (let deparment of this.listOfPickedAssignedDepartment) {
      if (selectedValue == deparment) {
        exist = true;
        break;
      }
    }
    if (!exist) {
      this.listOfPickedAssignedDepartment.push(selectedValue);
    }
  }
  removePickedAssignedDepartment(department) {
    this.listOfPickedAssignedDepartment.splice(
      this.listOfPickedAssignedDepartment.indexOf(department),
      1
    );
  }

  onSelectEmployeeChange(selectedValue: any) {
    console.log("Selected", selectedValue);
    let exist = false;
    for (let person of this.listOfPickedEmployee) {
      if (selectedValue == person) {
        exist = true;
        break;
      }
    }
    if (!exist) {
      this.listOfPickedEmployee.push(selectedValue);
    }
  }
  removePickedEmployee(person) {
    this.listOfPickedEmployee.splice(
      this.listOfPickedEmployee.indexOf(person),
      1
    );
  }

  pushNoti(message, target, action) {
    //noti đến mangers thuộc 1 phòng ban
    if (this.listOfPickedAssignedDepartment.length > 0) {
      for (let department of this.deparmentList) {
        if (target[0].Id == department.id) {
          //Find all managers of this department
          this.getAllManagersOfAnDepartment(department.id, message, action)
          break;
        }
      }
    }
    //noti đến cá nhân
    else {
      let realTarget = []
      for (let person of target) {
        for (let employee of this.employeeList) {
          if (person.Id == employee.id) {
            realTarget.push({ deviceid: employee.deviceId, typedeviceid: employee.operatingSystem })
            break;
          }
        }
      }
      this.global.pushNotificationFromServer(message, realTarget, action)
    }

  }
  getAllManagersOfAnDepartment(departmentID, message, action) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.getAllMangersOfADepartment + departmentID;
      this.http
        .get(
          apiURL,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lay ds cac manager thuoc 1 department ok')
            console.log(res.json());
            let target = [];
            for (let manager of res.json()) {
              target.push({ deviceid: manager.deviceId, typedeviceid: manager.operatingSystem })
            }
            this.global.pushNotificationFromServer(message, target, action)
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }
  assignmentSent() {
    debugger
    console.log(this.fromPage)
    if (this.fromPage == 'assign') {
      //phân công lần 2
      if (this.case.olderAssignees != null) {
        if (this.supportComment == "" && this.case.status == 20)
          this.presentToast("Xin vui lòng thêm ghi chú cho đơn vị hỗ trợ");
        else if (this.assignComment == "" && this.case.status != 20)
          this.presentToast("Xin vui lòng thêm ghi chú cho đơn vị hỗ trợ");
        else if (this.case.olderSupports != null && (this.listOfPickedDepartment.length == this.case.olderSupports.length)) {
          this.presentToast("Xin chọn thêm đơn vị hỗ trợ");
        }
        else {
          this.global.presentLoadingCustom('')
          let listEmployeeToSend = [];
          let listDeapartmentToSend = [];
          let listAssignedDepartmentToSend = [];
          for (let y of this.listOfPickedEmployee) {
            for (let x of this.employeeList) {
              if (y == x.fullname)
                listEmployeeToSend.push({ "Id": x.id, "ObjectType": "Officer", "Name": x.name, 'deparmentName': x.departments[0].name })
            }
          }
          for (let y of this.listOfPickedDepartment) {
            for (let x of this.listOfSupportDepartment) {
              if (y == x.name) {
                listDeapartmentToSend.push({ "Id": x.id, "ObjectType": "Department", "Area": x.area, "Name": x.name })
              }
            }
          }
          let departmentAreaTocompare = null

          for (let y of this.listOfPickedAssignedDepartment) {
            for (let x of this.deparmentList) {
              if (y == x.name) {
                departmentAreaTocompare = x.area;
                listAssignedDepartmentToSend.push({ "Id": x.id, "ObjectType": "Department", "Area": departmentAreaTocompare, "Name": x.name })
              }
            }
          }
          console.log('ds đơn vị hỗ trợ: ');
          console.log(listDeapartmentToSend)
          console.log('ds phân công nhân viên ');
          console.log(listEmployeeToSend)
          console.log('ds phân công đơn vị ');
          console.log(listAssignedDepartmentToSend)

          let listOfAssigneeToSend = listEmployeeToSend;

          console.log('aasssssssssssssssss')
          console.log(this.case.id)
          console.log({ Id: this.global.currentUserInfo.nguoidung_id })
          console.log(this.assignComment)
          console.log(this.supportComment)
          console.log(listOfAssigneeToSend)
          console.log(listDeapartmentToSend)

          if (!this.global.removeAreaCheckWhenAssinging) {
            if(departmentAreaTocompare != null)
            {if (this.case.area != null) {
              if (departmentAreaTocompare.id != this.case.area.id) {
                let alert = this.alertCtrl.create({
                  title: 'Thông báo',
                  message: 'Đơn vị phân công bạn chọn không trực tiếp phụ trách khu vực của vụ việc, vẫn muốn tiếp tục?',
                  buttons: [
                    {
                      text: 'Bỏ qua',
                      role: 'cancel',
                      handler: () => {
                        console.log('Cancel clicked');
                        this.global.cancleCustomLoading()
                        return;
                      }
                    },
                    {
                      text: 'Tiếp tục',
                      role: 'cancel',
                      handler: () => {
                        return new Promise((resolve, reject) => {
                          let headers = new Headers();
                          headers.append("Content-Type", "application/json");
                          headers.append("token", this.global.currentUserInfo.token)

                          let assigner = {
                            departmentName: this.global.currentUserInfo.ten_donvi,
                            Name: this.global.currentUserInfo.ho_ten,
                            Area: this.global.currentUserInfo.area,
                            Id: this.global.currentUserInfo.nguoidung_id,
                            DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer"
                          }
                          let apiURL
                          if (this.case.status != 12)
                            apiURL = this.global.apiURL.func_PhanCong;
                          else
                            apiURL = this.global.apiURL.assignSupport;
                          this.http
                            .post(
                              apiURL,
                              {
                                issueid: this.case.id,
                                assigner: assigner,
                                comment: this.assignComment,
                                supportcomment: this.supportComment,
                                assignee: listOfAssigneeToSend,
                                supportassignee: listDeapartmentToSend,
                              },
                              { headers: headers }
                            )
                            .subscribe(
                              res => {
                                this.global.cancleCustomLoading();
                                console.log(res.json());
                                if (res.json().code == 1) {
                                  console.log("phân công thành công");
                                  this.presentToast("Phân công thành công.");
                                  this.pushNoti('Một vụ việc vi phạm tại: ' + this.case.areadetail + ' vừa được điều phối đến bạn, xin vui lòng kiểm tra danh sách xử lý',
                                    listOfAssigneeToSend, 'xử lý')
                                  this.navCtrl.pop();
                                }
                                else if (res.json().code == 2) {
                                  this.logoutDueToTokenExpired()
                                }
                                else {
                                  this.presentToast("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                                }
                                resolve(res.json());
                              },
                              err => {
                                this.global.cancleCustomLoading();
                                // this.presentToast("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                                console.log(err);
                                reject(err);
                              }
                            );
                        });

                      }
                    }
                  ]
                });
                alert.present();
              }
            }}            
          }
          else {
            return new Promise((resolve, reject) => {
              let headers = new Headers();
              headers.append("Content-Type", "application/json");
              headers.append("token", this.global.currentUserInfo.token)

              let assigner = {
                departmentName: this.global.currentUserInfo.ten_donvi,
                Name: this.global.currentUserInfo.ho_ten,
                Area: this.global.currentUserInfo.area,
                Id: this.global.currentUserInfo.nguoidung_id,
                DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer"
              }
              let apiURL
              if (this.case.status != 12)
                apiURL = this.global.apiURL.func_PhanCong;
              else
                apiURL = this.global.apiURL.assignSupport;
              this.http
                .post(
                  apiURL,
                  {
                    issueid: this.case.id,
                    assigner: assigner,
                    comment: this.assignComment,
                    supportcomment: this.supportComment,
                    assignee: listOfAssigneeToSend,
                    supportassignee: listDeapartmentToSend,
                  },
                  { headers: headers }
                )
                .subscribe(
                  res => {
                    this.global.cancleCustomLoading();
                    console.log(res.json());
                    if (res.json().code == 1) {
                      console.log("phân công thành công");
                      this.presentToast("Phân công thành công.");
                      this.pushNoti('Một vụ việc vi phạm tại: ' + this.case.areadetail + ' vừa được điều phối đến bạn, xin vui lòng kiểm tra danh sách xử lý',
                        listOfAssigneeToSend, 'xử lý')
                      this.navCtrl.pop();
                    }
                    else if (res.json().code == 2) {
                      this.logoutDueToTokenExpired()
                    }
                    else {
                      alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                    }
                    resolve(res.json());
                  },
                  err => {
                    this.global.cancleCustomLoading();
                    // alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                    console.log(err);
                    reject(err);
                  }
                );
            });

          }

        }

      }
      else {
        //phân công lần 1
        if (this.assignComment == "") {
          this.presentToast("Xin vui lòng thêm ghi chú khi phân công");
        } else if (this.listOfPickedEmployee.length == 0 && this.listOfPickedAssignedDepartment.length == 0) {
          this.presentToast("Xin chọn nhân viên hoặc đơn vị để phân công");
        } else {
          this.global.presentLoadingCustom('')
          let listEmployeeToSend = [];
          let listDeapartmentToSend = [];
          let listAssignedDepartmentToSend = [];
          for (let y of this.listOfPickedEmployee) {
            for (let x of this.employeeList) {
              if (y == x.fullname)
                listEmployeeToSend.push({ "Id": x.id, "ObjectType": "Officer", "Name": x.name, 'deparmentName': x.departments[0].name })
            }
          }
          for (let y of this.listOfPickedDepartment) {
            for (let x of this.listOfSupportDepartment) {
              if (y == x.name) {
                listDeapartmentToSend.push({ "Id": x.id, "ObjectType": "Department", "Area": x.area, "Name": x.name })
              }
            }
          }
          let departmentAreaTocompare = null

          for (let y of this.listOfPickedAssignedDepartment) {
            for (let x of this.deparmentList) {
              if (y == x.name) {
                departmentAreaTocompare = x.area;
                listAssignedDepartmentToSend.push({ "Id": x.id, "ObjectType": "Department", "Area": departmentAreaTocompare, "Name": x.name })
              }
            }
          }
          console.log('ds đơn vị hỗ trợ: ');
          console.log(listDeapartmentToSend)
          console.log('ds phân công nhân viên ');
          console.log(listEmployeeToSend)
          console.log('ds phân công đơn vị ');
          console.log(listAssignedDepartmentToSend)

          let listOfAssigneeToSend
          if (listAssignedDepartmentToSend.length > 0) {
            listOfAssigneeToSend = listAssignedDepartmentToSend
            listDeapartmentToSend = []
          }
          else {
            listOfAssigneeToSend = listEmployeeToSend;
          }

          console.log('aasssssssssssssssss')
          console.log(this.case.id)
          console.log({ Id: this.global.currentUserInfo.nguoidung_id })
          console.log(this.assignComment)
          console.log(this.supportComment)
          console.log(listOfAssigneeToSend)
          console.log(listDeapartmentToSend)

          if (!this.global.removeAreaCheckWhenAssinging) {
            if (departmentAreaTocompare != null) {
              if (this.case.area != null) {
                if (departmentAreaTocompare.id != this.case.area.id) {
                  let alert = this.alertCtrl.create({
                    title: 'Thông báo',
                    message: 'Đơn vị phân công bạn chọn không trực tiếp phụ trách khu vực của vụ việc, vẫn muốn tiếp tục?',
                    buttons: [
                      {
                        text: 'Bỏ qua',
                        role: 'cancel',
                        handler: () => {
                          console.log('Cancel clicked');
                          this.global.cancleCustomLoading()
                          return;
                        }
                      },
                      {
                        text: 'Tiếp tục',
                        role: 'cancel',
                        handler: () => {
                          return new Promise((resolve, reject) => {
                            let headers = new Headers();
                            headers.append("Content-Type", "application/json");
                            headers.append("token", this.global.currentUserInfo.token)
  
                            let assigner = {
                              departmentName: this.global.currentUserInfo.ten_donvi,
                              Name: this.global.currentUserInfo.ho_ten,
                              Area: this.global.currentUserInfo.area,
                              Id: this.global.currentUserInfo.nguoidung_id,
                              DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer"
                            }
  
                            let apiURL
                            if (this.case.status != 12)
                              apiURL = this.global.apiURL.func_PhanCong;
                            else
                              apiURL = this.global.apiURL.assignSupport;
                            this.http
                              .post(
                                apiURL,
                                {
                                  issueid: this.case.id,
                                  assigner: assigner,
                                  comment: this.assignComment,
                                  supportcomment: this.supportComment,
                                  assignee: listOfAssigneeToSend,
                                  supportassignee: listDeapartmentToSend,
                                },
                                { headers: headers }
                              )
                              .subscribe(
                                res => {
                                  this.global.cancleCustomLoading();
                                  console.log(res.json());
                                  if (res.json().code == 1) {
                                    console.log("phân công thành công");
                                    this.presentToast("Phân công thành công.");
                                    //phân công phân công
                                    if (listAssignedDepartmentToSend.length > 0) {
                                      this.pushNoti('Một vụ việc vi phạm tại: ' + this.case.areadetail + ' vừa được điều phối đến đơn vị của bạn, xin vui lòng kiểm tra danh sách phân công',
                                        listOfAssigneeToSend, 'điều phối')
                                    }
                                    //phân công xử lý
                                    else {
                                      this.pushNoti('Một vụ việc vi phạm tại: ' + this.case.areadetail + ' vừa được giao cho bạn phụ trách, xin vui lòng kiểm tra danh sách xử lý',
                                        listOfAssigneeToSend, 'xử lý')
                                    }
  
                                    this.navCtrl.pop();
                                  }
                                  else if (res.json().code == 2) {
                                    this.logoutDueToTokenExpired()
                                  }
                                  else {
                                    this.presentToast("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                                  }
                                  resolve(res.json());
                                },
                                err => {
                                  this.global.cancleCustomLoading();
                                  // this.presentToast("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                                  console.log(err);
                                  reject(err);
                                }
                              );
                          });
                        }
                      }
                    ]
                  });
                  alert.present();
                }
              }
            }
          }          
          else {
            return new Promise((resolve, reject) => {
              let headers = new Headers();
              headers.append("Content-Type", "application/json");
              headers.append("token", this.global.currentUserInfo.token)

              let assigner = {
                departmentName: this.global.currentUserInfo.ten_donvi,
                Name: this.global.currentUserInfo.ho_ten,
                Id: this.global.currentUserInfo.nguoidung_id,
                DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer",
                Area: this.global.currentUserInfo.area
              }
              let apiURL
              if (this.case.status != 12)
                apiURL = this.global.apiURL.func_PhanCong;
              else
                apiURL = this.global.apiURL.assignSupport;
              this.http
                .post(
                  apiURL,
                  {
                    issueid: this.case.id,
                    assigner: assigner,
                    comment: this.assignComment,
                    supportcomment: this.supportComment,
                    assignee: listOfAssigneeToSend,
                    supportassignee: listDeapartmentToSend,
                  },
                  { headers: headers }
                )
                .subscribe(
                  res => {
                    this.global.cancleCustomLoading();
                    console.log(res.json());
                    if (res.json().code == 1) {
                      console.log("phân công thành công");
                      this.presentToast("Phân công thành công.");
                      //phân công phân công
                      if (listAssignedDepartmentToSend.length > 0) {
                        this.pushNoti('Một vụ việc vi phạm tại: ' + this.case.areadetail + ' vừa được điều phối đến đơn vị của bạn, xin vui lòng kiểm tra danh sách phân công',
                          listOfAssigneeToSend, 'điều phối')
                      }
                      //phân công xử lý
                      else {
                        this.pushNoti('Một vụ việc vi phạm tại: ' + this.case.areadetail + ' vừa được giao cho bạn phụ trách, xin vui lòng kiểm tra danh sách xử lý',
                          listOfAssigneeToSend, 'xử lý')
                      }
                      this.navCtrl.pop();
                    }
                    else if (res.json().code == 2) {
                      this.logoutDueToTokenExpired()
                    }
                    else {
                      alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                    }
                    resolve(res.json());
                  },
                  err => {
                    this.global.cancleCustomLoading();
                    // alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                    console.log(err);
                    reject(err);
                  }
                );
            });
          }

        }
      }
    }
    else if (this.fromPage == 'process') {

      if (this.showIfNotSupportType) {
        if (this.typeOfProcess == "")
          this.presentToast('Xin vui lòng chọn phân loại xử lý')
        else if (this.processComment == "")
          this.presentToast('Xin vui lòng nhập ghi chú')
        else {
          //Xử lý xuly
          this.global.presentLoadingCustom('')
          console.log(this.case.id + '/' + this.case.trangthai + '/' + this.global.currentUserInfo.ho_ten + '/' + this.processComment + '/' + this.base64ImgList)
          return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("token", this.global.currentUserInfo.token)
            let apiURL = this.global.apiURL.func_XuLy_SuCo_HoanTat;
            let assigner = {
              departmentName: this.global.currentUserInfo.ten_donvi,
              Area: this.global.currentUserInfo.area,
              Name: this.global.currentUserInfo.ho_ten,
              Id: this.global.currentUserInfo.nguoidung_id,
              DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer"
            }

            console.log(this.case.id)
            console.log(this.processComment)
            console.log(assigner)
            console.log(this.typeOfProcess)
            this.http
              .post(
                apiURL,
                {
                  issueid: this.case.id,
                  status: this.typeOfProcess,
                  assigner: assigner,
                  comment: this.processComment,
                },
                { headers: headers }
              )
              .subscribe(
                res => {
                  this.global.cancleCustomLoading();
                  console.log(res.json());
                  if (res.json().code == 1) {
                    console.log("gửi xử lý thành công");
                    if (this.global.imageURL.length > 0) {
                      this.newImgsSender();
                    }
                    this.presentToast("Đã gửi xử lý thành công.");
                    this.navCtrl.pop();
                  }
                  else if (res.json().code == 2) {
                    this.logoutDueToTokenExpired()
                  }
                  else {
                    alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                  }
                  resolve(res.json());
                },
                err => {
                  this.global.cancleCustomLoading();
                  alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                  console.log(err);
                  reject(err);
                }
              );
          });


        }
      }
      else {

        //Xuly hotro
        if (this.processComment == "")
          this.presentToast('Xin vui lòng nhập ghi chú')
        else {
        this.global.presentLoadingCustom('')
          return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("token", this.global.currentUserInfo.token)
            let apiURL = this.global.apiURL.support_confirm;
            let assigner = {
              departmentName: this.global.currentUserInfo.ten_donvi,
              Area: this.global.currentUserInfo.area,
              Name: this.global.currentUserInfo.ho_ten,
              Id: this.global.currentUserInfo.nguoidung_id,
              DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer"
            }

            this.http
              .post(
                apiURL,
                {
                  issueid: this.case.id,
                  assigner: assigner,
                  comment: this.processComment,
                },
                { headers: headers }
              )
              .subscribe(
                res => {
                  this.global.cancleCustomLoading();
                  console.log(res.json());
                  if (res.json().code == 1) {
                    console.log("gửi xử lý thành công");
                    this.presentToast("Đã gửi xử lý thành công.");
                    this.navCtrl.pop();
                  }
                  else if (res.json().code == 2) {
                    this.logoutDueToTokenExpired()
                  }
                  else {
                    alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                  }
                  resolve(res.json());
                },
                err => {
                  this.global.cancleCustomLoading();
                  alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                  console.log(err);
                  reject(err);
                }
              );
          });
        }


      }


    }

    else if (this.fromPage == 'approve') {
      if (this.case.statusSupport == 21) {
        this.presentToast("Đơn vị " + this.case.currentSupports[0].name + " chưa phân công hỗ trợ");
      } else if (this.case.statusSupport == 22) {
        this.presentToast("Chuyên viên " + this.case.currentSupports[0].name + " chưa hỗ trợ xử lý");
      } else {
        if (this.approveComment == "")
          this.presentToast('Xin vui lòng nhập nội dung phê duyệt')
        else {
          this.global.presentLoadingCustom('')
          // console.log(this.case.id + '/' + this.case.trangthai + '/' + this.global.currentUserInfo.ho_ten + '/' + this.processComment + '/' + this.base64ImgList)
          return new Promise((resolve, reject) => {
            let headers = new Headers();
            headers.append("Content-Type", "application/json");
            headers.append("token", this.global.currentUserInfo.token)
            let apiURL = this.global.apiURL.func_XuLy_SuCo_PheDuyetHoanThanh;
            let assigner = {
              departmentName: this.global.currentUserInfo.ten_donvi,
              Area: this.global.currentUserInfo.area,
              Name: this.global.currentUserInfo.ho_ten,
              Id: this.global.currentUserInfo.nguoidung_id,
              DepartmentId: this.global.currentUserInfo.donvi_id, ObjectType: "Officer"
            }

            console.log(this.case.id)
            console.log(this.approveComment)
            console.log(assigner)
            this.http
              .post(
                apiURL,
                {
                  issueid: this.case.id,
                  assigner: assigner,
                  comment: this.approveComment,
                  status: 32
                },
                { headers: headers }
              )
              .subscribe(
                res => {
                  this.global.cancleCustomLoading();
                  console.log(res.json());
                  if (res.json().code == 1) {
                    console.log("gửi phê duyệt thành công");
                    this.presentToast("Đã gửi phê duyệt thành công.");
                    this.navCtrl.pop();
                  }
                  else if (res.json().code == 2) {
                    this.logoutDueToTokenExpired()
                  }
                  else {
                    alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                  }
                  resolve(res.json());
                },
                err => {
                  this.global.cancleCustomLoading();
                  alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
                  console.log(err);
                  reject(err);
                }
              );
          });
        }
      }

    }
  }
  spamReport() {
    debugger
    console.log('báo rác vụ việc: ' + this.case.id)
   this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_XuLy_SuCo_BaoRac;
      let assigner = {
        departmentName: this.global.currentUserInfo.ten_donvi,
        Name: this.global.currentUserInfo.ho_ten,
        Id: this.global.currentUserInfo.nguoidung_id,
        ObjectType: "Officer",
        DepartmentId: this.global.currentUserInfo.donvi_id
      }
      console.log(assigner)
      this.http
        .post(
          apiURL,
          {
            issueid: this.case.id,
            assigner: assigner,
            comment: "Tin báo không đúng sự thật",
            status: 34,
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            this.global.cancleCustomLoading();
            console.log(res.json());
            if (res.json().code == 1) {
              console.log("báo rác thành công");
              this.presentToast("Báo tin rác thành công.");
              this.navCtrl.pop();
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
            }
            resolve(res.json());
          },
          err => {
            this.global.cancleCustomLoading();
            alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
            console.log(err);
            reject(err);
          }
        );
    });
  }

  closeImgZoom() {
    this.dialogForImgPicking = false;
    this.imgSliders.close();
    this.menu.swipeEnable(true);
  }
  openImgZoom(index) {
    this.navCtrl.push(ImgviewerPage, { imgs: this.case.attachment })
  }
  openImgProcessZoom(index) {

    let imgSendToViewer = []
    for (let pic of this.case.resolveAttachment) {
      imgSendToViewer.push(pic.filePath)
    }
    this.navCtrl.push(ImgviewerPage, { imgs: imgSendToViewer })

  }
  openExtraAttachedImgs() {
    this.navCtrl.push(ImgviewerPage, { imgs: this.imageURL })
  }
  openImgPicker() {
    this.dialogForImgPicking = true
    console.log(this.dialogForImgPicking)
    this.imgSliders.showModal();
    this.menu.swipeEnable(false);
  }
  newImgsSender() {
    for (let i = 0; i < this.global.imageURL.length; ++i) {
      let pic = this.global.imageURL[i]
      console.log(pic);
      if (pic.startsWith("file")) {
        this.file
          .resolveLocalFilesystemUrl(pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      } else {
        this.file
          .resolveLocalFilesystemUrl("file://" + pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      }

    }
  }

  // private OLDreadnewImgFile(file: any) {
  //   let orimForm = new FormData();
  //   const reader = new FileReader();
  //   reader.onloadend = () => {
  //     const imgBlob = new Blob([reader.result], { type: file.type });
  //     orimForm.append("data", imgBlob);
  //     orimForm.append("token", this.global.currentUserInfo.token);
  //     orimForm.append("id", this.case.id);
  //     this.postNewImgData(orimForm, file.name)
  //   };
  //   reader.readAsArrayBuffer(file);
  // }

  private readnewImgFile(file: any, index) {
    const reader = new FileReader();
    reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], { type: file.type });
      this.masterFormdataToSend.append("data", imgBlob)
      if (index == this.global.imageURL.length - 1) {
        let self = this;
        setTimeout(function () {
          console.log("Timeout");
          self.masterFormdataToSend.append("id", self.case.id);
          self.masterFormdataToSend.append("token", self.global.currentUserInfo.token);
          self.postNewImgData(self.masterFormdataToSend, 'abc')
        }, 2000);

      }
    };
    reader.readAsArrayBuffer(file);
  }

  private postNewImgData(formData: FormData, filename) {
    console.log('tên file: ' + filename);
    console.log("Here and ready to be uploaded - new img upload");
    console.log(formData.getAll('data'))

    let url = this.global.apiURL.gui_imgs_new + filename
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA"
    });

    // headers.append('location', JSON.stringify(this.locationToSendWithImgs));
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .map(res => res.json())
      .subscribe(
        data => {
          this.global.cancleCustomLoading();
          console.log(data);
          if (data.status == 1) {
            console.log('gửi ảnh thành công');
            // this.testUpdateImgURLtoDB(this.global.apiURL.img_to_db + data.file_path);
            // this.testUpdateImgURLtoDB(data.file_path)
          }
          else {
            // this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
            this.navCtrl.pop();
          }
        },
        err => {
          // this.presentToast('Sự việc đã được báo thành công nhưng có lỗi trong quá trình gửi ảnh/video');
          this.navCtrl.pop();
        }
      );
  }
  enchangryed
  testNewImgPicker() {
    this.navCtrl.push(ImgpickerPage);
  }
  testUpdateImgURLtoDB(imgLink) {
    console.log('img link to update DB: ' + imgLink)
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.update_resolve_attachement;
      this.http.post(apiURL,
        { Id: this.case.id, ResolveAttachment: [{ Filepath: imgLink }], VideoResolveAttachment: [] },
        { headers: headers }).subscribe(
          res => {
            console.log(res.json());
            console.log('lưu url img thành công')
            // this.testGetCasesByID()
            resolve(res.json());
          },
          err => {
            console.log(err);
            console.log('lưu url img thất bại')
            reject(err);
          }
        );
    });
  }

  logoutDueToTokenExpired() {
    if (!this.tokenOut) {
      this.global.cancleCustomLoading();
      alert("Phiên đăng nhập của bạn đã kết thúc, xin vui lòng đăng nhập lại.")
      this.global.islogin = false
      this.storage.set('autoLogin', false);
      this.menuCtrl.enable(true, "menuBeforeLogin");
      this.menuCtrl.enable(false, "menuAfterLogin");
      this.navCtrl.popToRoot();
      this.tokenOut = true;
    }


  }

}
