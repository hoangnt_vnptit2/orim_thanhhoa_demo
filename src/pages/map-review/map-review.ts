import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';

/**
 * Generated class for the MapReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;

@Component({
  selector: 'page-map-review',
  templateUrl: 'map-review.html',
})
export class MapReviewPage {
  @ViewChild('map') mapElement: ElementRef;
  geoCoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  map: any;
  markers = []
  currentLat
  currentLng
  address
  mapOptions = {
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    // center: latLng,
    zoom: 15,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    styles: [
      {
        featureType: 'administrative',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit',
        // elementType: 'labels.icon',
        stylers: [{ visibility: 'off' }]
      },

    ]
  }
  constructor(private nativeGeocoder: NativeGeocoder, public global: GlobalHeroProvider, public geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapReviewPage');
  }

  ionViewWillEnter() {
    this.currentLat = this.navParams.get('lon');
    this.currentLng = this.navParams.get('lat');
    this.address = this.navParams.get('address');
    console.log(this.currentLat)
    console.log(this.currentLng)
    this.loadMap()
  }
  backBtn()
  {
    this.navCtrl.pop();
  }
  loadMap() {
    let latLng = new google.maps.LatLng(this.currentLat, this.currentLng);
    //set options for map
    this.mapOptions['center'] = latLng,
      //initialize the map
      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

    var officesMarker;
    officesMarker = new google.maps.Marker({
      animation: google.maps.Animation.DROP,
      position: new google.maps.LatLng(this.currentLat, this.currentLng),
      title: 'Vị trí sự việc'
    });
    this.markers.push(officesMarker)
    officesMarker.setMap(this.map);
    let content = "<h6>Vị trí sự việc: " + this.address + "</h6>";
    this.addInfoWindow(officesMarker, content);
  }
  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    infoWindow.open(this.map, marker);
  }
}
