import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController } from 'ionic-angular';
import { ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { Http, Headers, RequestOptions } from "@angular/http";
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
import { Slides } from 'ionic-angular';
import { DashboardDetailPage } from '../dashboard-detail/dashboard-detail'
import { DashboardListPage } from '../dashboard-list/dashboard-list';
import { Storage } from "@ionic/storage";

declare var google;

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  pie1DataError = false
  //Map Options
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Slides) slides: Slides;
  timeSelect
  currentPick = null
  currentPickName = ''
  map: any;
  markers = []
  currentLat = 10.5083
  currentLng = 106.8635
  address = "abc xyz"
  geoCoderOptions: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  };
  mapOptions = {
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    rotateControl: false,
    fullscreenControl: false,
    // center: latLng,
    zoom: 9,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    styles: [
      {
        featureType: 'administrative',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'poi',
        stylers: [{ visibility: 'off' }]
      },
      {
        featureType: 'transit',
        // elementType: 'labels.icon',
        stylers: [{ visibility: 'off' }]
      },

    ]
  }
  //Chart Options

  pieChart1: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [
    ],
    options: {
      'titleTextStyle': {
        fontSize: 15
      },
      'width': '100%',
      'height': 'auto',
      'title': 'Thống kê theo lĩnh vực',
      'is3D': 'true',
      'chartArea': { 'width': '90%', 'height': '150' },
      colors: ['#fff7ec', '#fee8c8', '#fdd49e', '#fdbb84', '#fc8d59', '#ef6548', '#d7301f', '#b30000', '#7f0000']
    },
  };

  pieChart2: GoogleChartInterface = {
    chartType: 'PieChart',
    dataTable: [
    ],
    options: {
      'titleTextStyle': {
        fontSize: 15
      },
      'width': '100%',
      'height': 'auto',
      'title': 'Thống kê theo xử lý',
      'is3D': 'true',
      'chartArea': { 'width': '90%', 'height': '150' },
      colors: ['#FFEE58', '#e6693e']
    },
  };
  loading
  masterData = null
  constructor(public storage: Storage, public menuCtrl: MenuController,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, private http: Http, private nativeGeocoder: NativeGeocoder, public global: GlobalHeroProvider, public geolocation: Geolocation, public navCtrl: NavController, public navParams: NavParams) {
  }
  ionViewWillEnter() {
  }

  ionViewDidLeave() {
    let hasDetailPage = false;
    for (let view of this.navCtrl.getViews()) {
      if (view.component == DashboardListPage) {
        hasDetailPage = true; break;
      }
    }
    if (hasDetailPage) {
      this.global.currentDashboardParams = {
        id: this.currentPick,
        time: this.timeSelect,
        currentPickName: this.currentPickName
      }
    }
    else {
      this.global.currentDashboardParams = {
        id: '',
        time: '',
        currentPickName: ''
      }
    }
  }
  ionViewDidEnter() {
    this.addOrRemoveRotateLoading(true)
    if (this.global.currentDashboardParams.time == '') {
      this.timeSelect = 'year';
      this.currentPick = this.global.currentUserInfo.area.code
      this.currentPickName = this.global.currentUserInfo.area.name
      this.getProvinceStatistic(this.global.currentUserInfo.area.code, this.timeSelect);
    }
    else {
      this.timeSelect = this.global.currentDashboardParams.time;
      this.currentPick = this.global.currentDashboardParams.id;
      this.currentPickName = this.global.currentDashboardParams.currentPickName
      this.getProvinceStatistic(this.global.currentUserInfo.area.code, this.global.currentDashboardParams.time);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
    // this.loadMap();
  }

  addOrRemoveRotateLoading(state) {
    
    setTimeout(function(){ 
      if (state) {
        let element = document.getElementById("reloadRotate") as HTMLElement;
        element.classList.add("reloadRotate");
        }
        else
        {
        let element = document.getElementById("reloadRotate") as HTMLElement;
        element.classList.remove("reloadRotate");
        }
     }, 500);
  }
  
  getProvinceStatistic(area, time) {
    this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.Thong_Ke_Mobile;
      this.http
        .post(
          apiURL,
          {
            areaCodeStatic: area,
            kindOfTime: time
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            this.global.cancleCustomLoading()
            this.addOrRemoveRotateLoading(false)
            console.log(res.json());
            if (res.json().code == 1) {
              this.masterData = res.json().data
              console.log(this.masterData)
              this.onAreaChange(this.currentPickName, this.currentPick);
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              alert("Đã xảy ra lỗi kết nối, xin vui lòng thử lại sau.");
            }
            resolve(res);
          },
          err => {
            this.global.cancleCustomLoading();
            this.addOrRemoveRotateLoading(false)

            console.log(err);
            reject(err);
          }
        );
    });
  }
  logoutDueToTokenExpired() {
    this.global.cancleCustomLoading()
    alert("Phiên đăng nhập của bạn đã kết thúc, xin vui lòng đăng nhập lại.")
    this.global.islogin = false
    this.storage.set('autoLogin', false);
    this.menuCtrl.enable(true, "menuBeforeLogin");
    this.menuCtrl.enable(false, "menuAfterLogin");
    this.navCtrl.popToRoot();
  }
  slideChanged() {
    let index = this.slides.getActiveIndex();
    console.log('slide changed')
  }

  slideClick(index) {
    console.log(index)
    this.slides.slideTo(index, 500);
    // this.changeSlideStyle(index);
  }


  setupDataPie2() {
    this.pieChart2.dataTable = []
    this.pieChart2.dataTable.push(['Tổng', ''])
    for (let area of this.global.provinceChartData) {
      if (area.orgName == this.currentPickName) {
        this.pieChart2.dataTable.push(['Đã xử lý', area.processed]);
        this.pieChart2.dataTable.push(['Chưa xử lý', area.processing]);
      }
    }
    this.pieChart2.component.draw();
  }
  pie1Ready(e) {
    console.log('pie 1 ready')
  }
  pie1Error(e) {
    console.log(e)
    this.pie1DataError = true;
  }
  setupDataPie1() {
    this.pieChart1.dataTable = []
    this.pieChart1.dataTable.push(["Lĩnh vực", "Tỉ lệ"])
    if (this.currentPick === this.global.currentUserInfo.area.code)
      this.global.pieChart1Data = this.masterData.services
    else {
      for (let city of this.masterData.children) {
        if (city.areaCode === this.currentPick) {
          this.global.pieChart1Data = city.services; break;
        }
      }
    }
    for (let area of this.global.pieChart1Data) {
      this.pieChart1.dataTable.push([area.name, area.number])
    }
    console.log(this.pieChart1)
    this.pieChart1.component.draw();
  }
  onPieSelect(e) {
    console.log(e)
  }

  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
    let selfMap = this.map
    marker.addListener('click', function () {
      selfMap.setZoom(8);
      selfMap.setCenter(marker.getPosition());
      infoWindow.open(this.map, marker);
    });
  }

  backBtn() {
    this.navCtrl.pop();
  }

  removeDauCau(str) {
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
    str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
    str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
    str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
    str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
    str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
    str = str.replace(/Đ/g, "D");
    return str;
  }

  onTimeChange() {
    console.log(this.timeSelect)
    this.getProvinceStatistic(this.global.currentUserInfo.area.code, this.timeSelect);
  }

  displayPercentage(total, odd) {
    if (total != 0) {
      let result = odd * 100 / total
      return result.toFixed(0)
    }
    else
      return 0
  }

  onAreaChange(name, id) {
    console.log(name + '/' + id);
    this.currentPick = id;
    this.currentPickName = name
    this.setupDataPie1();
    this.changeSlideStyle(id)
  }

  changeSlideStyle(area) {

    document.getElementById(this.global.currentUserInfo.area.code).style.borderBottom = "none";

    for (let city of this.masterData.children) {
      document.getElementById(city.areaCode).style.borderBottom = "none";
    }
    setTimeout(function () {
      document.getElementById(area).style.borderBottom = "1px solid tomato";
    }, 300);
  }

  goList(city, type) {
    console.log(city)
    console.log(type)
    this.navCtrl.push(DashboardListPage, {
      info: {
        city: city, type: type, time: this.timeSelect
      }
    })
  }

}

