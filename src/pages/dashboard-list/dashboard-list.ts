import { Component } from '@angular/core';
import { NavController, IonicPage, ToastController, AlertController, ModalController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MediaPage } from '../media/media'
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { ChatRoomPage } from '../../pages/chat-room/chat-room';
import { Observable } from 'rxjs/Observable';
import { Map15Page } from '../map15/map15';
import { AssignconfirmPage } from '../assignconfirm/assignconfirm';
import {DashboardDetailPage} from '../dashboard-detail/dashboard-detail'
import { StatisticDetailPage } from '../statistic-detail/statistic-detail'


/**
 * Generated class for the ProcessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dashboard-list',
  templateUrl: 'dashboard-list.html',
})

export class DashboardListPage {
  area = null;
  type = null;
  timeSelect=null
  mainLabel=''

  hasCalendar = false;
  toggleCalendar = false;
  loading
  toggle
  prevPickdate = "";
  pickdate = "";
  listCases = []
  originalListOfCases = [] 

  constructor(public loadingCtrl: LoadingController, public menu: MenuController, public navParams: NavParams, public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private geolocation: Geolocation) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    // this.ionViewDidEnter()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewWillEnter() {
   this.area = this.navParams.get('info').city;
    this.type = this.navParams.get('info').type;
    this.timeSelect = this.navParams.get('info').time;
    if (this.type == 'processing')
      this.mainLabel = 'chưa xử lý'
    else
      this.mainLabel = 'đã xử lý'    
    this.pickdate = new Date(Date.now()).toLocaleDateString('en-GB')
    this.getCases();
  }

  getCases() {
    debugger
    let statusToSend=[]
    if (this.type == 'processing')
    statusToSend=[11,12,13,33,43, 20,21,22, 23 ]
    else
    statusToSend=[14,34,44]
    this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.getListOfDashboardByCaseStatus;
      this.http
        .post(
          apiURL,
          {
            kindOfTime:this.timeSelect,
            liststatus: statusToSend,
            areaCodeStatic: this.area
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc ok')
            console.log(res);
            this.global.cancleCustomLoading();
            if (res.json().code == 1) {
              this.listCases = res.json().data;
              this.originalListOfCases = this.listCases
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading();
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad StatisticPage');
  }

  openCalendar() {
    this.toggleCalendar = true;
  }

  closeCalendar() {
    this.toggleCalendar = false;
    this.searchByDate();
  }

  showHideCalendarButton() {
    if (this.hasCalendar)
      this.hasCalendar = false
    else
      this.hasCalendar = true
  }

  onDaySelect(event) {
    console.log(event);
    let dateForCheck = event.date + "/" + (event.month + 1) + "/" + event.year;
    if (this.global.compare2Dates(dateForCheck)) {
      this.pickdate = dateForCheck;
    } else
      this.presentToast(
        "Xin chọn ngày tìm kiếm nhỏ hơn hoặc bằng ngày hiện tại."
      );
  }

  searchByDate() {
    if (this.prevPickdate == "" && this.pickdate != "") {
      console.log("search lần đầu");
      this.prevPickdate = this.pickdate;
      this.listCases = this.global.searchDate(this.pickdate, this.originalListOfCases);
    } else if (this.prevPickdate != this.pickdate) {
      console.log("search các lần tiếp theo");
      this.prevPickdate = this.pickdate;
      this.listCases = this.global.searchDate(this.pickdate, this.originalListOfCases);
    } else {
      console.log("trùng ngày hoặc ngày ko hợp lệ ko search");
    }
  }

  backBtn() {
    this.navCtrl.pop();
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  goDetail(casedetail) {
    this.navCtrl.push(StatisticDetailPage, { case: casedetail })
  }
}