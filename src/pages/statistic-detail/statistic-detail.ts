import { Component } from "@angular/core";
import {
  NavController,
  ToastController,
  AlertController,
  ModalController,
  NavParams,
  MenuController,
  LoadingController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Http, Headers, RequestOptions } from "@angular/http";
import { MediaPage } from "../media/media";
import "rxjs/add/operator/map";
import { Storage } from "@ionic/storage";
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from "@ionic-native/diagnostic";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { Platform } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { GlobalHeroProvider } from "../../providers/global-hero/global-hero";
import { SmsPage } from "../../pages/sms/sms";
import { ChatRoomPage } from "../../pages/chat-room/chat-room";
import { Observable } from "rxjs/Observable";
import { Map15Page } from "../map15/map15";
import { MapReviewPage } from "../map-review/map-review";
import { ImgviewerPage } from '../imgviewer/imgviewer';
import { TimelineDisplayPage } from '../timeline-display/timeline-display';

/**
 * Generated class for the CaseDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-statistic-detail",
  templateUrl: "statistic-detail.html"
})
export class StatisticDetailPage {
  case = null;
  imgSliders = null;
  imgInsideViewer;
  dialogForImgPicking = false;
  currentCaseProgress = []
  showExtraProcessingDetailsFromTimeline=false;
  extraProgressInfo={
    phancong:null,
    xuly:null,
    phanconghotro:null,
    hotro:null
  }
  constructor(
    public loadingCtrl: LoadingController,
    public menu: MenuController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public global: GlobalHeroProvider,
    public splashScreen: SplashScreen,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    private openNativeSettings: OpenNativeSettings,
    private diagnostic: Diagnostic,
    public storage: Storage,
    public alertCtrl: AlertController,
    public http: Http,
    private toastCtrl: ToastController,
    public navCtrl: NavController,
    private geolocation: Geolocation
  ) { }

  ionViewDidLoad() {
    console.log("ionViewDidLoad CaseDetailPage");
  }

  ionViewDidEnter() {
    this.imgSliders = document.getElementById(
      "imageViewer"
    ) as HTMLElement;
  }

  closeImgZoom() {
    this.imgSliders.close();
    this.menu.swipeEnable(true);
  }


  goMap() {
    let parts = this.case.location.split(',')
    this.navCtrl.push(MapReviewPage, {
      lat: parts[1],
      lon: parts[0],
      address:
        this.case.areadetail
    });
  }

  // openImgZoom(index) {
  //   this.navCtrl.push(ImgviewerPage, { imgs: this.imglist })
  // }
  testTimeline() {
    this.navCtrl.push(TimelineDisplayPage, { progress: this.currentCaseProgress })
  }
  getProgress() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL = this.global.apiURL.func_layQuaTrinhXuLy_theoSuCo;
      this.http
        .post(
          apiURL,
          {
            issueid: this.case.id
          },
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log(res.json());
            console.log('get status ok')
            this.currentCaseProgress = res.json().data;
            this.updateExtraInfoFromTimeline();
            resolve(res.json());
          },
          err => {
            console.log(err);
            reject(err);
          }
        );
    });
  }

  updateExtraInfoFromTimeline()
  {
    for(let step of this.currentCaseProgress)
    {
      if(step.status==12)
      {
        let res= {
          name:'',
          date:'',
          department:''
        }
        if(step.assigner.name!=null)
        res.name=step.assigner.name
        res.date=step.createDate
        res.department=step.assigner.departmentName
        this.extraProgressInfo.phancong=res
      }
      else if(step.status==13)
      {
        let res= {
          name:'',
          date:'',
          department:''
        }
        if(step.assigner.name!=null)
        res.name=step.assigner.name
        res.date=step.createDate
        res.department=step.assigner.departmentName
        this.extraProgressInfo.xuly=res
      }
      else if(step.status==22)
      {
        let res= {
          name:'',
          date:'',
          department:''
        }
        if(step.assigner.name!=null)
        res.name=step.assigner.name
        res.date=step.createDate
        res.department=step.assigner.departmentName
        this.extraProgressInfo.phanconghotro=res
      }
      else if(step.status==23)
      {
        let res= {
          name:'',
          date:'',
          department:''
        }
        if(step.assigner.name!=null)
        res.name=step.assigner.name
        res.date=step.createDate
        res.department=step.assigner.departmentName
        this.extraProgressInfo.hotro=res
      }
    }
    this.showExtraProcessingDetailsFromTimeline=true;
  }

  ionViewWillEnter() {
    this.case = this.navParams.get("case");
    console.log(this.case);
    this.getProgress();
  }
  ionViewWillLeave() {
  }

  openImgProcessZoom(index) {

    let imgSendToViewer = []
    for (let pic of this.case.hinhanh_xuly) {
      imgSendToViewer.push(pic.src)
    }
    this.navCtrl.push(ImgviewerPage, { imgs: imgSendToViewer })
  }
}
