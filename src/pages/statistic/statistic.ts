
import { Component } from '@angular/core';
import { NavController, ToastController, AlertController, ModalController, NavParams, MenuController, LoadingController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import { Storage } from '@ionic/storage';
import { JwtHelper } from "jwt-simple";
import { Diagnostic } from '@ionic-native/diagnostic';
import { OpenNativeSettings } from '@ionic-native/open-native-settings';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GlobalHeroProvider } from '../../providers/global-hero/global-hero';
import { Observable } from 'rxjs/Observable';
import { AssignconfirmPage } from '../assignconfirm/assignconfirm';
import { CaseDetailPage } from '../case-detail/case-detail';
import { StatisticDetailPage } from '../statistic-detail/statistic-detail'

/**
* Generated class for the StatisticPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@Component({
  selector: 'page-statistic',
  templateUrl: 'statistic.html',
})
export class StatisticPage {
  tokenOut = false;
  isAdmin = false;
  hasCalendar = false;
  toggleCalendar = false;
  loading
  toggle
  prevPickdate = "";
  pickdate = "";
  listOfDoneAssign = []
  originalListOfDoneAssign = []
  listOfWaitingApproval = []
  originalListOfWaitingApproval = []
  listOfDoneApproval = []
  originalListOfDoneApproval = []
  listOfDoneSupport = []
  originalListOfDoneSupport = []
  listOfDoneAssignSupport = []
  originalListOfDoneAssignSupport = []

  //Extra list
  listOfAssign = [];
  originalListOfAssign = [];
  listOfProcess = [];
  originalListOfProcess = [];
  listOfApproval = [];
  originalListOfApproval = [];

  touchStarted=false;
  constructor(public menuCtrl: MenuController, public loadingCtrl: LoadingController, public menu: MenuController, public navParams: NavParams, public modalCtrl: ModalController, public global: GlobalHeroProvider, public splashScreen: SplashScreen, public platform: Platform, private androidPermissions: AndroidPermissions, private openNativeSettings: OpenNativeSettings, private diagnostic: Diagnostic, public storage: Storage, public alertCtrl: AlertController, public http: Http, private toastCtrl: ToastController, public navCtrl: NavController, private geolocation: Geolocation) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.ionViewWillEnter()
    // this.ionViewDidEnter()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewWillEnter() {
    if (this.global.checkPermission('dashboard_menu_app') && this.global.isThisAppHasAdminRole)
      this.isAdmin = true;
    if (this.global.currentTabInStatistic == '') {
      if (this.global.checkPermission('pheduyet_menu_app'))
        this.toggle = "doneAssign"
      else if (!this.global.checkPermission('pheduyet_menu_app'))
        this.toggle = "waitingApproval"
      // else if (this.global.currentUserInfo.phan_loai == '2' && this.global.right_code == 'PHAN_CONG')
      // this.toggle = 'doneAssignSupport'
      else
        this.toggle = 'doneSupport'
    }
    else
      this.toggle = this.global.currentTabInStatistic;
    this.toggleCalendar = false;
    this.pickdate = new Date(Date.now()).toLocaleDateString('en-GB');
    this.getListOfDoneAssign();
    this.getListOfDoneApproval();
    this.getListOfWaitingApproval();
    this.getListOfDoneSupport();
    this.getListOfDoneAssignSupport();

    //Extra list
    this.getListOfAssign();
    this.getListOfProcess();
    this.getListOfApproval();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatisticPage');
  }

  ionViewDidLeave() {
    let hasDetailPage = false;
    for (let view of this.navCtrl.getViews()) {
      if (view.component == StatisticDetailPage) {
        hasDetailPage = true;
      }
    }
    if (hasDetailPage)
      this.global.currentTabInStatistic = this.toggle;
    else
      this.global.currentTabInStatistic = ''
  }

  openCalendar() {
    this.toggleCalendar = true;
  }

  closeCalendar() {
    this.toggleCalendar = false;
    this.searchByDate();
  }

  showHideCalendarButton() {
    if (this.hasCalendar)
      this.hasCalendar = false
    else
      this.hasCalendar = true
  }

  onDaySelect(event) {
    console.log(event);
    let dateForCheck = event.date + "/" + (event.month + 1) + "/" + event.year;
    if (this.global.compare2Dates(dateForCheck)) {
      this.pickdate = dateForCheck;
    } else
      this.presentToast(
        "Xin chọn ngày tìm kiếm nhỏ hơn hoặc bằng ngày hiện tại."
      );
  }

  searchByDate() {
    if (this.prevPickdate == "" && this.pickdate != "") {
      console.log("search lần đầu");
      this.prevPickdate = this.pickdate;
      this.listOfDoneAssign = this.global.searchDate(this.pickdate, this.originalListOfDoneAssign);
      this.listOfWaitingApproval = this.global.searchDate(this.pickdate, this.originalListOfWaitingApproval);
      this.listOfDoneApproval = this.global.searchDate(this.pickdate, this.originalListOfDoneApproval);
    }
    else if (this.prevPickdate != this.pickdate) {
      console.log("search các lần tiếp theo");
      this.prevPickdate = this.pickdate;
      this.listOfDoneAssign = this.global.searchDate(this.pickdate, this.originalListOfDoneAssign);
      this.listOfWaitingApproval = this.global.searchDate(this.pickdate, this.originalListOfWaitingApproval);
      this.listOfDoneApproval = this.global.searchDate(this.pickdate, this.originalListOfDoneApproval);
    }
    else {
      console.log("trùng ngày hoặc ngày ko hợp lệ ko search");
    }
  }

  backBtn() {
    this.navCtrl.pop();
  }

  getListOfDoneAssign() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [11, 12, 14, 34, 44],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [1, 2],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        listStatusReview: [this.global.permissionCode.HandlingStart, this.global.permissionCode.SupportRequirements]
      }
    }
    this.global.presentLoadingCustom('')
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc đã phancong ok')
            console.log(res);
            this.global.cancleCustomLoading()
            if (res.json().code == 1) {
              this.listOfDoneAssign = res.json().data;
              this.originalListOfDoneAssign = this.listOfDoneAssign
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.global.cancleCustomLoading()
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  getListOfDoneAssignSupport() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [22],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [1, 2],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        listStatusReview: [this.global.permissionCode.SupportDeptAssign]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc đã phancong hotro ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfDoneAssignSupport = res.json().data;
              this.originalListOfDoneAssignSupport = this.listOfDoneAssignSupport
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  getListOfWaitingApproval() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [13, 33, 43],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [1,2],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        liststatus: [this.global.permissionCode.ExecuteAssign, this.global.permissionCode.SupportExecuteAssign]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc cho phe duyet ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfWaitingApproval = res.json().data;
              this.originalListOfWaitingApproval = this.listOfWaitingApproval
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  touchstart(e)
  {
    if(!this.touchStarted)
    this.touchStarted=true
  }

  getListOfDoneApproval() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [14, 34, 44],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [1, 2],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        listStatusReview: [this.global.permissionCode.ExecuteCompleted, this.global.permissionCode.RecycleIssueNotify, this.global.permissionCode.OutOfBoundNotify]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc đã pheduyet ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfDoneApproval = res.json().data;
              this.originalListOfDoneApproval = this.listOfDoneApproval
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  getListOfDoneSupport() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [23],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [1, 2],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        listStatusReview: [this.global.permissionCode.SupportExecuteAssign]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc đã hotro ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfDoneSupport = res.json().data;
              this.originalListOfDoneSupport = this.listOfDoneSupport
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }

  //Extra List
  getListOfAssign() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [11, 12],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [0],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        liststatus: [this.global.permissionCode.SupportRequirements, this.global.permissionCode.HandlingStart]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc phancong ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfAssign = res.json().data;
              this.originalListOfAssign = this.listOfAssign
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  getListOfProcess() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [12],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [0],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        liststatus: [this.global.permissionCode.ExecuteAssign]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc chờ xuly ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfProcess = this.listOfProcess.concat(res.json().data);
              this.originalListOfProcess = this.originalListOfProcess.concat(this.listOfProcess)
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            this.getListOfWaitingToSupport();
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  getListOfWaitingToSupport()
  {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [12],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [0],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        liststatus: [this.global.permissionCode.SupportExecuteAssign]
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc chờ xuly hotro ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfProcess = this.listOfProcess.concat(res.json().data);
              this.originalListOfProcess = this.originalListOfProcess.concat(this.listOfProcess)
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }
  getListOfApproval() {
    let bodyToSend
    if (this.isAdmin) {
      bodyToSend = {
        liststatus: [13, 33, 43],
        areaCodeStatic: this.global.currentUserInfo.area.code.toString()
      }
    }
    else {
      bodyToSend = {
        listAssignStatus: [0],
        assignee: [this.global.currentUserInfo.nguoidung_id, this.global.currentUserInfo.donvi_id],
        liststatus: [13, 33, 43],
      }
    }
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append("Content-Type", "application/json");
      headers.append("token", this.global.currentUserInfo.token)
      let apiURL
      if (this.isAdmin)
        apiURL = this.global.apiURL.func_View_HistoryNews__admin;
      else
        apiURL = this.global.apiURL.func_View_HistoryNews;
      this.http
        .post(
          apiURL,
          bodyToSend,
          { headers: headers }
        )
        .subscribe(
          res => {
            console.log('lấy ds các vụ việc đã hotro ok')
            console.log(res.json());
            if (res.json().code == 1) {
              this.listOfApproval = res.json().data;
              this.originalListOfApproval = this.listOfApproval
            }
            else if (res.json().code == 2) {
              this.logoutDueToTokenExpired()
            }
            else {
              this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            }
            resolve(res.json());
          },
          err => {
            console.log(err);
            this.presentToast('Đã xảy ra lỗi kết nối, xin bạn vui lòng thử lại.');
            reject(err);
          }
        );
    });
  }

  presentToast(mess) {
    let toast = this.toastCtrl.create({
      message: mess,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  goDetail(casedetail) {
    this.navCtrl.push(StatisticDetailPage, { case: casedetail })
  }
  logoutDueToTokenExpired() {
    this.global.cancleCustomLoading()
    if (!this.tokenOut) {
      this.tokenOut = true;
      alert("Phiên đăng nhập của bạn đã kết thúc, xin vui lòng đăng nhập lại.")
      this.global.islogin = false
      this.storage.set('autoLogin', false);
      this.menuCtrl.enable(true, "menuBeforeLogin");
      this.menuCtrl.enable(false, "menuAfterLogin");
      this.navCtrl.popToRoot();
    }
  }
}